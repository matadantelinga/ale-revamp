function media_uploder(el_numb) {

	var image_this = "#image_" + el_numb;
	var upload_this = "#alcatel_homeservice_image" + el_numb;
	
	mediaUploader = wp.media.frames.file_frame = wp.media({
		title: 'Choose Image',
		button: {
		text: 'Choose Image'
	}, multiple: false });

	// When a file is selected, grab the URL and set it as the text field's value
	mediaUploader.on('select', function() {

		attachment = mediaUploader.state().get('selection').first().toJSON();

		//jQuery( upload_this ).next().empty(); //empty image-holder

		//jQuery( upload_this ).next().attr( 'style' , 'width : 150px; height : 150px; background-image: url("' + attachment.url + '"); background-size: contain; background-repeat: no-repeat;' ); // set bg image-holder
		
		//jQuery( upload_this ).next().append( '<img src="<?php echo plugins_url() ?>/360views/images/del-icon.png" class="delete-upload-image" onclick="delete_upload_image(this)" />' ); // set del image

		jQuery( upload_this ).val( attachment.url ); // set hidden value
		if(jQuery(image_this).length == 0) {
			jQuery( upload_this ).parent().prepend( '<img src="'+ attachment.url +'" style="width : 250px" id="image_' + el_numb +'" /><br/>' );
		}
		else{
			jQuery( image_this ).attr('src' , attachment.url);
		}
	});
	// Open the uploader dialog
	mediaUploader.open();
}

function media_uploder_aboutus(el_numb) {

	var image_this = "#image_" + el_numb;
	var upload_this = "#alcatel_aboutus_image" + el_numb;
	
	mediaUploader = wp.media.frames.file_frame = wp.media({
		title: 'Choose Image',
		button: {
		text: 'Choose Image'
	}, multiple: false });

	// When a file is selected, grab the URL and set it as the text field's value
	mediaUploader.on('select', function() {

		attachment = mediaUploader.state().get('selection').first().toJSON();

		//jQuery( upload_this ).next().empty(); //empty image-holder

		//jQuery( upload_this ).next().attr( 'style' , 'width : 150px; height : 150px; background-image: url("' + attachment.url + '"); background-size: contain; background-repeat: no-repeat;' ); // set bg image-holder
		
		//jQuery( upload_this ).next().append( '<img src="<?php echo plugins_url() ?>/360views/images/del-icon.png" class="delete-upload-image" onclick="delete_upload_image(this)" />' ); // set del image

		jQuery( upload_this ).val( attachment.url ); // set hidden value
		if(jQuery(image_this).length == 0) {
			jQuery( upload_this ).parent().prepend( '<img src="'+ attachment.url +'" style="width : 250px" id="image_' + el_numb +'" /><br/>' );
		}
		else{
			jQuery( image_this ).attr('src' , attachment.url);
		}
	});
	// Open the uploader dialog
	mediaUploader.open();
}

function outlet_media_uploder(el_numb) {

	var image_this = "#image_" + el_numb;
	var upload_this = "#alcatel_meta_box_gallery" + el_numb;
	
	mediaUploader = wp.media.frames.file_frame = wp.media({
		title: 'Choose Image',
		button: {
		text: 'Choose Image'
	}, multiple: false });

	// When a file is selected, grab the URL and set it as the text field's value
	mediaUploader.on('select', function() {

		attachment = mediaUploader.state().get('selection').first().toJSON();

		//jQuery( upload_this ).next().empty(); //empty image-holder

		//jQuery( upload_this ).next().attr( 'style' , 'width : 150px; height : 150px; background-image: url("' + attachment.url + '"); background-size: contain; background-repeat: no-repeat;' ); // set bg image-holder
		
		//jQuery( upload_this ).next().append( '<img src="<?php echo plugins_url() ?>/360views/images/del-icon.png" class="delete-upload-image" onclick="delete_upload_image(this)" />' ); // set del image

		jQuery( upload_this ).val( attachment.url ); // set hidden value
		if(jQuery(image_this).length == 0) {
			jQuery( upload_this ).parent().append( '<img src="'+ attachment.url +'" style="width : 250px" id="image_' + el_numb +'" />' );
		}
		else{
			jQuery( image_this ).attr('src' , attachment.url);
		}
	});
	// Open the uploader dialog
	mediaUploader.open();
}


function form_document_media_uploder() {

	var file_name = "#file_name" ;
	var upload_this = "#form_document_file" ;
	var upload_this_hidden = "#form_document_box_file" ;
	
	mediaUploader = wp.media.frames.file_frame = wp.media({
		title: 'Choose File',
		button: {
		text: 'Choose File'
	}, multiple: false });

	// When a file is selected, grab the URL and set it as the text field's value
	mediaUploader.on('select', function() {

		attachment = mediaUploader.state().get('selection').first().toJSON();

		console.log(attachment);

		//jQuery( upload_this ).next().empty(); //empty image-holder

		//jQuery( upload_this ).next().attr( 'style' , 'width : 150px; height : 150px; background-image: url("' + attachment.url + '"); background-size: contain; background-repeat: no-repeat;' ); // set bg image-holder
		
		//jQuery( upload_this ).next().append( '<img src="<?php echo plugins_url() ?>/360views/images/del-icon.png" class="delete-upload-image" onclick="delete_upload_image(this)" />' ); // set del image

		jQuery( upload_this_hidden ).val( attachment.url ); // set hidden value
		if(jQuery(file_name).length == 0) {
			jQuery( upload_this ).parent().append( '<br/><span id="' + file_name +'">'+ attachment.filename +'</span>' );
		}
		else{
			jQuery( file_name ).html( attachment.filename);
		}
	});
	// Open the uploader dialog
	mediaUploader.open();
}