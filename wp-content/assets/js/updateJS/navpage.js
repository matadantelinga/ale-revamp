$(document).on('scroll', function(){
    if ($(window).scrollTop() > 100) {
        $('.scroll-top-wrapper').addClass('show');
        $('.nav-fixed').addClass('nav-scroll');
    } else {
        $('.scroll-top-wrapper').removeClass('show');
        $('.nav-fixed').removeClass('nav-scroll');
    }
});