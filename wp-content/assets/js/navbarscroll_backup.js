$(document).ready(function(){
    $('.btn-mouse-scroll , .get-promo').on('click',function (e) {
        e.preventDefault();

        var target = this.hash;
        var $target = $(target);
        var tambahjarak = 100;
        
        $('html, body').stop().animate({
            'scrollTop': ($target.offset().top) - 87 
        }, 700, 'swing', function () {
            window.location.hash = target;
        });
    });


    $('.nav-icon-lg').click(function(){
        $(this).toggleClass('opennav');
        $('.nav-main').fadeToggle(200);
    });
    $('.nav-icon-xs').click(function(){
        $(this).toggleClass('opennav');
        $('.nav-main').fadeToggle(200);
    });
    $('.video-about').click(function(){
        $('.open-video').fadeToggle(200);
    });
    $('.close-video').click(function(){
        $('.open-video').fadeToggle(200);
    });
});

$(function(){
    $('.scroll-top-wrapper').on('click', scrollToTop);
    });

    function scrollToTop() {
    verticalOffset = typeof(verticalOffset) != 'undefined' ? verticalOffset : 0;
    element = $('body');
    offset = element.offset();
    offsetTop = offset.top;
    $('html, body').animate({scrollTop: offsetTop}, 500, 'linear');
}

wow = new WOW(
  {
    animateClass: 'animated',
    offset:       100,
    callback:     function(box) {
    console.log("WOW: animating <" + box.tagName.toLowerCase() + ">")
    }
  }
);
wow.init();