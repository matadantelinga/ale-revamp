<!-- SLIDER -->
<header id="myCarousel" class="carousel slide">   
    <!-- Mouse Scroll -->
    <div class="mouse-scroll">
        <a class="btn-mouse-scroll" href="#content-oxo">
            <img class="img-mouse-scroll | animated fadeInDown infinite" src="wp-content/assets/img/icon/icon-mouse.png" alt="mouse">
        </a>
    </div>
    <div class="txt-scroll">
        <a class="btn-mouse-scroll" href="#content-oxo">scroll down</a>
    </div>

   

    <!-- Indicators -->
    <ol class="carousel-indicators">
        <?php 
            $args_slide = array( 'post_type' => 'slider_home' , 'orderby' => 'menu_order', 'order' => 'ASC', 'post_status' => 'publish' );
                $loop_slide = new WP_Query( $args_slide );
                $counter = 0;
                while ( $loop_slide->have_posts() ) : $loop_slide->the_post();
        ?>
        <li data-target="#myCarousel" data-slide-to="<?= $counter; ?>" <?= ($counter==0) ? 'class="active"' : '';?> ></li>
        <?php 
                $counter++;
                endwhile;
        ?>
    </ol>
    
    <!-- Wrapper for slides -->
    <div class="carousel-inner">
        <?php 
          $counter = 0;
                while ( $loop_slide->have_posts() ) : $loop_slide->the_post();
                  $slide_img = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ) , "full" );
                  $slide_img_xs =  MultiPostThumbnails::get_post_thumbnail_url(get_post_type(), 'secondary-image', NULL, 'large');
                    $slide_link = get_post_meta( get_the_ID(), 'home_slider_new_meta_url', true );
                    $slide_new_tab = get_post_meta( get_the_ID(), 'use_product_meta_new_tab', true );
                    $slide_desc = get_post_meta( get_the_id(), 'desc_metaname_value', true);
                    $slide_subTitle = get_post_meta( get_the_id(), 'home_slider_meta_subtitle', true);              
                    $slide_color = get_post_meta( get_the_id(), 'home_slider_radio_buttons',true);
                     $slide_url = get_post_meta(get_the_id(), 'home_slider_new_meta_url',true);
                    $cek = get_post_meta(get_the_id(),'home_slider_meta_use_new_tab',true);
                    // print_r($slide_color);die();

        ?>
                <div class="item <?= ($counter==0) ? 'active' : ''; ?>">
                     <?php 
                       if(!empty($slide_url)){
                         if($cek==true){
                           ?>
                            <a href="<?php echo esc_url($slide_url);?>" target="_blank">
                           <?php
                          } 
                          if($cek==false){
                            ?>
                            <a href="<?php echo esc_url($slide_url);?>">
                            <?php
                          }
                        }
                     ?>
                    <div class="fill | slider-xs" style="background-image:url(<?= $slide_img_xs ?>);"></div>
                   <div class="fill" style="background-image:url(<?= $slide_img[0]; ?>);">
                   </div>
                   <?php 
                   if( $slide_color === 'Merah'){?>
                   <div class="slider-caption1">
                   <?php } ?>
                   <?php if($slide_color === 'Orange'){?>
                    <div class="slider-caption2">
                   <?php } ?>
                   <?php if($slide_color === 'Hijau'){?>
                    <div class="slider-caption3">
                   <?php } ?>
                   <?php 
                   if($slide_color === 'Biru'){?>
                     <div class="slider-caption4">
                   <?php } ?>
                    <?php 
                   if($slide_color === 'Toska'){?>
                     <div class="slider-caption5">
                   <?php } ?>
                    <?php
                    if($slide_color === 'Ungu'){?>
                     <div class="slider-caption">
                   <?php } ?>
                    <h4><?= the_title(); ?></h4>
                    <h2><?= $slide_desc;?></h2>
                   </div>
                    </a>
                </div>
        <?php $counter++;  
                endwhile; ?>
    </div>
    <!-- Controls -->
    <a class="left carousel-control" href="#myCarousel" data-slide="prev">
        <span class="icon-prev"></span>
    </a>
    <a class="right carousel-control" href="#myCarousel" data-slide="next">
        <span class="icon-next"></span>
    </a>
</header>