<?php
/**
 * The template for displaying archive pages
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * If you'd like to further customize these archive views, you may create a
 * new template file for each one. For example, tag.php (Tag archives),
 * category.php (Category archives), author.php (Author archives), etc.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

get_header('nav'); ?>
<style type="text/css">
    // CSS Code - add this to the style.css file
.navigation { list-style:none; font-size:12px; }
.navigation li{ display:inline; }
.navigation li a{ display:block; float:left; padding:4px 9px; margin-right:7px; border:1px solid #efefef; }
.navigation li span.current { display:block; float:left; padding:4px 9px; margin-right:7px; border:1px solid #efefef; background-color:#f5f5f5;  }  
.navigation li span.dots { display:block; float:left; padding:4px 9px; margin-right:7px;  }
</style>


<section>
    <article>
        <div class="wrap-bloglist">
            <div class="container">
                <div class="row | title-bloglist">
                    <div class="col-lg-7 | col-md-6 | col-md-offset-1 | col-sm-7">
                        <div class="top-title-blog">
                            <h3><span>blog</span>list</h3>
                        </div>
                    </div>
                    <div class="col-lg-3 | col-md-4 | col-sm-5">
                        <div class="dropdown">
                            <a href="#" class="btn-blog-listview" dropdown-toggle" type="button" data-toggle="dropdown">urut berdasarkan</a>
                            <ul class="dropdown-menu">
                                <li><a href="#">Tanggal</a></li>
                                <li><a href="#">Judul</a></li>
                                <!-- <li><a href="#">Kategori</a></li> -->
                            </ul>
                        </div>
                        
                    </div>
                </div>
		<?php
		if ( have_posts() ) : ?>
			<?php
			/* Start the Loop */
			while ( have_posts() ) : the_post();
                $post_id_main = $post->ID;

                $background = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' );
                // print_r($background);die();
                $excerpt = substr( strip_tags( get_the_content() ), 0, 250);
                $excerpt = $excerpt.'.';
                $permalink = get_permalink($post->ID);
                $date=get_the_date('d', $post->ID );
                $month=get_the_date('M', $post->ID );
                $year = get_the_date('Y', $post->ID);
            ?>
    			<div class="row">
                    <div class="col-md-10 | col-md-offset-1">
                        <div class="content-bloglist | content-blog">
                            <div class="col-lg-5 | col-md-4 | col-sm-5 | nopadding">
                                <div class="img-blog" style="background: url('<?= $background[0]; ?>') no-repeat;background-position: center;background-size: cover;">
                                    <a href="<?= the_permalink() ?>">&nbsp;</a>
                                </div>
                            </div>
                            <div class="col-lg-7 | col-md-8 | col-sm-7 | nopadding">
                                <div class="wrap-txt-blog | pull-left">
                                    <h3 class="title-blog">
                                        <a href="<?= the_permalink() ?>"><?php the_title(); ?></a>
                                    </h3>
                                    <p class="txt-blog">
                                       <?= $excerpt; ?>
                                    </p>
                                    <div class="tag-blog">
                                        <?php the_tags( '<ul><li>', '</li><li>', '</li></ul>' ); ?>
                                    </div>
                                </div>
                                <div class="date-blog | pull-left">
                                    <p class="bdate"><?= $date; ?></p>
                                    <p class="bmonth"><?= $month;?></p>
                                    <p class="byear"><?= $year; ?></p>
                                </div>
                            </div>
                            <div class="view-blog">
                                <div class="comment-blog">
                                    <a href="#">
                                    <img src="<?= esc_url(get_option('siteurl')) ?>/wp-content/assets/img/icon/icon-time.png" alt="">
                                    <?= dg_get_date_diff(date('Y-m-d H:i:s'), get_the_time( 'Y-m-d H:i:s', $post->ID)); ?> yang lalu
                                    </a>
                                </div>
                                <ul class="socmed-blog">
                                    <li>share :</li>
                                    <li>
                                        <a href="<?= the_permalink() ?>" data-share-facebook data-share-url="<?= the_permalink() ?>" data-share-title="<?php the_title();?>" data-share-text="<?= $excerpt; ?>" ><i class="fa fa-facebook"></i></a>
                                    </li>
                                    <li>
                                        <a data-share-twitter data-share-url="<?= the_permalink() ?>" data-share-text="<?php the_title(); ?>" href="<?= the_permalink() ?>"><i class="fa fa-twitter"></i></a>
                                    </li>
                                    <li>
                                        <a href="mailto:?body=<?php the_permalink();?>"><i class="fa fa-envelope"></i></a>
                                    </li>
                                </ul>
                                <a class="btn-readmore" href="<?= $permalink ?>">
                                    selengkapnya<i class="fa fa-long-arrow-right"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            <?php
				/*
				 * Include the Post-Format-specific template for the content.
				 * If you want to override this in a child theme, then include a file
				 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
				 */
				//get_template_part( 'template-parts/content', get_post_format() );

			endwhile;

			the_posts_pagination( array(
				'prev_text' => '' . '<span class="screen-reader-text">' . 'Previous page' . '</span>',
				'next_text' => '<span class="screen-reader-text">' . 'Next page' . '</span>',
				'before_page_number' => '<span class="meta-nav screen-reader-text">' .'Page' . ' </span>',
			) );

		else :

			get_template_part( 'template-parts/post/content', 'none' );

		endif; ?>

	
	</div><!-- #primary -->
	<!-- <?php //get_sidebar(); ?> -->
</div><!-- .wrap -->
</article>
<section>
<?php get_footer();
