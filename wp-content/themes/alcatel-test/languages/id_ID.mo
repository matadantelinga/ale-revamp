��            )         �     �     �     �     �     �     �     �     �     �               "     *     8     A     N     ^     f     o  	   x     �  	   �     �  (   �  Q   �     %     .    7     E  	   b  �  l     �     	               +     1  	   6     @  	   F     P     b     n     t     �     �     �     �     �  	   �     �     �     �  	   �  +   �  L        k     w  2  �     �	     �	                                                	                                                                           
             ABOUT PRODUCT ARCHIVE About Product All Products Archive BUY CART COLOUR CONTINUE CONTINUE SHOPPING DETAIL ORDER DETAILS EVENT ARCHIVE LANGUAGE NEWS ARCHIVE PREVIOUS EVENTS PRODUCT PRODUCTS QUANTITY READ MORE SHIPPING ADDRESS THANK YOU TIPS ARCHIVE Thank you. Your order has been received. Unfortunately your order cannot be processed. Please attempt your purchase again. VIEW ALL View All We will send you an order confirmation by e-mail containing all the relevant details such as the products, price, shipping cost, expected delivery times, payment methods, delivery address, etc. For more information please contact our email contact@greebel.co.id / phone You order is being processed read more Project-Id-Version: Greebel 0.1.20150828
Report-Msgid-Bugs-To: https://wordpress.org/support/theme/twentysixteen
POT-Creation-Date: 2016-11-18 15:51+0700
PO-Revision-Date: 2016-12-28 15:15+0700
Last-Translator: 
Language-Team: 
Language: id_ID
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.8.11
Plural-Forms: nplurals=1; plural=0;
 TENTANG PRODUK ARSIP Tentang Produk Semua Produk Arsip BELI KERANJANG WARNA LANJUTKAN LANJUTKAN BELANJA DETIL ORDER DETIL ARSIP EVENT BAHASA ARSIP BERITA EVENT TERHADULU PRODUK PRODUK KUANTITAS LEBIH LANJUT ALAMAT PENGIRIMAN TERIMA KASIH ARSIP TIP Terima Kasih. Order Anda telah kami terima. Sayangnya order Anda tidak dapat diproses. Tolong lakukan pembelian kembali. LIHAT SEMUA Lihat Semua Kami akan mengirimkan order konfirmasi melalui email yang isinya mengandung semua detil order yang relevan seperti produk, harga, biaya pengiriman, waktu pengiriman, metode pembayaran, alamat pengiriman, dll. Untuk informasi lebih lanjut dapat menghubungi kami melalui email contact@greebel.co.id / telepon Order Anda sedang diproses lebih lanjut 