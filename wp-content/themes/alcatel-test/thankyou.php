<?php 
/**
 * Template Name: Terima Kasih Template
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

get_header('nav');  ?>
<div class="span9">

<div id="myModal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
          
          <div class="modal-body">
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">X</span></button>
            <div class="clearfix"></div>
            <div style="padding: 15px;">
              <h4>Terima Kasih</h4><br>
    
              <p>Terima kasih telah mengirimkan pesan kepada kami. Data anda telah berhasil disimpan di sistem kami.</p>
              <br>
              <p>Kami segera menghubungi anda kembali.</p>
            </div>
          </div>
                      
        </div>
    </div>
</div>

<div class="modal-backdrop fade in"></div>
</div>
<!-- Google Code for Alcatel Contact Us Conversion Page -->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 856368559;
var google_conversion_language = "en";
var google_conversion_format = "3";
var google_conversion_color = "ffffff";
var google_conversion_label = "aHUACL-2h3AQr8usmAM";
var google_remarketing_only = false;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/856368559/?label=aHUACL-2h3AQr8usmAM&amp;guid=ON&amp;script=0"/>
</div>
</noscript>
<?php 

get_template_part('contact_us');
get_footer('more'); ?>
<script type="text/javascript">
  $(document).ready(function(){
    $("#myModal").modal('show');
  });
  $("#myModal").on("hidden.bs.modal", function () {
    var ref = document.referrer;
    window.location = ref;
});
</script>
