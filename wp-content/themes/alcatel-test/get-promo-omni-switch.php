<style type="text/css">
    .oxo-title h3 {
    color: #7e5ea9;
}
.txt-item-promo {
    color: #403e43;
    /* font-size: 40px; */
    font-size: 33px;
    font-weight: 600;
    text-transform: uppercase;
    float: none;
    padding: 0px 30px;
}
.control-promo {
    color: #403e43;
}
.txt-oxo-promo p {
    color: #403e43;
    font-size: 22px;
    font-weight: 600;
    letter-spacing: 1px;
    text-transform: uppercase;
    margin-bottom: 10px;
}
.left-content .oxo-btn {
    color: #000;
    border: solid 1px #403e43;
    display: inline-block;
    margin-top: 30px;
}
</style>
<section>
    <article>
        <div class="wrap-content-switch">
          <div class="container">
                <div class="row">
                  <?php 
                    $args_slide = array( 'post_type' => 'solusi-alcatel' , 'orderby' => 'menu_order', 'order' => 'ASC', 'post_status' => 'publish' );          
                        $get_solusi = get_posts($args_slide);
                        // echo "<pre>";
                        // print_r($get_solusi);die();
                          if($get_solusi):
                            $Title1 = get_post_meta($get_solusi[1]->ID,'produk1_solusi_meta_box',true);
                            $Title2 = get_post_meta($get_solusi[1]->ID,'produk2_solusi_meta_box',true);
                            $image = get_the_post_thumbnail_url($get_solusi[1]);
                            $getTitle = $Title1.' '.$Title2;
                            ?>
                         <div class="col-md-12">
                            <div class="top-title | oxo-title | wow fadeInDown">
                                <h3><?= $Title1; ?><br><span><?= $Title2; ?></span></h3>
                                <!-- <p class="sub-left-content | sub-oxo">usaha kecil dan menengah</p> -->
                            </div>
                        </div>

                    <div class="col-md-5 | col-sm-6">
                        <div class="left-content | wow fadeInUp">
                            <?php 
                                // echo $current_template; 
                               $get_promo = get_post_id_based_on_template('promosi-alcatel',$vTitle);
                               // echo "<pre>";
                               // print_r($get_promo);die();
                               if(count($get_promo) >= 1):
                                        ?>
                            <div class="wrap-carousel-promo">
                                <div class="arrow-promo">
                                    <a class="control-promo" href="#carousel-promo" data-slide="prev">
                                        <i class="fa fa-4x fa-angle-left"></i>
                                    </a>
                                </div>
                           
                                <div id="carousel-promo" class="carousel-promo slide carousel-sync" data-ride="carousel" data-pause="false">
                                    <div class="carousel-inner | carousel-oxo-top">
                                    <?php 
                                    $count = 0;
                                    foreach ($get_promo as $key => $promo):
                                        $title3 = get_post_meta($promo->ID,'subtitle3_promosi_meta_box',true);
                                        if(!empty($title3)):
                                            ?>
                                        <div class="item | item-promo | <?= ($count==0) ? 'active' : ''; ?>">
                                            <p class="txt-item-promo">
                                                <?= $title3;?>
                                            </p>
                                        </div>
                                         <?php endif; $count++; endforeach;?>
                                    </div>
                                </div>
                                 <!-- NEXT BTN -->
                                <div class="arrow-promo">
                                    <a class="control-promo" href="#carousel-promo" data-slide="next">
                                        <i class="fa fa-4x fa-angle-right"></i>
                                    </a>
                                </div>
                                
                                 <div id="carousel-promo" class="slide carousel-sync" data-ride="carousel" data-pause="false">
                                    <div class="carousel-inner | carousel-oxo-bottom">
                                        <?php 
                                        $count=0;
                                        foreach ($get_promo as $key => $promo):
                                            $title5 = get_post_meta($promo->ID,'subtitle5_promosi_meta_box',true);
                                            $title4 = get_post_meta($promo->ID,'subtitle4_promosi_meta_box',true);
                                            $link1 = get_permalink($promo->ID);
                                            // $category = get_the_category_by_id($promo->ID);
                                         ?>
                                        <div class="item <?= ($count==0) ? 'active' : ''; ?>">
                                            <div class="txt-oxo-promo">
                                                <p><?= $title5; ?></p>
                                                <?php if($title4>0){ ?><p>harga idr <?= number_format($title4,2);?> </p><?php } ?>
                                            </div>
                                            <a class="btn-content | oxo-btn" href="<?= $link1;?>">lihat promo<i class="fa fa-long-arrow-right"></i></a>
                                           <!--  <a class="btn-content | oxo-btn" href="?page=Oxo-Connect-R2">lihat promo<i class="fa fa-long-arrow-right"></i> -->
                                            </a>
                                        </div>
                                        <?php $count++; endforeach; ?>
                                     
                                    </div>
                                </div>

                             </div>
                             <?php endif; ?>
                        </div>
                    </div>
                    <!-- END UPDATE --> 
                     <div class="col-md-7 | col-sm-6">
                        <div class="right-content | wow fadeInRight">
                             <img src="<?= $image; ?>" alt="<?= $getTitle;?>" title="">
                        </div>
                    </div>
                         <?php endif;?>
                </div>
            </div>
        </div>
    </article>
</section>
