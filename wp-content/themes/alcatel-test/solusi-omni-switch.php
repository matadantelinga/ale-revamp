<article>
        <div class="wrap-content-switch">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="top-title | switch-title | wow fadeInDown">
                            <h3><?= $SubTitle1; ?><br><span><?= $SubTitle2; ?></span></h3>
                            <p class="sub-left-content | sub-switch">
                                <?= $SubTitle3;?>
                            </p>
                        </div>
                    </div>
                    <div class="col-md-6 | col-sm-6">
                        <div class="left-content | wow fadeInUp">
                            <p class="txt-left-content | txt-switch">
                                <?= $Desc1; ?>
                            </p>
                            <a class="btn-content | switch-btn" href="<?= get_permalink($get_solusi[1]->ID); ?>">selengkapnya<i class="fa fa-long-arrow-right"></i></a>
                        </div>
                    </div>
                    <div class="col-md-6 | col-sm-6">
                        <div class="right-content | wow fadeInRight">
                            <img src="<?= $img1; ?>" alt="<?= $getTitle;?>" title="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </article>