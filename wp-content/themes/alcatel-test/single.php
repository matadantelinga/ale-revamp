<?php
/**
 * The template for displaying all single posts and attachments
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */
get_header('nav'); 
// Start the loop. ?>

<?php $post_id_main = 0; ?>

<style type="text/css">
    .img-blogview img {
        height: auto;
    }
    .img-artikel-terkait img {
        height: 170px;
        width: auto;
    }
    
    @media (max-width: 991px) and (min-width: 320px){
        .img-artikel-terkait a{
            display: block;
            height: 170px;
            overflow: hidden;
        }
        .img-artikel-terkait img {
            width: 100%;
            min-height: 170px;
            height: auto;
        }
    }
</style>

<section>
    <article>
        <div class="wrap-blogview">
            <div class="container">
                <div class="row | title-blogview">
                    <div class="col-lg-3 | col-md-3 | col-sm-4 | col-xs-12 | pull-right">
                        <div class="btn-blog-list | btn-blogview">
                            <a href="<?= site_url(); ?>/blog">blog list</a>
                        </div>
                    </div>
                </div>
                <!-- BLOG CONTENT-->
                <?php if(have_posts()): 
                        while(have_posts()): the_post();

                        $post_id_main = $post->ID;
                        $excerpt = substr( strip_tags( get_the_content() ), 0, 250);

                        ?>
                <div class="row | padd15">
                    <div class="wrap-content-blogview | clearfix">
                        <div class="col-md-10 | col-sm-10 | nopadding">
                            <div class="img-blogview">
                                <?= the_post_thumbnail('full'); ?>
                            </div>
                            <div class="tag-blog | tag-blogview">
                                <?php the_tags( '<ul><li>', '</li><li>', '</li></ul>' ); ?>
                            </div>
                            <div class="txt-blogview">
                                <h3 class="title-blogview">
                                    <?php the_title(); ?>
                                </h3>
                                <p><span><?= the_content();?></span></p>
                            </div>
                        </div>
                        <div class="col-md-2 | col-sm-2">
                            <div class="date-blog | date-blogview | pull-left">
                                <p class="bdate"><?= get_the_date('d',$post->ID); ?></p>
                                <p class="bmonth"><?= get_the_date('M',$post->ID); ?></p>
                                <p class="byear"><?= get_the_date('Y',$post->ID); ?></p>
                            </div>
                            <div class="share-blogview">
                                <p>bagikan</p>
                                <ul>
                                    <li>
                                        <a href="<?= the_permalink() ?>" data-share-facebook data-share-url="<?= the_permalink() ?>" data-share-title="<?php the_title();?>" data-share-text="<?= $excerpt; ?>" ><i class="fa fa-facebook"></i></a>
                                    </li>
                                    <li>
                                        <a data-share-twitter data-share-url="<?= the_permalink() ?>" data-share-text="<?php the_title(); ?>" href="<?= the_permalink() ?>"><i class="fa fa-twitter"></i></a>
                                    </li>
                                    <li>
                                        <a href="mailto:?body=<?php the_permalink();?>"><i class="fa fa-envelope"></i></a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <?php endwhile; endif; ?>
            </div><!-- end container -->
        </div>
    </article>
</section>

<!-- ARTIKEL TERKAIT -->
<section>
    <article>
        <div class="wrap-artikel-terkait">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h3 class="main-artikel-terkait">artikel terkait</h3>
                    </div>
                </div>
               
                <div class="row">
                 <?php 
                   $args = array(
                                    'posts_per_page' => 2
                                   ,'category_name' => 'Blog'
                                   ,'orderby'       => 'date'
                                   ,'order'         => 'DESC'
                                   ,'post_type'     => 'post'
                                   ,'post_status'   => 'publish'
                                   ,'exclude'       => array($post_id_main)
                                );                     
                            $get_post = get_posts($args);
                            foreach ($get_post as $the_post) :
                            
                ?>
                    <div class="col-md-6 | col-sm-6">
                        <div class="artikel-terkait">
                            <div class="col-lg-4 | col-md-4 | img-artikel-terkait | nopadding">
                                <a href="<?= get_permalink($the_post->ID ); ?>">
                                <?= get_the_post_thumbnail($the_post); ?>
                                </a>
                            </div>
                            <div class="col-lg-5 | col-md-5 | title-artikel-terkait">
                                <h3><a href="<?= get_permalink($the_post->ID ); ?>"><?= $the_post->post_title; ?></a></h3>
                            </div> 
                            <div class="col-lg-2 | col-md-2 | date-blog | date-artikel-terkait | pull-left">
                                <p class="bdate"><?= get_the_date('d',$the_post->ID); ?></p>
                                <p class="bmonth"><?= get_the_date('M',$the_post->ID); ?></p>
                                <p class="byear"><?= get_the_date('Y',$the_post->ID); ?></p>
                            </div>
                        </div>
                    </div>
                     <?php endforeach;?>
                </div>
            </div>
        </div>
    </article>
</section>
<?php get_footer(); ?>
