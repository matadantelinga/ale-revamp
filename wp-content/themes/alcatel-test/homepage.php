<?php 
/**
 * Template Name: Beranda Template
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

// if(is_home()) :
    get_header();
// elseif(is_404()):
//     get_header( '404' );
// else: 
// get_header('header'); 
// endif;
?>

<?php get_template_part('carousel'); ?>
    <!-- Page Content -->
    <!-- PRODUCT -->

<section>
<?php 
     $args_slide = array( 'post_type' => 'solusi-alcatel' , 'orderby' => 'menu_order', 'order' => 'ASC', 'post_status' => 'publish' );          
        $get_solusi = get_posts($args_slide);
        // print_r($get_solusi);die();
        if($get_solusi):
        // $count=0;
        //for($count=0; $count<4; $count++):
            $SubTitle1 = get_post_meta($get_solusi[0]->ID,'produk1_solusi_meta_box',true);
            $SubTitle2 = get_post_meta($get_solusi[0]->ID,'produk2_solusi_meta_box',true);
            $SubTitle3 = get_post_meta($get_solusi[0]->ID,'produk3_solusi_meta_box',true);
            $Desc1 = get_post_meta($get_solusi[0]->ID,'produk_solusi_meta_description',true);
            $img1 = get_the_post_thumbnail_url($get_solusi[0]);
            $link1 = get_permalink($get_solusi[0]->post_title);
            $caption = get_post(get_post_thumbnail_id($get_solusi[0]));
            $img_alt = $caption->post_title;


    ?>
    <article>
       
        <div id="content-oxo" class="wrap-content-oxo">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="top-title | oxo-title | wow fadeInDown">
                            <h3><?= $SubTitle1; ?><br><span><?= $SubTitle2; ?></span>
                           
                            <p class="sub-left-content | sub-oxo"><?= $SubTitle3;  ?></p></h3>
                        </div>
                    </div>
                    <div class="col-md-6 | col-sm-6">
                       
                        <div class="left-content | wow fadeInUp">
                            <p class="txt-left-content | txt-oxo"><?=  $Desc1; ?>
                            </p>
                            <a class="btn-content | oxo-btn" href="<?= get_permalink($get_solusi[0]->ID); ?>">selengkapnya<i class="fa fa-long-arrow-right"></i></a>
                        </div>
                       
                    </div>
                    
                    <div class="col-md-6 | col-sm-6">
                        <div class="right-content | wow fadeInRight">
                            <img src="<?= $img1; ?>" alt="<?= $img_alt; ?>" title="">
                        </div>
                    </div>
                     
                      
                </div>
            </div>
        </div>
  
    </article>
<?php 
    $SubTitle1 = get_post_meta($get_solusi[1]->ID,'produk1_solusi_meta_box',true);
    $SubTitle2 = get_post_meta($get_solusi[1]->ID,'produk2_solusi_meta_box',true);
    $SubTitle3 = get_post_meta($get_solusi[1]->ID,'produk3_solusi_meta_box',true);
    $Desc1 = get_post_meta($get_solusi[1]->ID,'produk_solusi_meta_description',true);
    $img1 = get_the_post_thumbnail_url($get_solusi[1]);
    $link1 = get_permalink($get_solusi[1]->post_title);
    $caption = get_post(get_post_thumbnail_id($get_solusi[1]));
            $img_alt = $caption->post_title;
?>
    <article>
        <div class="wrap-content-switch">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="top-title | switch-title | wow fadeInDown">
                            <h3><?= $SubTitle1; ?><br><span><?= $SubTitle2; ?></span>
                            <p class="sub-left-content | sub-switch">
                            <?= $SubTitle3; ?>
                            </p></h3>
                        </div>
                    </div>
                    <div class="col-md-6 | col-sm-6">
                        <div class="left-content | wow fadeInUp">
                            <p class="txt-left-content | txt-switch"><?=$Desc1; ?>
                            </p>
                            <a class="btn-content | switch-btn" href="<?= get_permalink($get_solusi[1]->ID); ?>">selengkapnya<i class="fa fa-long-arrow-right"></i></a>
                        </div>
                    </div>
                    <div class="col-md-6 | col-sm-6">
                        <div class="right-content | wow fadeInRight">
                             <img src="<?= $img1; ?>" alt="<?= $img_alt; ?>" title="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </article>
<?php 
    $SubTitle1 = get_post_meta($get_solusi[2]->ID,'produk1_solusi_meta_box',true);
    $SubTitle2 = get_post_meta($get_solusi[2]->ID,'produk2_solusi_meta_box',true);
    $SubTitle3 = get_post_meta($get_solusi[2]->ID,'produk3_solusi_meta_box',true);
    $Desc1 = get_post_meta($get_solusi[2]->ID,'produk_solusi_meta_description',true);
    $img1 = get_the_post_thumbnail_url($get_solusi[2]);
    $link1 = get_permalink($get_solusi[2]->post_title);
    $caption = get_post(get_post_thumbnail_id($get_solusi[2]));
            $img_alt = $caption->post_title;
?>
    <article>
        <div class="wrap-content-wifi">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="top-title | wifi-title | wow fadeInDown">

                            <h3><?= $SubTitle1; ?><br><span><?= $SubTitle2; ?></span>
                            <p class="sub-left-content | sub-wifi"><?= $SubTitle3; ?></p></h3>
                        </div>
                    </div>
                    <div class="col-md-7 | col-sm-7">
                        <div class="left-content | wow fadeInUp">
                            <p class="txt-left-content | txt-wifi"><?= $Desc1; ?>
                            </p>
                            <a class="btn-content | wifi-btn" href="<?= get_permalink($get_solusi[2]->ID); ?>">selengkapnya<i class="fa fa-long-arrow-right"></i></a>
                        </div>
                    </div>
                    <div class="col-md-5 | col-sm-5">
                        <div class="right-content | img-wifi | wow fadeInRight">
                            <img src="<?= $img1; ?>" alt="<?= $img_alt; ?>" title="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </article>
<?php endif; ?>
<!-- BLOG POST -->
<section>
    <article>
        <div class="wrap-blog">
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 | col-md-7 | col-md-offset-1 | col-sm-9">
                        <div class="top-title-blog">
                            <h4><span>blog&amp;</span>news</h4>
                        </div>
                    </div>
                    <?php   //$slide_link = get_post_meta( get_the_ID(), 'blog_new_meta_url', true );
                         //echo $slide_link;die();?>
                    <div class="col-lg-2 | col-md-3 | col-sm-3">
                        <div class="btn-blog-list">
                            <a href="<?= esc_url(get_option('siteurl')); ?>/blog">Blog List</a>
                        </div>
                    </div>
                </div>

                <!-- BLOG -->
                 <?php 
                     $args = array(
                            'posts_per_page' => 1
                           ,'category_name' => 'Blog'
                           ,'orderby'       => 'date'
                           ,'order'         => 'DESC'
                           ,'post_type'     => 'post'
                           ,'post_status'   => 'publish'
                        );                     
                     $postslist = new WP_Query( $args );
                     if($postslist->have_posts()):
                        while ( $postslist->have_posts()) : $postslist->the_post(); 
                            $imgBlog = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' );
                            // print_r($imgBlog);die();
                            $excerpt = substr( strip_tags( get_the_content() ), 0, 250);
                            $excerpt = $excerpt.'.';
                            $permalink = get_permalink($post->ID);
                            $date=get_the_date('d', $post->ID );
                            $month=get_the_date('M', $post->ID );
                            $year = get_the_date('Y', $post->ID);

                            // $current_date = date( 'Y-m-d H:i:s');
                            // $get_date = get_the_time( 'Y-m-d H:i:s', $post_id);
                            // $current_date1 = strtotime($current_date);
                            // $get_date1 = strtotime($get_date);
                            // $diff = $current_date1- $get_date1;
                            // $getCount =  round($diff / 86400);
                            // $date1 = get_the_date($post_id);
                            // $date2 = date(get_option('date_format'));
                            // $interval = date_diff($current_date1,$get_date1);
                            // echo $getCount;die();
                ?>
                <div class="row">
                    <div class="col-md-10 | col-md-offset-1">
                        <div class="content-blog">
                            <div class="col-lg-5 | col-md-4 | col-sm-5 | nopadding">
                                

                                <div class="img-blog" style="background:url(<?= $imgBlog[0]; ?>)no-repeat;background-position:center center;
                                    background-size:cover;">
                                    <a href="#">&nbsp;</a>
                                </div>
                            </div>
                            <div class="col-lg-7 | col-md-8 | col-sm-7 | nopadding">
                                <div class="wrap-txt-blog | pull-left">
                                    <h4 class="title-blog">
                                         <a href="<?= the_permalink() ?>"><?php the_title(); ?></a>
                                    </h4>
                                    <p class="txt-blog">
                                        <?= $excerpt; ?>
                                    </p>
                                    <div class="tag-blog">
                                        <?php the_tags( '<ul><li>', '</li><li>', '</li></ul>' ); ?>
                                    </div>
                                </div>
                                <div class="date-blog | pull-left">
                                    <p class="bdate"><?= $date; ?></p>
                                    <p class="bmonth"><?= $month;?></p>
                                    <p class="byear"><?= $year; ?></p>
                                </div>
                            </div>
                            <div class="view-blog">
                                <div class="comment-blog">
                                    <a href="javascript:void(0);">
                                    <img src="<?= esc_url(get_option('siteurl')) ?>/wp-content/assets/img/icon/icon-time.png" alt="time">
                                    <?= dg_get_date_diff(date('Y-m-d H:i:s'), get_the_time( 'Y-m-d H:i:s', $post->ID)); ?> yang lalu
                                    </a>
                                </div>
                                <ul class="socmed-blog">
                                    <li>share :</li>
                                    <li>
                                        <a href="<?php the_permalink();?>"" data-share-facebook data-share-url="<?php the_permalink();?>" data-share-title="<?php the_title();?>" data-share-text="<?php $excerpt;?>"><i class="fa fa-facebook"></i></a>
                                    </li>
                                    <li>
                                        <a href="<?php the_permalink();?>" data-share-twitter data-share-url="<?php the_permalink();?>" data-share-text="<?php the_title();?>"><i class="fa fa-twitter"></i></a>
                                    </li>
                                    <li>
                                        <a href="mailto:?body=<?php the_permalink();?>"><i class="fa fa-envelope"></i></a>
                                    </li>
                                </ul>
                                 <a class="btn-readmore" href="<?= $permalink ?>">
                                    selengkapnya<i class="fa fa-long-arrow-right"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                 <?php endwhile; endif; ?>
                <!-- Title Bottom -->
                <div class="row">
                    <div class="col-md-1 | col-sm-12 | col-md-offset-1">
                        <div class="arrow-blog">
                            <a class="left-control-blog" href="#carousel-blog" data-slide="prev">
                                <i class="fa fa-4x fa-angle-left"></i>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-8 | col-sm-12 | nopadd-blog-slide">
                        <div id="carousel-blog" class="carousel-blog slide">
                            <div class="carousel-inner | carousel-in-blog">
                                <!-- item -->
                                <?php
                                 $args = array(
                                    'posts_per_page' => 7
                                   ,'category_name' => 'Blog'
                                   ,'orderby'       => 'date'
                                   ,'order'         => 'DESC'
                                   ,'post_type'     => 'post'
                                   ,'post_status'   => 'publish'
                                );                     
                            $get_post = get_posts($args);
                            if($get_post){
                                // $link = get_permalink($get_post[0]->ID );
                                // echo $link;die();
                                // echo $get_post[0]->post_title;
                                // $title1 = $get_post[0]->post_title;
                                // $date1 = get_the_date('d',$get_post[0]->ID);
                                // $month1 = get_the_date('M',$get_post[0]->ID);
                                // $year1 = get_the_date('Y',$get_post[0]->ID);
                                // $title2 = $get_post[1]->post_title;
                                // $date2 = get_the_date('d',$get_post[1]->ID);
                                // $month2 = get_the_date('M',$get_post[1]->ID);
                                // $year2 = get_the_date('Y',$get_post[1]->ID);

                                // echo $date1 ."<br>". $month1 ."<br>". $year1."<br>".$title1;
                                // echo $date2 ."<br>". $month2 ."<br>". $year2."<br>".$title2;
                                // echo $get_post[1]->post_title;
                         
                                // while ( $postslist->have_posts()) : $postslist->the_post();
                                //     // print_r(the_title());
                                //     $date[0]=$postslist->title;
                                //     echo $date[0];die();
                                //     $date=get_the_date('d', $post_id );
                                //     $month=get_the_date('M', $post_id );
                                //     $year = get_the_date('Y', $post_id);
                                             // print_r($month);
                                //$count=0;
                                // for($count=1;$count<=6;$count++){
                                    // print_r($count);
                                 ?>

                                
                                <?php /* -- EDIT FAUZI --
                                <div class="item | item-in-blog | <?= ($count==0) ? 'active' : ''; ?>">
                                 
                                    <div class="wrap-title">
                                        
                                        <div class="col-md-6 | col-sm-6 | padd-left75 | padd-right75">
                                            <div class="wrap-title-bottom | clearfix">
                                                <div class="date-bottom">
                                                    <p class="tdate"><?= get_the_date('d',$get_post[1]->ID); ?></p>
                                                    <p class="tmonth"><?= get_the_date('M',$get_post[1]->ID); ?></p>
                                                    <p class="tyear"><?= get_the_date('Y',$get_post[1]->ID); ?></p>
                                                </div>
                                                <div class="title-bottom">
                                                    <a href="<?= get_permalink($get_post[1]->ID ); ?>"><?= $get_post[1]->post_title; ?></a>
                                                </div>
                                            </div>
                                        </div>
                                       
                                        
                                        <div class="col-md-6 | col-sm-6 | padd-left75 | padd-right75">
                                            <div class="wrap-title-bottom | clearfix">
                                                <div class="date-bottom">
                                                    <p class="tdate"><?= get_the_date('d',$get_post[2]->ID); ?></p>
                                                    <p class="tmonth"><?= get_the_date('M',$get_post[2]->ID); ?></p>
                                                    <p class="tyear"><?= get_the_date('Y',$get_post[2]->ID); ?></p>
                                                </div>
                                                <div class="title-bottom">
                                                   <a href="<?= get_permalink($get_post[2]->ID ); ?>"><?= $get_post[2]->post_title; ?></a>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                              <!-- end item -->
                                </div>
                                 
                               <div class="item | item-in-blog">
                                    <div class="wrap-title">
                                        <div class="col-md-6 | col-sm-6 | padd-left75 | padd-right75">
                                            <div class="wrap-title-bottom | clearfix">
                                                <div class="date-bottom">
                                                    <p class="tdate"><?= get_the_date('d',$get_post[3]->ID); ?></p>
                                                    <p class="tmonth"><?= get_the_date('M',$get_post[3]->ID); ?></p>
                                                    <p class="tyear"><?= get_the_date('Y',$get_post[3]->ID); ?></p>
                                                </div>
                                                <div class="title-bottom">
                                                   <a href="<?= get_permalink($get_post[3]->ID ); ?>"><?= $get_post[3]->post_title; ?></a>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div class="col-md-6 | col-sm-6 | padd-left75 | padd-right75">
                                            <div class="wrap-title-bottom | clearfix">
                                                <div class="date-bottom">
                                                    <p class="tdate"><?= get_the_date('d',$get_post[4]->ID); ?></p>
                                                    <p class="tmonth"><?= get_the_date('M',$get_post[4]->ID); ?></p>
                                                    <p class="tyear"><?= get_the_date('Y',$get_post[4]->ID); ?></p>
                                                </div>
                                                <div class="title-bottom">
                                                   <a href="<?= get_permalink($get_post[4]->ID ); ?>"><?= $get_post[4]->post_title; ?></a>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                               <!--   <div class="item | item-in-blog">
                                    <div class="wrap-title">
                                        <div class="col-md-6 | col-sm-6 | padd-left75 | padd-right75">
                                            <div class="wrap-title-bottom | clearfix">
                                                <div class="date-bottom">
                                                    <p class="tdate"><?= get_the_date('d',$get_post[5]->ID); ?></p>
                                                    <p class="tmonth"><?= get_the_date('M',$get_post[5]->ID); ?></p>
                                                    <p class="tyear"><?= get_the_date('Y',$get_post[5]->ID); ?></p>
                                                </div>
                                                <div class="title-bottom">
                                                   <a href="<?= get_permalink($get_post[5]->ID ); ?>"><?= $get_post[5]->post_title; ?></a>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div class="col-md-6 | col-sm-6 | padd-left75 | padd-right75">
                                            <div class="wrap-title-bottom | clearfix">
                                                <div class="date-bottom">
                                                    <p class="tdate"><?= get_the_date('d',$get_post[6]->ID); ?></p>
                                                    <p class="tmonth"><?= get_the_date('M',$get_post[6]->ID); ?></p>
                                                    <p class="tyear"><?= get_the_date('Y',$get_post[6]->ID); ?></p>
                                                </div>
                                                <div class="title-bottom">
                                                   <a href="<?= get_permalink($get_post[6]->ID ); ?>"><?= $get_post[6]->post_title; ?></a>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div> -->
                                <!-- end item -->
                                */
                               ?>

                               <?php unset($get_post[0]); ?>

                                <div class="item | item-in-blog | active">
                                    <div class="wrap-title">

                                <?php
                                $count = 0;
                                foreach($get_post as $mpost) :
                                ?>
                                        <div class="col-md-6 | col-sm-6 | padd-left75 | padd-right75">
                                            <div class="wrap-title-bottom | clearfix">
                                                <div class="date-bottom">
                                                    <p class="tdate"><?= get_the_date('d',$mpost->ID); ?></p>
                                                    <p class="tmonth"><?= get_the_date('M',$mpost->ID); ?></p>
                                                    <p class="tyear"><?= get_the_date('Y',$mpost->ID); ?></p>
                                                </div>
                                                <div class="title-bottom">
                                                    <a href="<?= get_permalink($mpost->ID ); ?>"><?= $mpost->post_title; ?></a>
                                                </div>
                                            </div>
                                        </div>

                                    <?php if($count%2 == 1 && $count !== count($get_post)-1) : ?>

                                     </div>
                              <!-- end item -->
                                </div>

                                <div class="item | item-in-blog">
                                    <div class="wrap-title">

                                    <?php endif; ?>

                                <?php

                                    $count++;
                                endforeach;

                                ?>
                                    </div>
                                <!-- end item -->
                                </div>


                               <?php }//$count++; }//} ?> 
                            </div>
                        </div>
                    </div><!-- end carousel blog -->
                     <div class="col-md-1 | col-sm-12">
                        <div class="arrow-blog">
                            <a class="right-control-blog" href="#carousel-blog" data-slide="next">
                                <i class="fa fa-4x fa-angle-right"></i>
                            </a>
                        </div>
                    </div>
                    
                </div>

            </div>
        </div>
    </article>
</section>
<!-- HUBUNGI KAMI -->
<?php get_template_part('contact_us'); ?>

    <?php get_footer(); ?>
    <!-- /.container -->