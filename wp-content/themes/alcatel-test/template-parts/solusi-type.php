<?php 
//SIDE TEMPLATE SOLUSI
function add_template_solusi_meta_box()
{
    add_meta_box("template-solusi-meta-box", "Template Page", "template_solusi_meta_box_markup", "solusi-alcatel", "side", "high", null);
}

add_action("add_meta_boxes", "add_template_solusi_meta_box");
function template_solusi_meta_box_markup($post)
{
     wp_nonce_field( basename(__FILE__), 'solusi_template_meta_nonce' );
    $current_template = get_post_meta( $post->ID, 'solusi_template_meta_box', true);
    // echo $current_template;die();
    $name = 'solusi_template_meta_box';
    $options = array(0=>'--Pilih Template--',1=>'Template Oxo Connect', 2=>'Template Omniswitch', 3=>'Template Wifi Omni Access');
    $selected = $solusi_template_meta_box;
    //ambil fungsi dropdown
    echo dg_dropdown( $name, $options, $selected, $current_template);
}

function Template_Solusi_MetaBox_Save( $post_id ) {
    $current_nonce = $_POST['solusi_template_meta_nonce'];
    $is_autosaving = wp_is_post_autosave( $post_id );
    $is_revision   = wp_is_post_revision( $post_id );
    $valid_nonce   = ( isset( $current_nonce ) && wp_verify_nonce( $current_nonce, basename( __FILE__ ) ) ) ? 'true' : 'false';

    // if the post is autosaving, a revision, or the nonce is not valid
    // do not save any changed settings.
    if ( $is_autosaving || $is_revision || !$valid_nonce ) {
        return;
    }

    // Find our 'promo_kategori_meta_box' field in the POST request, and save it
    // when the post is updated. Note that the POST field matches the
    // name of the select box in the markup.
    $solusi_template_meta_box = $_POST['solusi_template_meta_box'];
    update_post_meta( $post_id, 'solusi_template_meta_box', $solusi_template_meta_box );
}
add_action( 'save_post', 'Template_Solusi_MetaBox_Save' );
//end - SIDE TEMPLATE SOLUSI


function solusi_register(){
$lbl_solusi = array(
  'name'               => _x( 'Solusi', 'post type general name', 'your-plugin-textdomain' ),
  'singular_name'      => _x( 'Solusi', 'post type singular name', 'your-plugin-textdomain' ),
  'menu_name'          => _x( 'Solusi', 'admin menu', 'your-plugin-textdomain' ),
  'name_admin_bar'     => _x( 'Solusi', 'add new on admin bar', 'your-plugin-textdomain' ),
  'add_new'            => _x( 'Add New', 'book', 'your-plugin-textdomain' ),
  'add_new_item'       => __( 'Add New Solusi', 'your-plugin-textdomain' ),
  'new_item'           => __( 'New Solusi', 'your-plugin-textdomain' ),
  'edit_item'          => __( 'Edit Solusi', 'your-plugin-textdomain' ),
  'view_item'          => __( 'View Solusi', 'your-plugin-textdomain' ),
  'all_items'          => __( 'All Solusi', 'your-plugin-textdomain' ),
  'search_items'       => __( 'Search Solusi', 'your-plugin-textdomain' ),
  'parent_item_colon'  => __( 'Parent Solusi:', 'your-plugin-textdomain' ),
  'not_found'          => __( 'No promo found.', 'your-plugin-textdomain' ),
  'not_found_in_trash' => __( 'No promo found in Trash.', 'your-plugin-textdomain' )
);

$args_solusi = array(
  'labels'             => $lbl_solusi,
  'description'        => __( 'Description.', 'your-plugin-textdomain' ),
  'public'             => true,
  'publicly_queryable' => true,
  'show_ui'            => true,
  'show_in_menu'       => true,
  'query_var'          => true,
  'rewrite'            => array( 'slug' => 'solusi-alcatel' ),
  'capability_type'    => 'post',
  'has_archive'        => true,
  'hierarchical'       => true,
  'menu_position'      => null,
  'supports'           => array( 'title', 'thumbnail', 'page-attributes' )
);
  register_post_type( 'solusi-alcatel', $args_solusi );
}
add_action('init', 'solusi_register');
function create_solusi_taxonomies() {
    $lbl_solusi = array(
        'name'              => _x( 'Categories', 'taxonomy general name' ),
        'singular_name'     => _x( 'Category', 'taxonomy singular name' ),
        'search_items'      => __( 'Search Categories' ),
        'all_items'         => __( 'All Categories' ),
        'parent_item'       => __( 'Parent Category' ),
        'parent_item_colon' => __( 'Parent Category:' ),
        'edit_item'         => __( 'Edit Category' ),
        'update_item'       => __( 'Update Category' ),
        'add_new_item'      => __( 'Add New Category' ),
        'new_item_name'     => __( 'New Category Name' ),
        'menu_name'         => __( 'solusi-alcatel' ),
    );

    $args_solusi = array(
        'hierarchical'      => true, // Set this to 'false' for non-hierarchical taxonomy (like tags)
        'labels'            => $lbl_solusi,
        'show_ui'           => true,
        'show_admin_column' => true,
        'query_var'         => true,
        'rewrite'           => array( 'slug' => 'solusi-alcatel' ),
    );

    register_taxonomy( 'solusi_categories', array( 'solusi-alcatel' ), $args_solusi );
}
add_action( 'init', 'solusi_register', 0 );

add_action('save_post','wp_insert_post_solusi');

function wp_insert_post_solusi($post_id){
    global $post;
     // Bail if we're doing an auto save
    if(defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) return;

    // if our nonce isn't there, or we can't verify it, bail
    if( !isset( $_POST['meta_box_nonce'] ) || !wp_verify_nonce( $_POST['meta_box_nonce'], 'post_solusi_new_meta_box_nonce' ) ) return;

     // if our current user can't edit this post, bail
    if( !current_user_can( 'edit_post' ) ) return;

    $slug = "solusi-alcatel";
    if($slug != $post->post_type) return;

    if(isset($_POST['subtitle1_solusi_meta_box'])){
        update_post_meta($post_id,'subtitle1_solusi_meta_box',esc_html($_POST['subtitle1_solusi_meta_box']));
    }
     if(isset($_POST['subtitle2_solusi_meta_box'])){
        update_post_meta($post_id,'subtitle2_solusi_meta_box',esc_html($_POST['subtitle2_solusi_meta_box']));
    }
     if(isset($_POST['subtitle3_solusi_meta_box'])){
        update_post_meta($post_id,'subtitle3_solusi_meta_box',esc_html($_POST['subtitle3_solusi_meta_box']));
    }
    // Make sure your data is set before trying to save it
     // if (!empty($_POST['post_solusi_meta_description']))
     //  {
     //    $datta=htmlspecialchars($_POST['post_solusi_meta_description']);
     //    update_post_meta($post_id, 'post_solusi_meta_description', $datta );
     //  }
    if ( isset ( $_POST['post_solusi_meta_description'] ) ) {
    update_post_meta( $post_id, 'post_solusi_meta_description', $_POST['post_solusi_meta_description'] );
    }
    //description 2
     if ( isset ( $_POST['post_solusi_meta_description2'] ) ) {
    update_post_meta( $post_id, 'post_solusi_meta_description2', $_POST['post_solusi_meta_description2'] );
  }
    if(isset($_POST['post_solusi_use_new_tab'])){
       $use_new_tab = 1;
    }else{
        $use_new_tab = 0;
    }
     update_post_meta($post_id, 'post_solusi_use_new_tab', $use_new_tab);
    if( isset( $_POST['post_solusi_new_meta_url'] ) )
        update_post_meta( $post_id, 'post_solusi_new_meta_url', esc_url_raw( $_POST['post_solusi_new_meta_url'] ) );
  
}
add_action( 'add_meta_boxes', 'post_solusi_new_meta_box_add' );
function post_solusi_new_meta_box_add()
{
    add_meta_box( 'solusi-meta-box-id', 'Solusi Details', 'post_solusi_new_meta_box_cb', 'solusi-alcatel', 'normal', 'high' );
}

function post_solusi_new_meta_box_cb(){
  wp_nonce_field(basename(__FILE__), "meta-box-nonce");
   global $post;
   $promo_desc = get_post_meta($post->ID,'post_solusi_meta_description',true);
   $subTitle1 = get_post_meta($post->ID, 'subtitle1_solusi_meta_box', true);
   $subTitle2 = get_post_meta($post->ID, 'subtitle2_solusi_meta_box', true);
   $subTitle3 = get_post_meta($post->ID, 'subtitle3_solusi_meta_box', true);
   $promo_desc2 = get_post_meta($post->ID, 'post_solusi_meta_description2',true);
   $use_new_tab = get_post_meta($post->ID, 'post_solusi_use_new_tab', true);
   $value_txt = get_post_meta($post->ID, 'post_solusi_new_meta_url', true);

   $description = !empty($promo_desc) ? esc_html($promo_desc) : '';
   $subTtl1 = !empty($subTitle1) ? esc_html($subTitle1) : '';
   // print_r($subTtl1);die();
   $subTtl2 = !empty($subTitle2) ? esc_html($subTitle2) : '';
   $subTtl3 = !empty($subTitle3) ? esc_html($subTitle3) : '';
   $subDesc = !empty($promo_desc2) ? esc_html($promo_desc2) : '';
   $txt = !empty($value_txt) ? esc_url($value_txt ) : '';

   wp_nonce_field( 'post_solusi_new_meta_box_nonce', 'meta_box_nonce' );
       //so, dont ned to use esc_attr in front of get_post_meta 
  ?>
  <p>
  <label>Sub Title 1</label>
  <input type="text" name="subtitle1_solusi_meta_box" id="subtitle1_solusi_meta_box" value="<?= $subTtl1; ?>" style="width: 100%;"></p>
  <p>
  <label>Sub Title 2</label>
  <input type="text" name="subtitle2_solusi_meta_box" id="subtitle2_solusi_meta_box" value="<?= $subTtl2; ?>" style="width: 100%;"></p>
  <p><label>Sub Title 3</label>
  <input type="text" name="subtitle3_solusi_meta_box" id="subtitle3_solusi_meta_box" value="<?= $subTtl3; ?>" style="width: 100%;"></p>  
  <p>
    <p> 
        <input type="checkbox" id="post_solusi_use_new_tab" name="post_solusi_use_new_tab" value="1" <?= ($use_new_tab == 0)? "" : "checked" ; ?> > <label for="post_solusi_use_new_tab">Use New Tab? </label><br>
        <label for="post_solusi_new_meta_url">Link Cari Promosi</label>
        <input type="text" name="post_solusi_new_meta_url" id="post_solusi_new_meta_url" value="<?php echo $value_txt; ?>" placeholder="Masukan link promosi produk terkait" style="width: 100%;" />
    </p>
  <?php 
    // $promo_desc=  get_post_meta($_GET['post'], 'post_solusi_meta_description' , true ) ;
    // wp_editor( htmlspecialchars_decode($promo_desc), 'post_solusi_meta_description', $settings = array('textarea_name'=>'post_solusi_meta_description') );
     $promo_desc = get_post_meta( $post->ID, 'post_solusi_meta_description', false );
  wp_editor( $promo_desc[0], 'post_solusi_meta_description' );
    ?>
    </p><p>
  <?php 
   $promo_desc2 = get_post_meta( $post->ID, 'post_solusi_meta_description2', false );
  wp_editor( $promo_desc2[0], 'post_solusi_meta_description2' );
  ?></p>
    
  <?php  
}

//SOLUSI DESCRIPTION PRODUK
add_action( 'add_meta_boxes', 'post_produk_solusi_new_meta_box_add' );
function post_produk_solusi_new_meta_box_add()
{
    add_meta_box( 'produk-solusi-meta-box-id', 'Produk Overview', 'post_produk_solusi_new_meta_box_cb', 'solusi-alcatel', 'normal', 'high' );
}
function post_produk_solusi_new_meta_box_cb(){
  wp_nonce_field(basename(__FILE__), "meta-box-nonce");
   global $post;
   $titleProduk1 = get_post_meta($post->ID, 'produk1_solusi_meta_box', true);
   $titleProduk2 = get_post_meta($post->ID, 'produk2_solusi_meta_box', true);
   $titleProduk3 = get_post_meta($post->ID, 'produk3_solusi_meta_box', true);
   $produk_desc = get_post_meta($post->ID,'produk_solusi_meta_description',true);
   $produk_new_tab = get_post_meta($post->ID, 'produk_solusi_use_new_tab', true);
   $produk_txt = get_post_meta($post->ID, 'produk_solusi_new_meta_url', true);

   $description = !empty($produk_desc) ? esc_html($produk_desc) : '';
   $subTtl1 = !empty($titleProduk1) ? esc_html($titleProduk1) : '';
   // print_r($subTtl1);die();
   $subTtl2 = !empty($titleProduk2) ? esc_html($titleProduk2) : '';
   $subTtl3 = !empty($titleProduk3) ? esc_html($titleProduk3) : '';
   $txt = !empty($produk_txt) ? esc_url($produk_txt ) : '';

   wp_nonce_field( 'produk_solusi_new_meta_box_nonce', 'produk_meta_box_nonce' );
       //so, dont ned to use esc_attr in front of get_post_meta 
  ?>
  <p>
  <label>Produk Title 1</label>
  <input type="text" name="produk1_solusi_meta_box" id="produk1_solusi_meta_box" value="<?= $subTtl1; ?>" style="width: 100%;"></p>
  <p>
  <label>Produk Title 2</label>
  <input type="text" name="produk2_solusi_meta_box" id="produk2_solusi_meta_box" value="<?= $subTtl2; ?>" style="width: 100%;"></p>
  <p><label>Produk Title 3</label>
  <input type="text" name="produk3_solusi_meta_box" id="produk3_solusi_meta_box" value="<?= $subTtl3; ?>" style="width: 100%;"></p>  
  <p>
  <?php 
    $promo_desc = get_post_meta( $post->ID, 'produk_solusi_meta_description', false );
  wp_editor( $promo_desc[0], 'produk_solusi_meta_description' );
    ?>
    </p>
   <!--  <p> 
        <input type="checkbox" id="produk_solusi_use_new_tab" name="produk_solusi_use_new_tab" value="1" <?= ($use_new_tab == 0)? "" : "checked" ; ?> > <label for="produk_solusi_use_new_tab">Use New Tab? </label><br>
        <label for="produk_solusi_new_meta_url">Link Url</label>
        <input type="text" name="produk_solusi_new_meta_url" id="produk_solusi_new_meta_url" value="<?php echo  $txt; ?>" placeholder="Masukan Url" style="width: 100%;" />
    </p> -->
  <?php  
}
add_action('save_post','wp_insert_produk_solusi');

function wp_insert_produk_solusi($post_id){
    global $post;
     // Bail if we're doing an auto save
    if(defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) return;

    // if our nonce isn't there, or we can't verify it, bail
    if( !isset( $_POST['produk_meta_box_nonce'] ) || !wp_verify_nonce( $_POST['produk_meta_box_nonce'], 'produk_solusi_new_meta_box_nonce' ) ) return;

     // if our current user can't edit this post, bail
    if( !current_user_can( 'edit_post' ) ) return;

    $slug = "solusi-alcatel";
    if($slug != $post->post_type) return;

    if(isset($_POST['produk1_solusi_meta_box'])){
        update_post_meta($post_id,'produk1_solusi_meta_box',esc_html($_POST['produk1_solusi_meta_box']));
    }
     if(isset($_POST['produk2_solusi_meta_box'])){
        update_post_meta($post_id,'produk2_solusi_meta_box',esc_html($_POST['produk2_solusi_meta_box']));
    }
     if(isset($_POST['produk3_solusi_meta_box'])){
        update_post_meta($post_id,'produk3_solusi_meta_box',esc_html($_POST['produk3_solusi_meta_box']));
    }
    // Make sure your data is set before trying to save it
     // if (!empty($_POST['post_solusi_meta_description']))
     //  {
     //    $datta=htmlspecialchars($_POST['post_solusi_meta_description']);
     //    update_post_meta($post_id, 'post_solusi_meta_description', $datta );
     //  }
    if ( isset ( $_POST['produk_solusi_meta_description'] ) ) {
    update_post_meta( $post_id, 'produk_solusi_meta_description', $_POST['produk_solusi_meta_description'] );
    }
   
    if(isset($_POST['produk_solusi_use_new_tab'])){
       $use_new_tab = 1;
    }else{
        $use_new_tab = 0;
    }
     update_post_meta($post_id, 'produk_solusi_use_new_tab', $use_new_tab);
    if( isset( $_POST['produk_solusi_new_meta_url'] ) )
        update_post_meta( $post_id, 'produk_solusi_new_meta_url', esc_url_raw( $_POST['produk_solusi_new_meta_url'] ) );
  
}


// NOTIF SOLUSI MENU
function my_update_notice() {
    $screen = get_current_screen();
    //If not on the screen with ID 'edit-post' abort.
    if( $screen->id !='solusi-alcatel' ){
        return;
    }
    ?>
    <div class="updated notice">
      <p>
        <strong>Notifikasi :</strong>
        <ol>
          <li><?php _e('Size Minimum Featured Image : 600 x 256 pixels');?></li>
          <li><?php _e('Size Minimum Image Large    : 1300 x 1000 pixels');?></li>
        </ol>
      </p>
    </div>
    <?php
}
add_action( 'admin_notices', 'my_update_notice' );
// END NOTIF SOLUSI
// NOTIF PROMOSI MENU
function my_update_notice2() {
    $screen = get_current_screen();
    //If not on the screen with ID 'edit-post' abort.
    if( $screen->id !='promosi-alcatel' ){
        return;
    }
    ?>
    <div class="updated notice">
      <p>
        <strong>Notifikasi :</strong>
        <ol>
          <li><?php _e('Size Minimum Featured Image : 600 x 256 pixels');?></li>
          <li><?php _e('Size Minimum Image Produk   : 140 x 140 pixels');?></li>
          <li><?php _e('Size Image Minimum untuk Image Large    : 1300 x 1000 pixel');?></li>
        </ol>
      </p>
    </div>
    <?php
}
add_action( 'admin_notices', 'my_update_notice2' );
// END NOTIF PROMO
// NOTIF SLIDERS MENU
function my_update_notice3() {
    $screen = get_current_screen();
    //If not on the screen with ID 'edit-post' abort.
    if( $screen->id !='slider_home' ){
        return;
    }
    ?>
    <div class="updated notice">
      <p>
        <strong>Notifikasi :</strong>
        <ol>
          <li><?php _e('Size Minimum Featured Image : 1360 × 722 pixels');?></li>
          <li><?php _e('Size Minimum Mobile Image   : 768 × 512 pixels');?></li>
        </ol>
      </p>
    </div>
    <?php
}
add_action( 'admin_notices', 'my_update_notice3' );
// END NOTIF SLIDERS
// NOTIF POST MENU
function my_update_notice4() {
    $screen = get_current_screen();
    //If not on the screen with ID 'edit-post' abort.
    if( $screen->id !='post' ){
        return;
    }
    ?>
    <div class="updated notice">
      <p>
        <strong>Notifikasi :</strong>
        <ol>
          <li><?php _e('Size Minimum Featured Image : 1360 × 722 pixels');?></li>
        </ol>
      </p>
    </div>
    <?php
}
add_action( 'admin_notices', 'my_update_notice4' );
// END NOTIF POST