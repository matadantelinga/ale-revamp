<?php
if( ! class_exists('WP_List_Table')){
  require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php');
}

class WP_formcontact extends WP_List_Table{
  function get_columns(){
      $column = array(
        'cb'=> '<input type="checkbox" />'
        ,'date_created' => 'Tanggal Masuk'
        ,'name'=> 'Nama'
        ,'perusahaan' => 'Perusahaan'
        ,'email'=> 'Email'
        ,'telepon' => 'No. Telepon'
        ,'phone' => 'Nomor Telepon'
        ,'produk'=>'Produk'
        // ,'pesan'=>'Pesan'
      );
      return $column;
  }
  function get_sortable_columns(){
      $sortable = array(
        'date_created'=> array('dateCreated',true),
        'name' => array('name',true),
        'perusahaan' => array('perusahaan',true),
        'email' => array('email',true),
        'telepon' => array('telepon',true),
        'phone' => array('phone',true),
        'produk' => array('produk', true)
        // 'pesan' => array('pesan',true)
      );
  }
  function prepare_items(){
      global $wpdb;
      $table_name = $wpdb->prefix."contact_us";
      $query = "SELECT * FROM ". $table_name;
      print_r($_POST['s']);
      if(isset($_POST['s'])){
          $cari = $_POST['s'];
          $query = "WHERE name like '%$cari%' perusahaan like '%$cari%' OR email like '%$cari%' OR telepon like '%$cari%' OR phone '%$cari%' OR produk '%$cari%' OR pesan like '%$cari%'";
      }
      $orderby = !empty($_GET['orderby']) ? $_GET['orderby'] : 'date_created';
      $order   = !empty($_GET['order']) ? $_GET['order'] : 'desc';
      $query  .= " ORDER BY $orderby $order";

    $perpage = $this->get_items_per_page('data_per_page', 20);
    $currentpage = $this->get_pagenum();
    $totalitems = $wpdb->query($query);
    $offset = ($currentpage-1)*$perpage;
    $query .= " LIMIT $offset, $perpage";
    
    $rows = $wpdb->get_results($query);
    
    $this->set_pagination_args( array(
      'total_items' => $totalitems,
      'per_page' => $perpage
    ) );
    
    $columns  = $this->get_columns();
    $hidden   = array();
    $sortable = $this->get_sortable_columns();
    $this->_column_headers = array($columns, $hidden, $sortable);
    
    $this->items = $rows;
  }

  function column_default($item, $column_name) {
    return $item->$column_name;
  }

 function column_name($item){
    $actions = array(
      'edit' => sprintf('<a href="?page=%s&id=%s">View Data</a>','contact_edit',$item->id)
      ,'delete' => sprintf('<a href="?page=%s&action=delete&id=%s">Hapus</a>','contact_mainmenu',$item->id),
    );
    
    return sprintf('%1$s %2$s', $item->name, $this->row_actions($actions));
  }

  function column_cb($item){
    return sprintf('<input type="checkbox" name="id[]" value="%s" />',$item->id);
  }
  function get_bulk_actions(){
    $actions = array(
      'delete' => 'Hapus'
    );
    return $actions;
  }
}

function contact_edit(){
  global $wpdb;
  $table_name = $wpdb->prefix . "contact_us";
  $data = $wpdb->get_results($wpdb->prepare("SELECT * FROM ".$table_name." WHERE id=%s",$_GET['id']));

  foreach ($data as $value) {
      $id = $value->id;
      $name = $value->name;
      $company = $value->perusahaan;
      $email = $value->email;
      $telepon = $value->telepon;
      $phone = $value->phone;
      $produk = $value->produk;
      $pesan = $value->pesan;
      $tgl_masuk = $value->date_created;
  }
  ?>
<div class="wrap">

  <h2>Data Detail Kontak Form</h2>
  <form method="post" action="?page=contact_mainmenu">
    <input type="hidden" name="id" value="<?= $id; ?>">
    <table class="form-table">
      <tbody>
       <tr>
        <th>Tanggal Masuk</th>
        <td><input type="text" name="dateCreated" value="<?= $tgl_masuk;?>" style="width: 100%" disabled></td>
      </tr>
      <tr>
        <th><label>Nama</label></th>
        <td><input type="text" name="inputName" value="<?= $name; ?>" style="width: 100%" disabled></td>
      </tr>
      <tr>
        <th><label>Perusahaan</label></th>
        <td><input type="text" name="inputCompany" value="<?= $company; ?>" style="width: 100%" disabled></td>
        <!-- <td><input type="text" name="alamat" value="<?php echo $wilayah; ?>" size="40"></td> -->
      </tr>
      <tr>
        <th><label>Email</label></th>
        <td><input type="text" name="inputEmail" value="<?= $email; ?>" style="width: 100%" disabled></td>
      </tr>
       <tr>
        <th><label>No. Telepon</label></th>
        <td><input type="text" name="inputTlp" value="<?= $telepon; ?>" style="width: 100%" disabled></td>
      </tr>
       <tr>
        <th><label>No. Handphone</label></th>
        <td><input type="text" name="inputPhone" value="<?= $phone; ?>" style="width: 100%" disabled></td>
      </tr>
      <tr>
        <th><label>Produk</label></th>
        <td><input type="text" value="<?= $produk; ?>" style="width: 100%" disabled></td>
      </tr>
      <tr>
        <th><label>Pesan</label></th>
        <td><textarea name="inputMsg" value="" rows="4" style="width: 100%" disabled><?= $pesan; ?></textarea></td>
      </tr>
      </tbody>
    </table>
     <p><a href="<?= admin_url('admin.php?page=contact_mainmenu'); ?>" class="button button-primary" value="View Data">Lihat Semua Data</a></p>
   <!--  <p><input type="submit" name='edit' class="button button-primary" value="View Data" disabled></p> -->
  </form>
  </div>
  <?php 
}

function contact_input(){
?>
  <div class="wrap">
  <h2>Tambah Data Kontak Form</h2>
  <form method="post" action="?page=contact_mainmenu">
    <table class="form-table" >
      <tbody>
      <tr>
        <th>Tanggal Masuk</th>
        <td><input type="text" class="form-control" name="dateCreated"></td>
      </tr>
      <tr>
        <th><label>Name</label></th>
        <td><input type="text" class="form-control" name="inputName"></td>
      </tr>
      <tr>
        <th><label>Perusahaan</label></th>
        <td><input type="text" class="form-control" name="inputCompany" size="40"></td>
      </tr>
      <tr>
        <th><label>Email</label></th>
        <td><input type="text" class="form-control" name="inputEmail" size="40"></td>
      </tr>
      <tr>
        <th><label>Telepon</label></th>
        <td><input type="text" class="form-control" name="inputTlp" size="40"></td>
      </tr>
      <tr>
        <th><label>Phone Number</label></th>
        <td><input type="text" class="form-control" name="inputPhone" size="40"></td>
      </tr>
      <tr>
        <th><label>Produk</label></th>
        <td><select name="produk" class="form-control selectpicker" >
              <option value="" name="produk" class="option-top">--Pilih Produk--</option>
              <option <?php if($produk == 'Web Development') echo 'selected'; ?> value="Web Development">Web Development</option>
              <option <?php if($produk == 'Google AdWord') echo 'selected'; ?> value="Google AdWord">Google AdWord</option>
              <option <?php if($produk == 'Google Analytics') echo 'selected';?> value="Google Analytics">Google Analytics</option>
              <option <?php if($produk == 'SEO') echo 'selected';?> value="SEO">SEO</option>
            </select>
        </td>
      </tr>
       <tr>
        <th><label>Pesan</label></th>
        <td><textarea class="form-control" class="form-control" name="inputMsg" rows="5" id="comment" placeholder="Pesan"></textarea></td>
      </tr>
      </tbody>
    </table>
    <p><input type="submit" name='tambah' class="button button-primary" value="Tambah Data"></p>
  </form>
  </div>
<?php
}

function contact_mainmenu(){
    global $wpdb;
    $table_name = $wpdb->prefix. "contact_us";

    $message = "";

    if(isset($_POST['tambah'])){
    $tambah = $wpdb->insert(
      $table_name, 
      array('date_created'=>$_POST['dateCreated'],'name'=>$_POST['inputName'], 'perusahaan'=>$_POST['inputCompany'], 'email'=>$_POST['inputEmail'], 'telepon'=>$_POST['inputTlp'], 'phone'=>$_POST['inputPhone'], 'produk'=>$_POST['produk'],'pesan'=>$_POST['inputMsg']),
      array('%s','%s','%s','%s','%s','%s','%s')
    );
    if($tambah) $message = "Data berhasil ditambahkan";
  }elseif(isset($_POST['edit'])){
    $update = $wpdb->update(
      $table_name, 
      array('date_created'=>$_POST['dateCreated'],'name'=>$_POST['inputName'],'perusahaan'=>$_POST['inputCompany'], 'email'=>$_POST['inputEmail'], 'telepon'=>$_POST['inputTlp'], 'phone'=>$_POST['inputPhone'], 'produk'=>$_POST['produk'],'pesan'=>$_POST['inputMsg']),
      array('id'=>$_POST['id']),
      array('%s','%s','%s','%s','%s','%s','%s'),
      array('%s')
    );
    if($update) $message = "Data berhasil diedit";
  }elseif(isset($_POST['action']) or isset($_POST['action2'])){
    if($_POST['action']=="delete" or $_POST['action2']=="delete"){
      $variable = $_POST['id'];
      foreach ($variable as $key) {
        $wpdb->query($wpdb->prepare("DELETE FROM ".$table_name." WHERE id=%s".$key));
      }
      $message = "Data berhasil dihapus";
    }
  }elseif(isset($_GET['action'])){
    if($_GET['action']=="delete"){
      $hapus = $wpdb->query($wpdb->prepare("DELETE FROM ".$table_name." WHERE id=%s",$_GET['id']));
      if($hapus) $message = "Data berhasil dihapus";
    }
  }

//Instansiasi object from WP_List_Table
    $formcontact = new WP_formcontact();
    echo '<div class="wrap">
          <h2>TABLE KONTAK FORM</h2>';
          // <br><a href="?page=contact_input" class="add-new-h2">Tambah Data</a>';
    if($message!=""){
      echo '<div id="message" class="updated notice is-dismissable">
              <p>'.$message.'</p>
              <button type="button" class="notice-dismiss"></button>
            </div>';
    }
    $formcontact->prepare_items();
    echo '<form method="post">';
      $formcontact->search_box('search','search_id');
    echo '</form>';
    echo '<form method="post">';
      $formcontact->display();
    echo '</form></div>';
}

add_action( 'admin_menu', 'formcontact_menu' );
function formcontact_menu(){
    $hook = add_menu_page('Kontak Form','Kontak Form', 'activate_plugins','contact_mainmenu','contact_mainmenu');
    add_action("load-$hook",'contact_options');

    // add_submenu_page('contact_mainmenu', 'Tambah Data', 'Tambah Data', 'administrator', 'contact_input', 'contact_input');
  add_submenu_page(null, 'Edit Data', 'Edit Data', 'administrator', 'contact_edit', 'contact_edit');
}
function contact_options() {
  global $contactus;
  $option = 'per_page';
  $args = array(
         'label' => 'Data Contact Us',
         'default' => 2,
         'option' => 'data_per_page'
         );
  add_screen_option( $option, $args );
  
  $contactus = new WP_formcontact;
}

add_filter('set-screen-option', 'contactus_set_options', 10,3); //menampilkan Screen Options
function contactus_set_options($status, $option, $value){
  return $value;
}

// add_action('save_post','wp_insert_contact_us',10,3);
function wp_insert_contact_us(){
      $err = '';
      $success = '';
      global $wpdb;
      $table_name = $wpdb->prefix. "contact_us";
      if(isset($_POST['task']) && $_POST['task'] === 'submit' ){
          $name = $wpdb->escape(trim($_POST['inputName']));
          $company = $wpdb->escape(trim($_POST['inputCompany']));
          $email = $wpdb->escape(trim($_POST['inputEmail']));
          $tlp = $wpdb->escape(trim($_POST['inputTlp']));
          $phone = $wpdb->escape(trim($_POST['inputPhone']));
          $produk = $wpdb->escape(trim($_POST['inputProduk']));
          $message = $wpdb->escape(trim($_POST['inputMsg']));
          if($name ==""|| $company=="" || $email=="" || $tlp=="" || $phone=="" || $produk=="" || $message==""){
              $err = 'Please don\'t leave the required fields.';
              echo $err;die();
          }else if(!filter_var($email, FILTER_VALIDATE_EMAIL)){
              $err = 'Invalid email address.';
              echo $err;die();
          }else if(email_exists($email)){
              $err = 'Email already exist.';
              echo $err;die();
          }else{
            echo 'berhasil';die();
              $tambah = $wpdb->insert(  
                $table_name, 
                array('name'=>$_POST['inputName'], 'perusahaan'=>$_POST['inputCompany'], 'email'=>$_POST['inputEmail'], 'telepon'=>$_POST['inputTlp'], 'phone'=>$_POST['inputPhone'], 'produk'=>$_POST['produk'],'pesan'=>$_POST['inputMsg']),
                array('%s','%s','%s','%s','%s','%s','%s'));
                if($tambah) $message = "Data berhasil ditambahkan";
          }
      }
}

function contact_us_view(){
    ?>
      <form data-toggle="validator" role="form" action="?page=wp_insert_contact_us">
          <div class="form-group">
              <input type="text" class="form-control" name="inputName" id="inputName" placeholder="Nama" required><?= $err; ?>
          </div>
          <div class="form-group">
              <input type="text" class="form-control" name="inputCompany" id="inputCompany" placeholder="Perusahaan" required><?= $err; ?>
          </div>
          <div class="form-group">
              <input type="email" class="form-control" name="inputEmail" id="inputEmail" placeholder="Email" data-error="Sorry, that email address is invalid" required><?= $err; ?>
              <div class="help-block with-errors"></div>
          </div>
          <div class="form-group">
              <input type="text" class="form-control" name="inputTlp" id="inputTlp" placeholder="No. Telepon" required><?= $err; ?>
          </div>
          <div class="form-group">
              <input type="text" class="form-control" name="inputPhone" id="inputPhone" placeholder="Phone Number" required><?= $err; ?>
          </div>
          
          <select name="select" class="form-control selectpicker" >
              <option value="" name="inputProduk" class="option-top">Produk</option>
              <option <?php if($produk == 'Web Development') echo 'selected'; ?> value="Web Development">Web Development</option>
              <option <?php if($produk == 'Google AdWord') echo 'selected'; ?> value="Google AdWord">Google AdWord</option>
              <option <?php if($produk == 'Google Analytics') echo 'selected';?> value="Google Analytics">Google Analytics</option>
              <option <?php if($produk == 'SEO') echo 'selected';?> value="SEO">SEO</option>
          </select><?= $err; ?>

          <textarea class="form-control" name="inputMsg" rows="5" id="comment" placeholder="Pesan"></textarea>

         <!--  <div class="wrap-captcha">
              <img src="wp-content/assets/img/captcha.jpg" alt="">
          </div> -->

          <div class="btn-contact | form-group">
              <button type="submit" class="btn btn-warning">kirim</button>
          </div>
          <input type="hidden" name="task" value="submit">
      </form>
    <?php 
}