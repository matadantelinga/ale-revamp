
    <article>
        <div class="wrap-content-oxo | oxo-solusi-list">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="top-title | oxo-title | wow fadeInDown">
                            <h3><?= $SubTitle1; ?><br><span><?= $SubTitle2; ?></span></h3>
                            <div class="row">
                            <div class="col-sm-6">
                            <p class="sub-left-content | sub-oxo"><?= $SubTitle3;?></p>
                            </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 | col-sm-6">
                        <div class="left-content | wow fadeInUp">
                            <p class="txt-left-content | txt-oxo"><?= $Desc1; ?>
                            </p>
                            <a class="btn-content | oxo-btn" href="<?= get_permalink($get_solusi[$i]->ID); ?>">selengkapnya<i class="fa fa-long-arrow-right"></i></a>
                        </div>
                    </div>
                    <div class="col-md-6 | col-sm-6">
                        <div class="right-content | wow fadeInRight">
                            <img src="<?= $img1; ?>" alt="<?= $getTitle;?>" title="">
                        </div>
                    </div>
                    <!-- <?php //endwhile; ?> -->
                      <div class="clearfix"></div>
                </div>
            </div>
        </div>
  
    </article>


