<?php 
/**
 * The template for displaying all single posts and attachments
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */

get_header('nav');  ?>
<!-- DETAIL PRODUCT -->
<section>
<?php 
    if(have_posts()):
    $i=0;
    while(have_posts()): the_post();
        $SubTitle1 = get_post_meta($post->ID,'subtitle1_promosi_meta_box',true);
        $SubTitle2 = get_post_meta($post->ID,'subtitle2_promosi_meta_box',true);
        $SubTitle3 = get_post_meta($post->ID,'subtitle3_promosi_meta_box',true);
        $SubTitle4 = get_post_meta($post->ID,'subtitle4_promosi_meta_box',true);
        $SubTitle5 = get_post_meta($post->ID,'subtitle5_promosi_meta_box',true);
        $SubTitle6 = get_post_meta($post->ID,'subtitle6_promosi_meta_box',TRUE);
        $SubTitle7 = get_post_meta($post->ID,'subtitle7_promosi_meta_box',TRUE);
        $SubTitle8 = get_post_meta($post->ID,'subtitle8_promosi_meta_box',TRUE);
        $LinkUrl1 = get_post_meta($post->ID,'url1_promosi_new_meta_url',true);
        $LinkUrl2 = get_post_meta($post->ID,'url2_promosi_new_meta_url',true);
        $LinkUrl3 = get_post_meta($post->ID,'url3_promosi_new_meta_url',true);
        $Desc = get_post_meta($post->ID,'post_promosi_meta_description',true);
        $Desc2 = get_post_meta($post->ID,'post_promosi_meta_description2',true);
        $LinkPromo = get_post_meta( $post->ID, 'post_promosi_new_meta_url', true);
        $Image1 = get_post_meta( $post->ID, 'img1_upload_meta_box', true );
        $Image2 = get_post_meta( $post->ID, 'img2_upload_meta_box', true );
        $Image3 = get_post_meta( $post->ID, 'img3_upload_meta_box', true );
        $img = get_the_post_thumbnail_url($post);
        $img1 = wp_get_attachment_image_src($Image1);
        $img2 = wp_get_attachment_image_src($Image2);
        $img3 = wp_get_attachment_image_src($Image3);
        // print_r($img1);die();
        $link1 = get_permalink($post->post_title);
        $oxoTitle = $SubTitle1.''.$SubTitle2;

?>
 <section>
    <article>
        <div class="wrap-detail-produk">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="title-detail-produk | animated | bounceInDown">
                             <img src="<?= site_url();?>/wp-content/assets/img/promo-detail.png" alt="<?= $oxoTitle;?>">
                            <h3><?= $SubTitle1."&nbsp;".$SubTitle2; ?></h3>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="txt-detail-produk | animated | bounceInLeft">
                           <!--  <h3 class="txt-user"><?= $SubTitle3; ?> user</h3> -->
                            <h3 class="txt-user"><?= $SubTitle3; ?></h3>
                            <p class="sub-txt-user">
                                 <?= $SubTitle5; ?>
                            </p>
                            <p><?= $Desc; ?></p>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="img-detail-produk">
                            <div class="wrap-img-detail">
                                <div class="main-img-detail | animated | zoomIn">
                                   <img src="<?= $img; ?>" alt="<?= $oxoTitle;?>">
                                </div>

                                <div class="btn-get-promo">
                                    <a class="get-promo" href="#hubungi-kami">hubungi kami sekarang!</a>
                                </div>
                                <div class="btn-lihat-promo">
                                    <a href="<?= site_url();?>/promo/?page=oxo-connect-r2">Lihat promo lainnya
                                    <i class="fa fa-chevron-right"></i>
                                    </a>
                                </div>
                                <?php if(!empty($LinkUrl1) && !empty($img1[0]) && !empty($SubTitle6)):?>
                                <div class="sub-img-detail | animated | bounceInUp">
                                    <a href="<?= $LinkUrl1; ?>">
                                       <img src="<?= $img1[0]; ?>" alt="<?= $SubTitle6; ?>">
                                        <p><?= $SubTitle6; ?></p>
                                    </a>
                                </div> 
                                <?php endif; ?>
                                <?php if(!empty($LinkUrl2) && !empty($img2[0]) && !empty($SubTitle7)):?>
                                <div class="sub-img-detail | animated | bounceInUp">
                                   <a href="<?= $LinkUrl2; ?>">
                                       <img src="<?= $img2[0]; ?>" alt="<?= $SubTitle7; ?>">
                                        <p><?= $SubTitle7; ?></p>
                                    </a>
                                </div>
                                <?php endif; ?>
                                <?php if(!empty($LinkUrl3) && !empty($SubTitle8) && !empty($img3[0])):?>
                                <div class="sub-img-detail | animated | bounceInUp">
                                    <a href="<?= $LinkUrl3; ?>">
                                        <img src="<?= $img3[0]; ?>" alt="<?= $SubTitle8; ?>">
                                        <p><?= $SubTitle8; ?></p>
                                    </a>
                                </div>
                            <?php endif; ?>
                            </div>

                            <!-- carousel-xs -->
                            <div class="carousel-img-detail">
                                <div class="arrow-img-promo">
                                    <a class="control-img-promo" href="#carousel-img-promo" data-slide="prev">
                                        <i class="fa fa-4x fa-angle-left"></i>
                                    </a>
                                </div>
                                
                                <div id="carousel-img-promo" class="carousel-img-promo slide carousel-sync" data-ride="carousel" data-pause="false">
                                    <div class="carousel-inner | carousel-img-top">
                                        <div class="item | item-img-promo | active">
                                            <div class="sub-img-detail">
                                                <a href="<?= $LinkUrl1; ?>">
                                                   <img src="<?= $img1[0]; ?>" alt="<?= $SubTitle6; ?>">
                                                    <p><?= $SubTitle6; ?></p>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="item | item-img-promo">
                                            <div class="sub-img-detail">
                                                <a href="<?= $LinkUrl2; ?>">
                                                    <img src="<?= $img2[0]; ?>" alt="<?= $SubTitle7; ?>">
                                                    <p><?= $SubTitle7; ?></p>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="item | item-img-promo">
                                            <div class="sub-img-detail">
                                                <a href="<?= $LinkUrl3; ?>">
                                                    <img src="<?= $img3[0]; ?>" alt="<?= $SubTitle8; ?>">
                                                    <p><?= $SubTitle8; ?></p>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                                <!-- NEXT BTN -->
                                <div class="arrow-img-promo">
                                    <a class="control-img-promo" href="#carousel-img-promo" data-slide="next">
                                        <i class="fa fa-4x fa-angle-right"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </article>
</section>
<?php endwhile; endif; ?>
<?php get_template_part('promo_form_page'); ?>
<?php get_footer('more'); ?>
    <!-- /.container -->