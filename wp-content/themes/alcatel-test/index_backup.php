<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

get_header('nav'); 

?>
<section>
    <article>
        <div class="wrap-blogview">
            <div class="container">
                <div class="row | title-blogview">
                    <div class="col-lg-3 | col-md-3 | col-sm-4 | col-xs-12 | pull-right">
                        <div class="btn-blog-list | btn-blogview">
                            <a href="bloglist.html">blog list</a>
                        </div>
                    </div>
                </div>
;
                <!-- BLOG CONTENT-->
                <?php while(have_posts()) : the_post() ?>
                <div class="row | padd15">
                    <div class="wrap-content-blogview | clearfix">
                        <div class="col-md-10 | col-sm-10 | nopadding">
                            <div class="img-blogview">
                                <img src="<?= get_the_post_thumbnail(); ?>" >
                            </div>
                            <div class="tag-blog | tag-blogview">
                                <ul>
                                    <li><a href="#">MB</a></li>
                                    <li><a href="#">User Experience</a></li>
                                    <li><a href="#">Cloud</a></li>
                                    <li><a href="#">Vision</a></li>
                                    <li><a href="#">Insight And Analytics</a></li>
                                </ul>
                            </div>
                            <div class="txt-blogview">
                                <h3 class="title-blogview">
                                    <?php the_title(); ?>
                                </h3>
                                <p><span><?php the_content();?></span></p>
                            </div>
                        </div>
                        <div class="col-md-2 | col-sm-2">
                            <div class="date-blog | date-blogview | pull-left">
                                <p class="bdate">10</p>
                                <p class="bmonth">nov</p>
                                <p class="byear">2016</p>
                            </div>
                            <div class="share-blogview">
                                <p>bagikan</p>
                                <ul>
                                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                    <li><a href="#"><i class="fa fa-youtube-play"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <?php endwhile; ?>
            </div><!-- end container -->
        </div>
    </article>
</section>

<!-- ARTIKEL TERKAIT -->
<section>
    <article>
        <div class="wrap-artikel-terkait">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h3 class="main-artikel-terkait">artikel terkait</h3>
                    </div>
                </div>
               
                <div class="row">
                 <?php 
                   $args = array(
                                    'posts_per_page' => 10
                                   ,'category_name' => 'Blog'
                                   ,'orderby'       => 'date'
                                   ,'order'         => 'DESC'
                                   ,'post_type'     => 'post'
                                   ,'post_status'   => 'publish'
                                );                     
                            $get_post = get_posts($args);
                            if($get_post):
                                $i=0;
                                for($i=1; $i<3; $i++):
                ?>
                    <div class="col-md-6 | col-sm-6">
                        <div class="artikel-terkait">
                            <div class="col-lg-4 | col-md-4 | img-artikel-terkait | nopadding">
                                <img src="<?= get_the_post_thumbnail_url($get_post[$i]); ?>" alt="">
                            </div>
                            <div class="col-lg-5 | col-md-5 | title-artikel-terkait">
                                <h3><a href="<?= get_permalink($get_post[$i]->ID ); ?>"><?= $get_post[$i]->post_title; ?></a></h3>
                            </div> 
                            <div class="col-lg-2 | col-md-2 | date-blog | date-artikel-terkait | pull-left">
                                <p class="bdate"><?= get_the_date('d',$get_post[$i]->ID); ?></p>
                                <p class="bmonth"><?= get_the_date('M',$get_post[$i]->ID); ?></p>
                                <p class="byear"><?= get_the_date('Y',$get_post[$i]->ID); ?></p>
                            </div>
                        </div>
                    </div>
                     <?php endfor; endif; ?>
                </div>
            </div>
        </div>
    </article>
</section>

<?php get_footer(); ?>
