<?php 
/**
 * The template for displaying all single posts and attachments
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */
get_header('nav');  ?>
<section>
<?php 
$doc=strlen(trim($doc['url']));
if(have_posts()):
while(have_posts()): the_post();

        $SubTitle1 = get_post_meta($post->ID,'subtitle1_solusi_meta_box',true);
        $SubTitle2 = get_post_meta($post->ID,'subtitle2_solusi_meta_box',true);
        $SubTitle3 = get_post_meta($post->ID,'subtitle3_solusi_meta_box',true);
        $Desc1 = get_post_meta($post->ID,'post_solusi_meta_description',true);
        $Desc2 = get_post_meta($post->ID,'post_solusi_meta_description2',true);
        $LinkPromo = get_post_meta( $post->ID, 'post_solusi_new_meta_url', true);
        // print_r($LinkPromo);die();
        $img1 = get_the_post_thumbnail_url($post);
        $link1 = get_permalink($post->post_title);
        $getTitle = $SubTitle1.''.$SubTitle2;
        // print_r($link1);
        $doc = get_post_meta(get_the_ID(), 'wp_custom_attachment', true);
?>
    <article>
        <div class="wrap-solusi-wifi">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="top-title | wifi-title | wifi-detail | wow fadeInDown">
                            <h3><?= $SubTitle1; ?><br><span><?= $SubTitle2; ?></span></h3>
                            <p class="sub-left-content | sub-wifi">
                            <?= $SubTitle3; ?>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 | col-sm-5">
                        <div class="txt-solusi | color-grey | wow fadeInDown">
                            <p class="text-justify"><?=  $Desc1; ?></p>
                        </div>
                    </div>
                    <div class="col-md-6 | col-sm-7">
                        <div class="wrap-img-solusi">
                            <div class="main-img-solusi | img-wifi-solusi |wow fadeInUp | gallery">
                                <div class="wrap-zoom">
                                    <a href="<?= $img1; ?>" rel="prettyPhoto">
                                        <img src="<?= esc_url(get_option('siteurl')) ?>/wp-content/assets/img/icon/icon-zoom.png" alt="zoom">
                                    </a>
                                </div>
                                <a href="<?= $img1; ?>" rel="prettyPhoto">
                                    <img src="<?= $img1; ?>" alt="<?= $getTitle;?>">
                                </a>
                            </div>
                            <!-- <a class="btn-cari-solusi" href="<?= $LinkPromo; ?>">cari promosi</a> -->
                             <a class="btn-cari-solusi" href="<?= site_url();?>/promo/?page=wifi-omniAccess-ap-1101">cari promosi</a>
                            <?php if($doc > 0):  ?>
                            <a class="btn-download-solusi" href="<?php echo $doc['url']; ?>" target="_blank">unduh brosur</a>
                             <?php else: ?>
                            <a class="btn-download-solusi" href="">unduh brosur</a>
                             <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </article>
<!-- </section>   -->

<!-- DETAIL SOLUSI -->
<!-- <section> -->
    <article>
        <div class="wrap-fitur-solusi">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="fitur-solusi">
                           <!--  <h3>keuntungan</h3> -->
                            <?= $Desc2; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </article>
  <!--   <?php //endif; 
  endwhile; endif; ?> -->
</section>
<?php get_template_part('hubungi_kami'); ?>
 <?php get_footer(); ?>