<?php 
function enqueue_admin_scripts() {
    $template_url = get_bloginfo('template_url') . '/js/multi-post-thumbnails-admin.js';
    wp_enqueue_script("featured-image-custom",  $template_url, array('jquery'));
}
