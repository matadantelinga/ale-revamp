<?php 
/**
 * Template Name: Promosi Template
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

get_header('nav');  
?>
<!-- MAIN TITLE -->
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="title-promo">
                <img src="<?= site_url();?>/wp-content/assets/img/promo.png" alt="promo">
            </div>
        </div>
    </div>
</div>
<?php

$args_slide = array('post_type' => 'solusi-alcatel' , 'orderby' => 'menu_order', 'order' => 'asc', 'post_status' => 'publish',
              'date_query' => array(
                   array(
                     'before'    => array(
                       'year'  => date('2000'),
                       'month' => date('12'),
                       'day'   => date('30')
                      ),
                     'inclusive' => true,
                   )
                  )
);
//$args_slide = array('post_type' => 'solusi-alcatel' , 'meta_key'    => 'show_on_promo_page','meta_value'  => 'yes'); 

if(!empty($_GET['page'])){
  $args_slide['name'] = $_GET['page'];
}         
$get_solusi = get_posts($args_slide);
//PC::debug($get_solusi);

global $post;
foreach ($get_solusi as $key => $value) {
  
  $post = $value;
  setup_postdata( $post );
  $template = get_page_template_slug(); //get_post_meta($value->ID, 'solusi_template_meta_box', true); 
  get_template_part('template-parts/promo-parts-'.substr($template, 0, -4));

  wp_reset_postdata();
  
}

// die();

// if (array_key_exists('page', $_GET)){


 
//   $getPostName = get_name_from_posts('publish','solusi-alcatel',$getx);
//    // print_r( $getPostName);die();
//   $getName = $getPostName[0]->post_name;
//   $vTitle = $getPostName[0]->post_title;
//         if($getx==='oxo-connect-r2'){
//           // print_r($vTitle);die();
//           include "get-promo-oxo-connect.php"; 
//           // include "get-promo-product.php";
//         }
//         if($getx==='omniswitch-6350'){
//           // print_r($vTitle);die();
//            include "get-promo-omni-switch.php";
//         }
//         if($getx==='wifi-omniAccess-ap-1101'){
//           // print_r($vTitle);die();
//           include "get-promo-wifi-omniaccess.php";
//         }
   
// }else{
// global $post;
//  // $args_promosi = array( 'post_type' => 'promosi-alcatel' , 'orderby' => 'menu_order', 'order' => 'ASC', 'post_status' => 'publish' );
//  // $get_promosi = get_posts($args_promosi);  
// $getPost = get_id_from_posts('publish','promosi-alcatel');
// global $post_temp;
// $post_temp = array('data'=> array(), 'active' => -1);
// // print_r($post_temp);die();
// if($getPost){

//     foreach ($getPost as $key => $value) {
//         $id = get_post_id_by_meta_key_and_value('promo_kategori_meta_box',$value->ID);

//         if(!isset($post_temp[$id])) {
//             $post_temp['data'][$id] = array();
//         }

//         $post_temp['data'][$id] = $value;

//     }

 
//       foreach ($post_temp as $key => $value) :
         
//           $id = $value['OXO CONNECT R2']->ID;
//           $id2 = $value['OMNISWITCH 6350']->ID;
//           $id3 = $value['WIFI OMNIACCESS AP 1101']->ID;
//           $id4 = $value['OMNIACCESS STELLAR SERIES']->ID;
//           $getTempOxo = get_post_id_by_meta_key_and_value('promo_kategori_meta_box',$id);
//            $getTempOmni = get_post_id_by_meta_key_and_value('promo_kategori_meta_box',$id2);
//             $getTempWifi = get_post_id_by_meta_key_and_value('promo_kategori_meta_box',$id3);
//             $getTempStellar = get_post_id_by_meta_key_and_value('promo_kategori_meta_box',$id4);

//           if($getTempOxo){
//               $post_temp['active'] = $getTempOxo;
//               // echo 'oxo'."<br>";
//               include "promo-oxo-connect.php";
//           }
//           if($getTempOmni){
//               $post_temp['active'] = $getTempOmni;
//               // echo 'omniswitch'."<br>";
//              include "promo-omni-switch.php";
//           }if($getTempWifi){
//               $post_temp['active'] = $getTempWifi;
//               // echo 'wifi'."<br>";
//               include "promo-wifi-omniaccess.php";
//           }if($getTempStellar){
//             $post_temp['active'] = $getTempStellar;
//             include "promo-omniaccess-stellar.php";
//           }

//       endforeach;
//   // }
// }

// }
?>
<?php get_template_part('contact_us'); ?>
<?php get_footer(); ?>
    <!-- /.container -->