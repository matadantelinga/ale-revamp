<?php 
/**
 * Template Name: Solusi Template
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

get_header('nav');  ?>
<!-- MAIN TITLE -->
<section>
    <article>
        <div class="wrap-main-solusilist">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="head-solusilist | animated zoomIn">
                            <h3><?php the_title();?></h3>
                            <?php while(have_posts()): the_post();?>
                            <p>
                                <?= the_content(); ?>
                            </p>
                            <?php endwhile;?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </article>
</section>
 <!-- PRODUCT -->
<section>
<?php 
 
 $args_slide = array( 'post_type' => 'solusi-alcatel' , 'orderby' => 'menu_order', 'order' => 'ASC', 'post_status' => 'publish',

                 'date_query' => array(
                                    array(
                                      'after'    => array(
                                        'year'  => date('2000'),
                                        'month' => date('12'),
                                        'day'   => date('30')
                                       ),
                                      'inclusive' => true,
                                    )
                                   )
  );          
 $get_solusi = get_posts($args_slide);
 // print_r($get_solusi);die();
 if($get_solusi ):

     if(!empty($get_solusi[0])) : 

        $SubTitle1 = get_post_meta($get_solusi[0]->ID,'subtitle1_solusi_meta_box',true);
        $SubTitle2 = get_post_meta($get_solusi[0]->ID,'subtitle2_solusi_meta_box',true);
        $SubTitle3 = get_post_meta($get_solusi[0]->ID,'subtitle3_solusi_meta_box',true);
         $Desc1 = get_post_meta($get_solusi[0]->ID,'produk_solusi_meta_description',true);
         $img1 = get_the_post_thumbnail_url($get_solusi[0]);
         $img2 = get_field('image_solusi',$get_solusi[0]->ID);
         $link1 = get_permalink($get_solusi[0]->post_title);
         $caption = get_post(get_post_thumbnail_id($get_solusi[0]));
         $img_alt = $caption->post_title;


 ?>
 <article>
    
     <div id="content-oxo" class="wrap-content-oxo">
         <div class="container">
             <div class="row">
                 <div class="col-md-12">
                     <div class="top-title | oxo-title | wow fadeInDown">
                         <h3><?= $SubTitle1; ?><br><span><?= $SubTitle2; ?></span>
                        
                         <p class="sub-left-content | sub-oxo"><?= $SubTitle3;  ?></p></h3>
                     </div>
                 </div>
                 <div class="col-md-6 | col-sm-6">
                    
                     <div class="left-content | wow fadeInUp">
                        <div class="txt-left-content | txt-oxo"><?=  $Desc1; ?>
                        </div>
                         <a class="btn-content | oxo-btn" href="<?= get_permalink($get_solusi[0]->ID); ?>">selengkapnya<i class="fa fa-long-arrow-right"></i></a>
                     </div>
                    
                 </div>
                 
                 <div class="col-md-6 | col-sm-6">
                     <div class="right-content | wow fadeInRight">
                         <img src="<?= $img2; ?>" alt="<?= $img_alt; ?>" title="">
                     </div>
                 </div>
                  
                   
             </div>
         </div>
     </div>

 </article>
<?php 

 endif;

 if (!empty($get_solusi[1])) :
     
    $SubTitle1 = get_post_meta($get_solusi[1]->ID,'subtitle1_solusi_meta_box',true);
    $SubTitle2 = get_post_meta($get_solusi[1]->ID,'subtitle2_solusi_meta_box',true);
    $SubTitle3 = get_post_meta($get_solusi[1]->ID,'subtitle3_solusi_meta_box',true);
 $Desc1 = get_post_meta($get_solusi[1]->ID,'produk_solusi_meta_description',true);
 $img1 = get_the_post_thumbnail_url($get_solusi[1]);
 $img2 = get_field('image_solusi',$get_solusi[1]->ID);
 $link1 = get_permalink($get_solusi[1]->post_title);
 $caption = get_post(get_post_thumbnail_id($get_solusi[1]));
         $img_alt = $caption->post_title;
?>
 <article>
     <div class="wrap-content-switch">
         <div class="container">
             <div class="row">
                 <div class="col-md-12">
                     <div class="top-title | switch-title | wow fadeInDown">
                         <h3><?= $SubTitle1; ?><br><span><?= $SubTitle2; ?></span>
                         <p class="sub-left-content | sub-switch">
                         <?= $SubTitle3; ?>
                         </p></h3>
                     </div>
                 </div>
                 <div class="col-md-6 | col-sm-6">
                     <div class="left-content | wow fadeInUp">
                        <div class="txt-left-content | txt-switch"><?=$Desc1; ?>
                        </div>
                        <a class="btn-content | switch-btn" href="<?= get_permalink($get_solusi[1]->ID); ?>">selengkapnya<i class="fa fa-long-arrow-right"></i></a>
                     </div>
                 </div>
                 <div class="col-md-6 | col-sm-6">
                     <div class="right-content | wow fadeInRight">
                          <img src="<?= $img2 ?>" alt="<?= $img_alt; ?>" title="">
                     </div>
                 </div>
             </div>
         </div>
     </div>
 </article>
<?php 
 
 endif;

 if(!empty($get_solusi[2])) :

$SubTitle1 = get_post_meta($get_solusi[2]->ID,'subtitle1_solusi_meta_box',true);
$SubTitle2 = get_post_meta($get_solusi[2]->ID,'subtitle2_solusi_meta_box',true);
$SubTitle3 = get_post_meta($get_solusi[2]->ID,'subtitle3_solusi_meta_box',true);
 $Desc1 = get_post_meta($get_solusi[2]->ID,'produk_solusi_meta_description',true);
 $img1 = get_the_post_thumbnail_url($get_solusi[2]);
 $img2 = get_field('image_solusi',$get_solusi[2]->ID);
 $link1 = get_permalink($get_solusi[2]->post_title);
 $caption = get_post(get_post_thumbnail_id($get_solusi[2]));
         $img_alt = $caption->post_title;
?>
 <article>
     <div class="wrap-content-wifi">
         <div class="container">
             <div class="row">
                 <div class="col-md-12">
                     <div class="top-title | wifi-title | wow fadeInDown">

                         <h3><?= $SubTitle1; ?><br><span><?= $SubTitle2; ?></span>
                         <p class="sub-left-content | sub-wifi"><?= $SubTitle3; ?></p></h3>
                     </div>
                 </div>
                 <div class="col-md-6 | col-sm-6">
                     <div class="left-content | wow fadeInUp">
                         <div class="txt-left-content | txt-wifi"><?= $Desc1; ?>
                         </div>
                         <a class="btn-content | wifi-btn" href="<?= get_permalink($get_solusi[2]->ID); ?>">selengkapnya<i class="fa fa-long-arrow-right"></i></a>
                     </div>
                 </div>
                 <div class="col-md-6 | col-sm-6">
                     <div class="right-content | img-wifi | wow fadeInRight">
                         <img src="<?= $img2; ?>" alt="<?= $img_alt; ?>" title="">
                     </div>
                 </div>
             </div>
         </div>
     </div>
 </article>
<?php 
 
 endif;

endif; 

?>
</section>

<?php get_template_part('contact_us'); ?>

<?php get_footer(); ?>
<!-- /.container -->