<?php 
/**
  * Template Name: 05 - Grey White
 * Template Post Type: solusi-alcatel
 * 
 * The template for displaying all single posts and attachments
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */
get_header('nav');  ?>
<section>
<?php 
$doc = strlen(trim($doc['url']));
if(have_posts()):
while(have_posts()): the_post();

        $SubTitle1 = get_post_meta($post->ID,'subtitle1_solusi_meta_box',true);
        $SubTitle2 = get_post_meta($post->ID,'subtitle2_solusi_meta_box',true);
        $SubTitle3 = get_post_meta($post->ID,'subtitle3_solusi_meta_box',true);
        $Desc1 = get_post_meta($post->ID,'post_solusi_meta_description',true);
        $Desc2 = get_post_meta($post->ID,'post_solusi_meta_description2',true);
        $LinkPromo = get_post_meta( $post->ID, 'post_solusi_new_meta_url', true);
        $getTitle = $SubTitle1.''.$SubTitle2;
        // print_r($LinkPromo);die();
        $img1 = get_the_post_thumbnail_url($post);
        $img2 = MultiPostThumbnails::get_post_thumbnail_url(get_post_type(), 'secondary-image');
        $link1 = get_permalink($post->post_title);
        // print_r($link1);
        $doc = get_post_meta(get_the_ID(), 'wp_custom_attachment', true);
?>
    <article>
        <div class="wrap-solusi-oxo wrap-solusi-omnivista">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="top-title | oxo-title | wow fadeInDown">
                            <h3><?= $SubTitle1; ?><br><span><?= $SubTitle2; ?></span></h3>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 | col-sm-5">
                        <div class="txt-solusi | wow fadeInDown">
                            <h3><?= $SubTitle3; ?></h3>

                            <p><?=  $Desc1; ?></p>
                        </div>
                    </div>
                    <div class="col-md-6 | col-sm-7">
                        <div class="wrap-img-solusi">
                            <div class="main-img-solusi | wow fadeInUp | gallery">
                                <div class="wrap-zoom">
                                    <a href="<?= $img1; ?>" rel="prettyPhoto">
                                        <img src="<?= esc_url(get_option('siteurl')) ?>/wp-content/assets/img/icon/icon-zoom.png" alt="zoom">
                                    </a>
                                </div>
                                <a href="<?= $img1; ?>" rel="prettyPhoto">
                                    <img src="<?= $img2; ?>" alt="<?= $getTitle;?>">
                                </a>
                            </div>
                            <!-- <a class="btn-cari-solusi" href="<?= site_url();?>/promo/?page=<?= basename(get_the_permalink()); ?>">cari promosi</a>
                            <a class="btn-download-solusi" href="<?= ($doc > 0) ? $doc['url'] : 'javascript:void(0);'; ?>" target="_blank">unduh brosur</a> -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </article>
<!-- </section>   -->

<!-- DETAIL SOLUSI -->
<!-- <section> -->
    <article>
    <?= $Desc2; ?>
        <!-- <div class="wrap-fitur-solusi">
            <div class="container ale-container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="fitur-solusi">
                             <h3>keuntungan</h3>
                           
                        </div>
                    </div>
                </div>
            </div>
        </div> -->
    </article>
  <!--   <?php //endif; 
  endwhile; endif; ?> -->
</section>
 <?php get_footer(); ?>