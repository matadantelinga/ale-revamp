<!DOCTYPE html>
<html>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>ALCATEL</title>
      <link rel="shortcut icon" type="image/x-icon" href="<?php echo esc_url( get_option('siteurl') ); ?>/wp-content/assets/img/logo/favicon.png" />

    <link rel="stylesheet" href="<?= esc_url(get_option('siteurl')) ?>/wp-content/assets/css/app.css">
    <link rel="stylesheet" href="<?= esc_url(get_option('siteurl')) ?>/wp-content/assets/css/style.css">
    <link rel="stylesheet" href="<?= esc_url(get_option('siteurl')) ?>/wp-content/assets/css/app-rsg.css?v=20170904-1353">
    <link rel="stylesheet" href="<?= esc_url(get_option('siteurl')) ?>/wp-content/assets/css/animate.css">
     <link rel="stylesheet" href="<?= esc_url(get_option('siteurl')) ?>/wp-content/assets/css/prettyPhoto.css">
    <!-- FONT -->
    <link href="<?= esc_url(get_option('siteurl')) ?>/wp-content/assets/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    
    <?php if (WP_ENV !== 'development'): ?>
    <!-- MONSERRAT FONT GOOGLE -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
    <?php endif; ?>
    

    <?php if (WP_ENV !== 'development'): ?>
          <!-- Google Tag Manager -->
        <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-P6FMMX7');</script>
        <!-- End Google Tag Manager -->
    <?php endif ?>
<!--cookies notification code-->
<link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.1.0/cookieconsent.min.css" />
<script src="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.1.0/cookieconsent.min.js"></script>
<script>
window.addEventListener("load", function(){
window.cookieconsent.initialise({
  "palette": {
    "popup": {
      "background": "#8156be"
    },
    "button": {
      "background": "#ffffff"
    }
  },
  "theme": "classic",
  "position": "top",
  "content": {
    "message": "We use cookies at alcatelkomunikasi.com so we can serve you content and advertising that is relevant to you. By using alcatelkomunikasi.com website, you’re agreeing to the use of cookies.",
    "href": "http://alcatel.seekyou.id/kebijakan-privasi/"
  }
})});
</script>

    <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
<?php if (WP_ENV !== 'development'): ?>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-P6FMMX7"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<?php endif ?>


<!-- NAVBAR -->
<div class="nav-main">
    <div class="container">
    <?php
         wp_nav_menu( array(
            'sort_column' => 'menu_order',
            'menu'   => 'Something custom walker',
            'walker' => new Custom_Nav_Walker(),
            'theme_location' => 'primary-menu'
        ) );
    ?>
    </div>
</div>

<nav class="nav-fixed | nav-white">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 | col-md-4 | col-sm-4 | col-xs-5">
                <div class="logo-topleft seo-bug-fix">
                    <a href="<?= site_url(); ?>">
                        <h1>
                        <img src="<?= esc_url( home_url('/')); ?>wp-content/assets/img/acalogo.png" alt="ACAPacific">
                        </h1>
                    </a>
                </div>
            </div>
            <div class="wrap-nav-icon-lg | col-lg-4 | col-md-4 | col-sm-4 | col-xs-2">
                <div class="nav-icon-lg | nav-icon-black">
                    <span></span>
                    <span></span>
                    <span></span>
                    <span></span>
                </div>
            </div>
            <div class="col-lg-4 | col-md-4 | col-sm-4 | col-xs-5">
                <div class="logo-topright | pull-right seo-bug-fix">
                    <a href="<?= site_url(); ?>">
                        <h2>
                        <img src="<?= esc_url( home_url('/')); ?>wp-content/assets/img/alelogo.png" alt="Alcatel Lucent Enterprise">
                        </h2>
                    </a>
                </div>
            </div>
            <!-- <div class="col-md-12 | col-xs-2">
                <div class="nav-icon-xs | nav-icon-black">
                    <span></span>
                    <span></span>
                    <span></span>
                    <span></span>
                </div>
            </div> -->
        </div>
    </div>
</nav>


