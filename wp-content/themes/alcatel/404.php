<?php 
/**
 * Template Name: 404 Page Template
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

get_header('nav');  ?>
<section>
    <article>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="notfound">
                        <h3>404</h3>
                        <h4>ERROR</h4>
                        <p>Maaf halaman yang Anda cari tidak ditemukan</p>
                        <a href="<?= site_url();?>">Halaman Utama</a>
                    </div>
                </div>
            </div>
        </div>
    </article>
</section>

<?php get_footer(); ?>