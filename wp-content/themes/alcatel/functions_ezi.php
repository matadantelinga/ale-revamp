<?php

function dg_get_template_view($template_name, $part_name=null, $data=array()) {
    ob_start();
    $params = $data;
    include(locate_template($template_name, $part_name));
    $var = ob_get_contents();
    ob_end_clean();
    return $var;
}

function dg_send_email($to='', $title, $view, $params=array()){

  $headers = array();
  $headers[] = 'Content-Type: text/html; charset=UTF-8';
  $headers[] = 'From: Administrator <sales@alcatelkomunikasi.com>';

  if(isset($params['cc'])){
    $headers[] = 'Cc: '.$params['cc'];
  }

  if(isset($params['bcc'])){
    $headers[] = 'Bcc: '.$params['bcc'];
  }

  $body_data = array(
                'config' => array(
                            'title' => $title
                            ,'logo' => home_url('/').'wp-content/assets/img/logo/aca-right-black.png'
                  )
                ,'data' => !empty($params['data']) ? $params['data'] : array()
    );

  $body = dg_get_template_view($view.'.php', null, $body_data);
  
  if(!empty($to)){
    wp_mail($to, '[Notification] '.$body_data['config']['title'], $body, $headers);
  }
  

}


/* @Recreate the default filters on the_content so we can pull formated content with get_post_meta
-------------------------------------------------------------- */
add_filter( 'meta_content', 'wptexturize'        );
add_filter( 'meta_content', 'convert_smilies'    );
add_filter( 'meta_content', 'convert_chars'      );
add_filter( 'meta_content', 'wpautop'            );
add_filter( 'meta_content', 'shortcode_unautop'  );
add_filter( 'meta_content', 'prepend_attachment' );
