<?php 
/**
 * Template Name: Beranda Template
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

// if(is_home()) :
    get_header('nav');
// elseif(is_404()):
//     get_header( '404' );
// else: 
// get_header('header'); 
// endif;
?>

<?php get_template_part('template-parts/home-carousel'); ?>
    <!-- Page Content -->
    <!-- PRODUCT -->

<section>
<?php 
 
    $args_slide = array( 'post_type' => 'solusi-alcatel' , 'orderby' => 'menu_order', 'order' => 'ASC', 'post_status' => 'publish',

                    'date_query' => array(
                                       array(
                                         'after'    => array(
                                           'year'  => date('2000'),
                                           'month' => date('12'),
                                           'day'   => date('30')
                                          ),
                                         'inclusive' => true,
                                       )
                                      )
     );          
    $get_solusi = get_posts($args_slide);
    // print_r($get_solusi);die();
    if($get_solusi ):
  
        if(!empty($get_solusi[0])) : 

            $SubTitle1 = get_post_meta($get_solusi[0]->ID,'subtitle1_solusi_meta_box',true);
            $SubTitle2 = get_post_meta($get_solusi[0]->ID,'subtitle2_solusi_meta_box',true);
            $SubTitle3 = get_post_meta($get_solusi[0]->ID,'subtitle3_solusi_meta_box',true);
            $Desc1 = get_post_meta($get_solusi[0]->ID,'produk_solusi_meta_description',true);
            $img1 = get_the_post_thumbnail_url($get_solusi[0]);
            $link1 = get_permalink($get_solusi[0]->post_title);
            $caption = get_post(get_post_thumbnail_id($get_solusi[0]));
            $img_alt = $caption->post_title;


    ?>
    <article>
       
        <div id="content-oxo" class="wrap-content-pabx">
            <div class="container">
                <div class="row">
                    <!-- <div class="col-md-12">
                        
                    </div> -->
                    <div class="col-md-6 | col-sm-6">
                         <div class="top-title | pabx-title | wow fadeInDown">
                            <h3><?= $SubTitle1; ?><br><span><?= $SubTitle2; ?></span>
                           
                            <p class="sub-left-content | sub-oxo"><?= $SubTitle3;  ?></p></h3>
                        </div>
                        <div class="left-content | wow fadeInUp">
                            <div class="txt-left-content | txt-pabx"><?=  $Desc1; ?>
                            </div>
                           <ul class="home-link">
                                <li>
                                <a class="btn-content | oxo-btn" href="<?= get_permalink($get_solusi[0]->ID); ?>">selengkapnya<i class="fa fa-long-arrow-right"></i></a>
                                </li>
                                <li>
                                <a class="btn-content | oxo-btn" style="margin-left: 2em;" href="/promo/">Promo<i class="fa fa-long-arrow-right"></i></a>
                                </li>
                           </ul>
                        </div>
                       
                    </div>
                    
                    <div class="col-md-6 | col-sm-6">
                        <div class="right-content | wow fadeInRight">
                            <img src="<?= $img1; ?>" alt="<?= $img_alt; ?>" title="" width="100%">
                        </div>
                    </div>
                     
                      
                </div>
            </div>
        </div>
  
    </article>
<?php 

    endif;

    if (!empty($get_solusi[1])) :
        
    $SubTitle1 = get_post_meta($get_solusi[1]->ID,'subtitle1_solusi_meta_box',true);
    $SubTitle2 = get_post_meta($get_solusi[1]->ID,'subtitle2_solusi_meta_box',true);
    $SubTitle3 = get_post_meta($get_solusi[1]->ID,'subtitle3_solusi_meta_box',true);
    $Desc1 = get_post_meta($get_solusi[1]->ID,'produk_solusi_meta_description',true);
    $img1 = get_the_post_thumbnail_url($get_solusi[1]);
    $link1 = get_permalink($get_solusi[1]->post_title);
    $caption = get_post(get_post_thumbnail_id($get_solusi[1]));
            $img_alt = $caption->post_title;
?>
    <article>
        <div class="wrap-content-switch-new">
            <div class="container">
                <div class="row">
                    <!-- <div class="col-md-12">
                        
                    </div> -->
                    <div class="col-md-6 | col-sm-6">
                        <div class="top-title | switch-new-title | wow fadeInDown">
                            <h3><?= $SubTitle1; ?><br><span><?= $SubTitle2; ?></span>
                            <p class="sub-left-content | sub-switch">
                            <?= $SubTitle3; ?>
                            </p></h3>
                        </div>
                        <div class="left-content | wow fadeInUp">
                            <div class="txt-left-content | txt-switch-new"><?=$Desc1; ?>
                            </div>
                            <ul class="home-link">
                                <li>
                                <a class="btn-content | oxo-btn | black-btn" href="<?= get_permalink($get_solusi[1]->ID); ?>">selengkapnya<i class="fa fa-long-arrow-right"></i></a>
                                </li>
                                <li>
                                <a class="btn-content | oxo-btn | black-btn" style="margin-left: 2em;" href="/promo/">Promo<i class="fa fa-long-arrow-right"></i></a>
                                </li>
                           </ul>
                        </div>
                    </div>
                    <div class="col-md-6 | col-sm-6">
                        <div class="right-content | wow fadeInRight">
                             <img src="<?= $img1; ?>" alt="<?= $img_alt; ?>" title=""  width="100%" style="margin-top: 20%;">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </article>
<?php 
    
    endif;

    if(!empty($get_solusi[2])) :

    $SubTitle1 = get_post_meta($get_solusi[2]->ID,'subtitle1_solusi_meta_box',true);
    $SubTitle2 = get_post_meta($get_solusi[2]->ID,'subtitle2_solusi_meta_box',true);
    $SubTitle3 = get_post_meta($get_solusi[2]->ID,'subtitle3_solusi_meta_box',true);
    $Desc1 = get_post_meta($get_solusi[2]->ID,'produk_solusi_meta_description',true);
    $img1 = get_the_post_thumbnail_url($get_solusi[2]);
    $link1 = get_permalink($get_solusi[2]->post_title);
    $caption = get_post(get_post_thumbnail_id($get_solusi[2]));
            $img_alt = $caption->post_title;
?>
    <article>
        <div class="wrap-content-access-point">
            <div class="container">
                <div class="row">
                    <!-- <div class="col-md-12">
                        
                    </div> -->
                    <div class="col-md-6 | col-sm-6">
                        <div class="top-title | access-point-title | wow fadeInDown">
                            <h3><?= $SubTitle1; ?><br><?= $SubTitle2; ?>
                            <p class="sub-left-content | sub-wifi"><?= $SubTitle3; ?></p></h3>
                        </div>
                        <div class="left-content | wow fadeInUp">
                            <div class="txt-left-content | txt-access-point"><?= $Desc1; ?>
                            </div>
                            <ul class="home-link">
                                <li>
                                <a class="btn-content | oxo-btn" href="<?= get_permalink($get_solusi[2]->ID); ?>">selengkapnya<i class="fa fa-long-arrow-right"></i></a>
                                </li>
                                <li>
                                <a class="btn-content | oxo-btn" style="margin-left: 2em;" href="/promo/">Promo<i class="fa fa-long-arrow-right"></i></a>
                                </li>
                           </ul>
                        </div>
                    </div>
                    <div class="col-md-6 | col-sm-6">
                        <div class="right-content | wow fadeInRight">
                            <img src="<?= $img1; ?>" alt="<?= $img_alt; ?>" title="" width="100%">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </article>
<?php 
    
    endif;

endif; 

?>
<!-- BLOG POST -->
<section>
    <article>
        <div class="wrap-blog">
            <div class="container">
                <div class="row">
                    <div class="col-md-9">
                        <div class="top-title-blog">
                            <h4>blog&amp;news</h4>
                        </div>
                    </div>
                    <?php   //$slide_link = get_post_meta( get_the_ID(), 'blog_new_meta_url', true );
                         //echo $slide_link;die();?>
                    <div class="col-md-3">
                        <div class="btn-blog-list">
                            <a href="<?= esc_url(get_option('siteurl')); ?>/blog">Blog List</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="wrap-blog-content">
            <div class="container">
                <!-- BLOG -->
                 <?php 
                     $args = array(
                            'posts_per_page' => 1
                           ,'category_name' => 'Blog'
                           ,'orderby'       => 'date'
                           ,'order'         => 'DESC'
                           ,'post_type'     => 'post'
                           ,'post_status'   => 'publish'
                        );                     
                     $postslist = new WP_Query( $args );
                     if($postslist->have_posts()):
                        while ( $postslist->have_posts()) : $postslist->the_post(); 
                            $imgBlog = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' );
                            // print_r($imgBlog);die();
                            $excerpt = substr( strip_tags( get_the_content() ), 0, 250);
                            $excerpt = $excerpt.'.';
                            $permalink = get_permalink($post->ID);
                            $date=get_the_date('d', $post->ID );
                            $month=get_the_date('M', $post->ID );
                            $year = get_the_date('Y', $post->ID);

                ?>
                <div class="row">
                    <div class="col-md-12">
                        <div class="content-blog">
                            <div class="col-lg-5 | col-md-4 | col-sm-5 | nopadding">
                                

                                <div class="img-blog" style="background:url(<?= $imgBlog[0]; ?>)no-repeat;background-position:center center;
                                    background-size:cover;">
                                    <a href="#">&nbsp;</a>
                                </div>
                            </div>
                            <div class="col-lg-7 | col-md-8 | col-sm-7 | nopadding">
                                <div class="wrap-txt-blog | pull-left">
                                    <h4 class="title-blog">
                                         <a href="<?= the_permalink() ?>"><?php the_title(); ?></a>
                                    </h4>
                                    <p class="txt-blog">
                                        <?= $excerpt; ?>
                                    </p>
                                    <!-- <div class="tag-blog">
                                        <?php the_tags( '<ul><li>', '</li><li>', '</li></ul>' ); ?>
                                    </div> -->
                                </div>
                                <div class="date-blog | pull-left">
                                    <p class="bdate"><?= $date; ?></p>
                                    <p class="bmonth"><?= $month;?></p>
                                    <p class="byear"><?= $year; ?></p>
                                </div>
                            </div>
                            <div class="view-blog">
                                <div class="comment-blog">
                                    <a href="javascript:void(0);">
                                    <img src="<?= esc_url(get_option('siteurl')) ?>/wp-content/assets/img/icon/icon-time.png" alt="time">
                                    <?= dg_get_date_diff(date('Y-m-d H:i:s'), get_the_time( 'Y-m-d H:i:s', $post->ID)); ?> yang lalu
                                    </a>
                                </div>
                                <ul class="socmed-blog">
                                    <li>share :</li>
                                    <li>
                                        <a href="<?php the_permalink();?>"" data-share-facebook data-share-url="<?php the_permalink();?>" data-share-title="<?php the_title();?>" data-share-text="<?php $excerpt;?>"><i class="fa fa-facebook"></i></a>
                                    </li>
                                    <li>
                                        <a href="<?php the_permalink();?>" data-share-twitter data-share-url="<?php the_permalink();?>" data-share-text="<?php the_title();?>"><i class="fa fa-twitter"></i></a>
                                    </li>
                                    <li>
                                        <a href="mailto:?body=<?php the_permalink();?>"><i class="fa fa-envelope"></i></a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                 <?php endwhile; endif; ?>
            </div>
        </div>
        <div class="wrap-blog-carousel">
            <div class="container">
                <!-- Title Bottom -->
                <div class="row">
                <div class="col-md-1 | col-sm-12">
                    <div class="arrow-blog">
                        <a class="left-control-blog" href="#carousel-blog" data-slide="prev">
                            <i class="fa fa-4x fa-angle-left"></i>
                        </a>
                    </div>
                </div>
                <div class="col-md-10 | nopadd-blog-slide">
                    <div id="carousel-blog" class="carousel-blog slide">
                        <div class="carousel-inner | carousel-in-blog">
                            <!-- item -->
                            <?php
                                $args = array(
                                'posts_per_page' => 7
                                ,'category_name' => 'Blog'
                                ,'orderby'       => 'date'
                                ,'order'         => 'DESC'
                                ,'post_type'     => 'post'
                                ,'post_status'   => 'publish'
                            );                     
                        $get_post = get_posts($args);
                        if($get_post){
                            
                            unset($get_post[0]); ?>

                            <div class="item | item-in-blog | active">
                                <div class="wrap-title">

                            <?php
                            $count = 0;
                            foreach($get_post as $mpost) :
                            ?>
                                    <div class="col-md-6 | col-sm-6 | padd-left75 | padd-right75">
                                        <div class="wrap-title-bottom | clearfix">
                                            <div class="date-bottom">
                                                <p class="tdate"><?= get_the_date('d',$mpost->ID); ?></p>
                                                <p class="tmonth"><?= get_the_date('M',$mpost->ID); ?></p>
                                                <p class="tyear"><?= get_the_date('Y',$mpost->ID); ?></p>
                                            </div>
                                            <div class="title-bottom">
                                                <a href="<?= get_permalink($mpost->ID ); ?>"><?= $mpost->post_title; ?></a>
                                            </div>
                                        </div>
                                    </div>

                                <?php if($count%2 == 1 && $count !== count($get_post)-1) : ?>

                                    </div>
                            <!-- end item -->
                            </div>

                            <div class="item | item-in-blog">
                                <div class="wrap-title">

                                <?php endif; ?>

                            <?php

                                $count++;
                            endforeach;

                            ?>
                                </div>
                            <!-- end item -->
                            </div>


                            <?php }//$count++; }//} ?> 
                        </div>
                    </div>
                </div><!-- end carousel blog -->
                    <div class="col-md-1 | col-sm-12">
                    <div class="arrow-blog">
                        <a class="right-control-blog" href="#carousel-blog" data-slide="next">
                            <i class="fa fa-4x fa-angle-right"></i>
                        </a>
                    </div>
                </div>
            </div>
            </div>
        </div>
    </article>
</section>

    <?php get_footer(); ?>
    <!-- /.container -->