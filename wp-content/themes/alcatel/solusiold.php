<?php 
/**
 * Template Name: Solusi Template
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

get_header('nav');  ?>
<!-- MAIN TITLE -->
<section>
    <article>
        <div class="wrap-main-solusilist">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="head-solusilist | animated zoomIn">
                            <h3><?php the_title();?></h3>
                            <?php while(have_posts()): the_post();?>
                            <p>
                                <?= the_content(); ?>
                            </p>
                            <?php endwhile;?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </article>
</section>
 <!-- PRODUCT -->
<section>
    <?php 
      $doc=strlen(trim($doc['url']));
      $args_slide = array( 'post_type' => 'solusi-alcatel' , 'orderby' => 'menu_order', 'order' => 'asc', 'post_status' => 'publish', 'posts_per_page' => 3 );          
        $get_solusi = get_posts($args_slide);
        // echo '<pre>';
        // echo print_r($get_solusi); die();

        global $post;

        foreach ($get_solusi as $key => $value) {
            
            
            $post = $value;
            setup_postdata( $post );
            $template = get_page_template_slug(); //get_post_meta($value->ID, 'solusi_template_meta_box', true); 
            get_template_part('template-parts/solusi-parts-'.substr($template, 0, -4));
            
            wp_reset_postdata();
        }
        

        // if($get_solusi):
        //      $getCount = count($get_solusi);
        //      $i=0;
        //      for($i=0; $i<$getCount; $i++):
                
        //         $SubTitle1 = get_post_meta($get_solusi[$i]->ID,'produk1_solusi_meta_box',true);
        //         $SubTitle2 = get_post_meta($get_solusi[$i]->ID,'produk2_solusi_meta_box',true);
        //         $SubTitle3 = get_post_meta($get_solusi[$i]->ID,'produk3_solusi_meta_box',true);
        //         $Desc1 = get_post_meta($get_solusi[$i]->ID,'produk_solusi_meta_description',true);
        //         $img1 = get_the_post_thumbnail_url($get_solusi[$i]);
        //         $link1 = get_permalink($get_solusi[$i]->post_title);
        //         $getIdTemplate = $get_solusi[$i]->ID;
        //         $getTitle = $SubTitle1.' '.$SubTitle2;

        //         // print_r($getIdTemplate);die();
        //         $result = get_template_solusi_by_meta_key_and_postid($getIdTemplate);
        //         foreach ($result as $key => $value) {
        //             $getTemplate = $value->meta_value;  
        //            // echo "$getTemplate"."<br>";       
        //             if($getTemplate =='template-1'):
        //                 get_template_part('template-parts/solusi-oxo-connect');
        //                 // include "solusi-oxo-connect.php";
        //             endif; 
        //             if($getTemplate=='template-2'):
        //                 get_template_part('template-parts/solusi-omni-switch');
        //                 // include "solusi-omni-switch.php";
        //             endif;
        //             if($getTemplate=='template-3'):
        //                 get_template_part('template-parts/solusi-wifi-omniaccess');
        //                 // include "solusi-wifi-omniaccess.php";
        //             endif; 
        //         }
        //     endfor;
        // endif;
    
?>
</section>

<?php get_template_part('contact_us'); ?>

<?php get_footer(); ?>
<!-- /.container -->