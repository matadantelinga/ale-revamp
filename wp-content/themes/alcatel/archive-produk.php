<?php 
/**
 * Template Name: Product Page Template
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

get_header('nav');  ?>

<!-- Product List -->
<!-- MAIN TITLE -->
<section>
    <div class="container">
        <div class="wrap-main-productlist-title">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="top-title-product">
                            <h4>PRODUK</h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!--Product List-->

<section id="product-container">
    <!--filter dropdown btn-->
    
    <div class="container">
        <?php while(have_posts()): the_post(); 
            global $wp_query, $paged;
            $paged =(get_query_var('paged')) ? absint(get_query_var('paged')) : 1;
            ?>
        <div class="row">
            <div class="col-md-9 col-sm-9 col-xs-12">
            </div>
            <div class="col-md-3 col-sm-3 col-xs-12">
                <div class="dropdown">
                    <a href="#" class="btn-product-list dropdown-toggle" type="button" data-toggle="dropdown">Urutkan Produk</a>
                    <ul class="dropdown-menu">
                        <li><a href="?sortby=asc">Dari A-Z</a></li>
                        <li><a href="?sortby=desc">Dari Z-A</a></li>
                    </ul>
                </div>
            </div>    
        </div>
    </div>
<?php 
   if (array_key_exists("sortby", $_GET) === true)
   {
    $postslist = sortIt($_GET['sortby']);
?>
    <div id="product-lists-wrap">
        <div class="container">
        <?php
            $i = 1;
            if ($postslist ->have_posts()) { while ($postslist ->have_posts()) : $postslist ->the_post(); $i=0; $i<4; $i++ ?>
            <div class="row">
                <div class="col-md-3">
                    <div class="product-wrapper">
                        <div class="product-img">
                            <img src="<?php the_post_thumbnail_url(); ?> " alt="<?php the_title(); ?>" width="100%" />
                        </div>    
                        <div class="product-title">
                            <h3><?php the_title(); ?></h3>
                        </div>
                        <div class="product-content">
                            <?php the_content();?>
                        </div>
                    </div>
                </div> 
                <?php if ($i==4) {
                    echo' </div>';
                }
                endwhile;   } ?>
        </div>
    </div>
<?php } else { 
    $postslist = new wp_query (array(
        'post_type' => 'produk',
        'numberposts' => -1,
        'posts_per_page' => 2,
        'orderby' => date,
        'paged' => $paged,
        'order' => 'DESC',
    ));
?>
    <div id="product-lists-wrap">
        <div class="container">
        <?php
            $i = 1;
            if ($postslist ->have_posts()) { while ($postslist ->have_posts()) : $postslist ->the_post(); $i=0; $i<4; $i++ ?>
            <div class="row">
                <div class="col-md-3 | col-sm-3 | col-xs-6">
                    <div class="product-wrapper">
                        <div class="product-img">
                            <img src="<?php the_post_thumbnail_url(); ?> " alt="<?php the_title(); ?>" width="100%" />
                        </div>    
                        <div class="product-title">
                            <h3><?php the_title(); ?></h3>
                        </div>
                        <div class="product-content">
                            <?php the_content();?>
                        </div>
                    </div>
                </div> 
                <?php if ($i==4) {
                    echo' </div>';
                }
                endwhile;   } ?>
        </div>
    </div>
    
            <?php }  ?>
            <!-- PAGINATION -->
            <div class="row">
                <div class="col-md-12 | col-sm-12">
                    <div class="center-pagination section-standard-padding">
                        <ul class="pagination ulpage">
                            <li><?php alcatel_pagination(); ?></li>
                        </ul>
                    </div>
                </div>
            </div>
            <?php endwhile; ?>

            <div class="container">
                <div class="row">
                    <div class="col-md-12 text-right">
                        <div class="btn-other-product">
                            <a href="#">PRODUK LAINNYA <i class="fa fa-chevron-right"></i></a>
                        </div>
                    </div>
                </div>
            </div>
</section>

<?php get_template_part('contact_us'); ?>
<?php get_footer(); ?>