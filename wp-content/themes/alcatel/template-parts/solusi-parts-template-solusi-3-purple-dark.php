<!-- Template SOLUSI PURPLE - DARK  Ex: OMNIACCESS -->
<?php
$SubTitle1 = get_post_meta(get_the_ID(),'produk1_solusi_meta_box',true);
$SubTitle2 = get_post_meta(get_the_ID(),'produk2_solusi_meta_box',true);
$SubTitle3 = get_post_meta(get_the_ID(),'produk3_solusi_meta_box',true);
$Desc1 = get_post_meta(get_the_ID(),'produk_solusi_meta_description',true);
$img1 = get_the_post_thumbnail_url(get_the_ID());
$link1 = get_the_permalink();
$getTitle = $SubTitle1.' '.$SubTitle2;


?>
<article>
    <div class="wrap-content-wifi">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="top-title | wifi-title | wow fadeInDown">
                        <h3><?= $SubTitle1; ?><br><span><?= $SubTitle2; ?></span></h3>
                        <p class="sub-left-content | sub-wifi"><?= $SubTitle3;?></p>
                    </div>
                </div>
                <div class="col-md-7 | col-sm-7">
                    <div class="left-content | wow fadeInUp">
                        <p class="txt-left-content | txt-wifi"><?= $Desc1; ?>
                        </p>
                        <a class="btn-content | wifi-btn" href="<?= $link1; ?>">selengkapnya<i class="fa fa-long-arrow-right"></i></a>
                    </div>
                </div>
                <div class="col-md-5 | col-sm-5">
                    <div class="right-content | img-wifi | wow fadeInRight">
                        <img src="<?= $img1; ?>" alt="<?= $getTitle;?>" title="">
                    </div>
                </div>
            </div>
        </div>
    </div>
</article>