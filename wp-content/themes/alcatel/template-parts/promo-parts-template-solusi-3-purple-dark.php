<!-- PRODUCT OXO-->
<?php
$Title1 = get_post_meta(get_the_ID(),'produk1_solusi_meta_box',true);
$Title2 = get_post_meta(get_the_ID(),'produk2_solusi_meta_box',true);
$image = get_the_post_thumbnail_url();
$getTitle = $Title1.' '.$Title2;

 $args_slide = array( 'post_type' => 'promosi-alcatel' , 'orderby' => 'published', 'order' => 'asc', 'orderby' => 'published', 'post_status' => 'publish'
    , 'meta_query' => array(
                            array(
                                'key'   => 'promo_kategori_meta_box',
                                'value' => basename(get_the_permalink()),
                            )
    ) 
); 

$get_promo = get_posts($args_slide);

if(count($get_promo) > 0):

?>

<section>
    <article>
        <div class="wrap-content-wifi">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="top-title | wifi-title | wow fadeInDown">
                            <h3><?= $Title1; ?><br><span><?= $Title2; ?></span></h3>
                        </div>
                    </div>
                    <div class="col-md-5 | col-sm-6">
                        <div class="left-content | wow fadeInUp">
                            <div class="wrap-carousel-promo">

                                <?php if(count($get_promo) > 1): ?>  
                                <div class="arrow-promo">
                                    <a class="control-promo" href="#carousel-promo" data-slide="prev">
                                        <i class="fa fa-4x fa-angle-left"></i>
                                    </a>
                                </div>
                                <?php endif; ?>
                            
                                <div id="carousel-promo" class="carousel-promo slide carousel-sync" data-ride="carousel" data-pause="false">
                                    <div class="carousel-inner | carousel-oxo-top">
                                    <?php 
                                    foreach ($get_promo as $key => $promo):
                                        $title3 = get_post_meta($promo->ID,'subtitle3_promosi_meta_box',true);
                                        if(!empty($title3)):?>
                                        <div class="item | item-promo | <?= ($key==0) ? 'active' : ''; ?>">
                                            <p class="txt-item-promo">
                                                <?= $title3;?>
                                            </p>
                                        </div>
                                       <?php else: ?>
                                        <div class="item | item-promo | <?= ($key==0) ? 'active' : ''; ?>">
                                            
                                        </div>
                                    <?php endif;
                                    endforeach; ?>
                                    </div>
                                </div>
                                
                                <?php if(count($get_promo) > 1): ?>  
                                <!-- NEXT BTN -->
                                <div class="arrow-promo">
                                    <a class="control-promo" href="#carousel-promo" data-slide="next">
                                        <i class="fa fa-4x fa-angle-right"></i>
                                    </a>
                                </div>
                                <?php endif; ?>
                               
                                <div id="carousel-promo" class="slide carousel-sync" data-ride="carousel" data-pause="false">
                                    <div class="carousel-inner | carousel-oxo-bottom">
                                     <?php 
                                       
                                        foreach ($get_promo as $key => $promo):
                                            $title5 = get_post_meta($promo->ID,'subtitle5_promosi_meta_box',true);
                                            $title4 = get_post_meta($promo->ID,'subtitle4_promosi_meta_box',true);
                                            $link1 = get_permalink($promo->ID);
                                         ?>
                                        <div class="item <?= ($key==0) ? 'active' : ''; ?>">
                                            <div class="txt-oxo-promo">
                                               <p><?= $title5; ?></p>
                                               <?= !empty($title4) ? '<p>'.$title4.'</p>' : ''; ?>
                                            </div>
                                            <a class="btn-content | oxo-btn | desktop-only-inline-block" href="<?= $link1;?>">lihat promo<i class="fa fa-long-arrow-right"></i>
                                            </a>
                                        </div>
                                        <?php endforeach; ?>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="col-md-6 | col-sm-5">
                        <div class="right-content | img-wifi | wow fadeInRight">
                            <img src="<?= $image; ?>" alt="<?= $getTitle;?>" title="">
                            <a class="btn-content | oxo-btn | mobile-only-inline-block" href="<?= $link1;?>">lihat promo<i class="fa fa-long-arrow-right"></i>
                                            </a>
                        </div>
                    </div>
                     
                </div>
            </div>
        </div>
    </article>
</section>

<?php endif;?>
