<!-- Template SOLUSI WHITE - GREY Ex: OMNI SWITCH -->
<?php
$SubTitle1 = get_post_meta(get_the_ID(),'produk1_solusi_meta_box',true);
$SubTitle2 = get_post_meta(get_the_ID(),'produk2_solusi_meta_box',true);
$SubTitle3 = get_post_meta(get_the_ID(),'produk3_solusi_meta_box',true);
$Desc1 = get_post_meta(get_the_ID(),'produk_solusi_meta_description',true);
$img1 = get_the_post_thumbnail_url(get_the_ID());
$link1 = get_the_permalink();
$getTitle = $SubTitle1.' '.$SubTitle2;


?>
<article>
    <div class="wrap-content-switch">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="top-title | switch-title | wow fadeInDown">
                        <h3><?= $SubTitle1; ?><br><span><?= $SubTitle2; ?></span></h3>
                        <p class="sub-left-content | sub-switch">
                            <?= $SubTitle3;?>
                        </p>
                    </div>
                </div>
                <div class="col-md-6 | col-sm-6">
                    <div class="left-content | wow fadeInUp">
                        <p class="txt-left-content | txt-switch">
                            <?= $Desc1; ?>
                        </p>
                        <a class="btn-content | switch-btn" href="<?= $link1; ?>">selengkapnya<i class="fa fa-long-arrow-right"></i></a>
                    </div>
                </div>
                <div class="col-md-6 | col-sm-6">
                    <div class="right-content | wow fadeInRight">
                        <img src="<?= $img1; ?>" alt="<?= $getTitle;?>" title="">
                    </div>
                </div>
            </div>
        </div>
    </div>
</article>