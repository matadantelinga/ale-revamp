<?php 
/**
 * Template Name: Product Page Template
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

get_header('nav');  ?>

<!-- Product List -->
<!-- MAIN TITLE -->
<section>
    <div class="container">
        <div class="wrap-main-productlist-title">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="top-title-product">
                            <h4>PRODUK</h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!--Product List-->

<section id="product-container">
    <!--filter dropdown btn-->
    
    <div class="container">
        <?php while(have_posts()): the_post(); 
            global $wp_query, $paged;
            $paged =(get_query_var('paged')) ? absint(get_query_var('paged')) : 1;
            ?>
        <div class="row">
            <div class="col-md-9 col-sm-9 col-xs-12">
            </div>
            <div class="col-md-3 col-sm-3 col-xs-12">
                <div id="product-dropdown" class="dropdown">
                <?php 
                        if (array_key_exists("sortby", $_GET) === false)
                        {
                    ?>
                     <a href="#" class="btn-product-list dropdown-toggle product-short-btn" type="button" data-toggle="dropdown" id="product-button">Urutkan Produk</a>
                    <?php 
                                     } elseif (array_key_exists("sortby", $_GET) === true)
                                    {
                    ?>
                    <?php if (sortIt($_GET['sortby'] == 'asc')) { ?>
                    <a href="#" class="btn-product-list dropdown-toggle product-short-btn" type="button" data-toggle="dropdown" style="display:inline-block;" id="btn-dropdown-az">Dari A-Z</a>
                    <?php } elseif (sortIt($_GET['sortby'] == 'desc')) { echo'a';} } ?>
                    <ul class="dropdown-menu">
                        <li><a href="?sortby=asc" id="product-button-az" >Dari A-Z</a></li>
                        <li><a href="?sortby=desc" id="product-button-za" >Dari Z-A</a></li>
                    </ul>
                </div>
            </div>    
        </div>
    </div>
<?php 
   if (array_key_exists("sortby", $_GET) === true)
   {
    $postslist = sortIt($_GET['sortby']);
?>
    <div id="product-lists-wrap">
        <div class="container">
        <?php
            if ($postslist ->have_posts()) {  ?>
            <div class="row">
                <?php $i=0; while ($postslist ->have_posts()) : $postslist ->the_post(); 
                    if($i >=4){
                        $i = 0;
                    }
                        $i += 1;?>
                <div class="col-md-3" style="padding-bottom: 15px;">
                    <div class="product-wrapper">
                        <div class="product-img">
                            <img src="<?php the_post_thumbnail_url(); ?> " alt="<?php the_title(); ?>" width="100%" />
                        </div>    
                        <div class="product-title">
                            <h3><?php the_title(); ?></h3>
                        </div>
                        <div class="product-content">
                            <?php the_content();?>
                        </div>
                    </div>
                </div> 
                <?php if ($i==4) {
                        echo"</div><div class='row'>";
                    } endwhile;  } ?>
        </div>
    </div>
<?php } else { 
    $postslist = new wp_query (array(
        'post_type' => 'produk',
        'numberposts' => -1,
        'posts_per_page' => 12,
        'orderby' => date,
        'paged' => $paged,
        'order' => 'DESC',
    ));
?>
    <div id="product-lists-wrap">
        <div class="container">
            <?php
            if ($postslist ->have_posts()) {  ?>
            <div class="row" style="padding-bottom: 15px;">
                <?php $i=0; while ($postslist ->have_posts()) : $postslist ->the_post(); 
                    if($i >=4){
                        $i = 0;
                    }
                        $i += 1;?>
                <div class="col-md-3" style='padding-bottom: 15px;'>
                    <div class="product-wrapper">
                        <div class="product-img">
                            <img src="<?php the_post_thumbnail_url(); ?> " alt="<?php the_title(); ?>" width="100%" />
                        </div>    
                        <div class="product-title">
                            <h3><?php the_title(); ?></h3>
                        </div>
                        <div class="product-content">
                            <?php the_content();?>
                        </div>
                    </div>
                </div> 
                <?php if ($i==4) {
                        echo"</div><div class='row' style='padding-bottom:15px;'>";
                    } endwhile;  } ?>
        </div>
    </div>
    
            <?php }  ?>
            <!-- PAGINATION -->
            <div class="row">
                <div class="col-md-12 | col-sm-12">
                    <div class="center-pagination section-standard-padding">
                        <ul class="pagination ulpage">
                            <li><?php alcatel_pagination(); ?></li>
                        </ul>
                    </div>
                </div>
            </div>
            <?php endwhile; ?>

            <div class="container">
                <div class="row">
                    <div class="col-md-12 text-right">
                        <div class="btn-other-product">
                            <a href="#">PRODUK LAINNYA <i class="fa fa-chevron-right"></i></a>
                        </div>
                    </div>
                </div>
            </div>
</section>

<?php get_template_part('contact_us'); ?>
<?php get_footer(); ?>