<?php 
/**
 * Template Name: Tentang Kami Template
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

get_header('nav');  ?>
<!-- TENTANG KAMI -->
<section>
    <article>
        <div class="wrap-about">
            <div class="container">
                <div class="row">
                    <div class="col-md-5">
                        <div class="main-title-about">
                            <h3>tentang kami</h3>
                        </div>
                        <?php   
                            $alcatel_theme_options2 = get_option('alcatel_theme_options');
                            $title1 = $alcatel_theme_options2['alcatel_aboutus_title1'];
                            $title2 = $alcatel_theme_options2['alcatel_aboutus_title2'];
                            $desc1 = $alcatel_theme_options2['alcatel_aboutus_title_1'];
                            $desc2 = $alcatel_theme_options2['alcatel_aboutus_title_2'];
                            $imgVideo = $alcatel_theme_options2['alcatel_aboutus_image4'];
                            $linkVideo = $alcatel_theme_options2['alcatel_aboutus_url1'];

                           //if ( have_posts() ) : while ( have_posts() ) : the_post();
                            ?>
                        <div class="txt-about">
                            <h3 class="subtitle-about"><?= $title1; ?></h3>
                            <p><?= $desc1; ?></p>
                        </div>
                        <?php 
                            $location = get_post_meta( get_the_ID(), 'blog_new_meta_url', true );
                            $large_image_url = wp_get_attachment_image_src( get_post_thumbnail_id(), 'large');
                            // echo $large_image_url[0]; die();
                        ?>
                          <div class="txt-about">
                            <h3 class="subtitle-about"><?= $title2; ?></h3>
                            <p><?= $desc2; ?></p>
                        </div> 
                        <!-- <?php //endwhile; endif;?> -->
                    </div>
                      <?php 
                        // $alcatel_theme_options = 'alcatel_theme_options'; 
                        // $alcatel_theme_options = get_option($alcatel_theme_options);
                        // $url1    = esc_url( $alcatel_theme_options['alcatel_homeservice_url1'] );
                        // $linkUrl   = $alcatel_theme_options['alcatel_aboutus_url1'];
                       ?>
                    <div class="col-md-7">
                         <div class="video-about">
                            <img src="<?= $imgVideo; ?>" alt="<?= $title1; ?>">
                            <div class="txt-video-about"><p>Click to Play</p></div>
                        </div>
                        <div class="open-video">
                            <i class="fa fa-times | close-video" aria-hidden="true"></i>
                             <iframe width="560" height="315" src="<?= $linkVideo; ?>" frameborder="0" allowfullscreen></iframe>
                           <!--  <iframe src="<?= $location ?>" frameborder="0" allowfullscreen>
                            </iframe> -->
                           <!--  <iframe src="<?= $location ?>" frameborder="0" allowfullscreen>
                            </iframe> -->
                        </div>

                        <div class="contact-about">
                            <?php dynamic_sidebar('contact-bar')?>
                        </div>
                       <!--  <div class="socmed-about">
                            <p>sosial media</p>
                            <a href="#"><i class="fa fa-facebook"></i></a>
                            <a href="#"><i class="fa fa-twitter"></i></a>
                        </div> -->
                        <div class="socmed-about">
                            <a href="<?= site_url();?>/hubungi-kami">hubungi kami<i class="fa fa-user"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </article>
</section>


<?php get_footer(); ?>