<?php
/**
 * The template for displaying pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

get_header('nav');  
the_Post();
?>
	<!-- PAGE-->
<section>
    <article>
        <div class="wrap-blogview">
            <div class="container">
                <!-- BLOG CONTENT-->
                <div class="row | padd15">
                    <div class="col-md-12">
                    <div class="top-title">
                        <h1 class="page-title"><?php the_title();?></h1>
                    </div>
                    <div class="page-content">
                        <?php the_content();?>
                    </div>
                </div>

            </div><!-- end container -->
        </div>
    </article>
</section>


<?php get_footer(); ?>