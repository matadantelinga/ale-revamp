<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
dynamic_sidebar('footer-bar');
?>
    <!-- FOOTER -->
<footer>
    <div class="wrap-footer">
        <div class="container">
            <div class="row">
                <div class="wrap-scroll">
                    <div class="scroll-top-wrapper ">
                        <span class="scroll-top-inner">
                            <i class="fa fa-2x fa-angle-up"></i>
                        </span>
                    </div>
                </div>
                <?php $alcatel_theme_options = 'alcatel_theme_options';
                      $alcatel_theme_options = get_option($alcatel_theme_options);
                 ?>
                <div class="col-md-6 | col-sm-6">
                    <div class="socmed-footer">
                        <ul>
                            <li><a href="<?= esc_url($alcatel_theme_options['alcatel_facebook']); ?>" target="_blank"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="<?= esc_url($alcatel_theme_options['alcatel_twitter']); ?>" target="_blank"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="<?= esc_url($alcatel_theme_options['alcatel_youtube']); ?>" target="_blank"><i class="fa fa-youtube-play"></i></a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-6 | col-sm-6">
                    <div class="copyright">
                        <p>&copy; Copyright Alcatel-Lucent Enterprise</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>


    <script src="<?php echo esc_url( get_option('siteurl') ); ?>/wp-content/assets/js/jquery-1.11.0.js"></script>
	<script src="<?php echo esc_url( get_option('siteurl') ); ?>/wp-content/assets/js/bootstrap.js"></script>
	<script src="<?php echo esc_url( get_option('siteurl') ); ?>/wp-content/assets/js/wow.min.js"></script>
    <script src="<?php echo esc_url( get_option('siteurl') ); ?>/wp-content/assets/js/jquery.easing.min.js"></script>
    <script src="<?php echo esc_url( get_option('siteurl') ); ?>/wp-content/assets/js/jquery.prettyPhoto.js"></script>
    <script src="<?php echo esc_url( get_option('siteurl') ); ?>/wp-content/assets/js/basePrettyPhoto.js"></script>
    <?php if (WP_ENV !== 'development'): ?>
    <script src='https://www.google.com/recaptcha/api.js'></script>
    <?php endif; ?>
    <!-- <script src="<?php //echo esc_url( get_option('siteurl') ); ?>/wp-content/assets/js/navbarscroll.js"></script>  -->

<!-- script -->
<!-- CAROUSEL PROMO -->
<script type="text/javascript">
    $('.carousel-sync').on('slide.bs.carousel', function(ev) {
    var dir = ev.direction == 'right' ? 'prev' : 'next';
    $('.carousel-sync').not('.sliding').addClass('sliding').carousel(dir);
});
$('.carousel-sync').on('slide.bs.carousel', function(ev) {
    $('.carousel-sync').removeClass('sliding');
});

$(document).ready(function(){
    $('.btn-mouse-scroll , .get-promo').on('click',function (e) {
        e.preventDefault();

        var target = this.hash;
        var $target = $(target);
        var tambahjarak = 100;
        
        $('html, body').stop().animate({
            'scrollTop': ($target.offset().top) - 87 
        }, 700, 'swing', function () {
            window.location.hash = target;
        });
    });
});
</script>
<!-- SCROLL NAV -->
<script type="text/javascript">
$(function(){
    // $(document).on('scroll', function(){
    // if ($(window).scrollTop() > 10) {
    //     $('.scroll-top-wrapper').addClass('show');
    //     $('.nav-fixed').addClass('nav-scroll');
    //     $('.nav-icon-lg').addClass('nav-icon-black');
    //     $('.nav-icon-xs').addClass('nav-icon-black');
    //     $('.img-mouse-scroll').removeClass('infinite');
    //     $('.logo-topleft').hide(100);
    //     $('.logo-topleft-black').show(100);
    //     $('.logo-topright').hide(100);
    //     $('.logo-topright-black').show(100);
    // } else {
    //     $('.scroll-top-wrapper').removeClass('show');
    //     $('.nav-fixed').removeClass('nav-scroll');
    //     $('.nav-icon-lg').removeClass('nav-icon-black');
    //     $('.nav-icon-xs').removeClass('nav-icon-black');
    //     $('.img-mouse-scroll').addClass('infinite');
    //     $('.logo-topleft').show(100);
    //     $('.logo-topleft-black').hide(100);
    //     $('.logo-topright').show(100);
    //     $('.logo-topright-black').hide(100);
    // }
});

   $('.scroll-top-wrapper').on('click', scrollToTop);
    //});

    function scrollToTop() {
    verticalOffset = typeof(verticalOffset) != 'undefined' ? verticalOffset : 0;
    element = $('body');
    offset = element.offset();
    offsetTop = offset.top;
    $('html, body').animate({scrollTop: offsetTop}, 500, 'linear');
}
</script>


<script>
$(document).ready(function(){
    // $('.btn-mouse-scroll').on('click',function (e) {
    //     e.preventDefault();

    //     var target = this.hash;
    //     var $target = $(target);
    //     var tambahjarak = 100;
        
    //     $('html, body').stop().animate({
    //         'scrollTop': ($target.offset().top) - 87 
    //     }, 700, 'swing', function () {
    //         window.location.hash = target;
    //     });
    // });
    $('.btn-mouse-scroll').on('click',function (e) {
        e.preventDefault();

        var target = this.hash;
        var $target = $(target);
        var tambahjarak = 100;
        
        $('html, body').stop().animate({
            'scrollTop': ($target.offset().top) - 87 
        }, 700, 'swing', function () {
            window.location.hash = target;
        });
    });
     $('.nav-icon-lg').click(function(){
        $(this).toggleClass('opennav');
        $('.nav-main').fadeToggle(200);
    });
    $('.nav-icon-xs').click(function(){
        $(this).toggleClass('opennav');
        $('.nav-main').fadeToggle(200);
    });
    $('.video-about').click(function(){
        $('.open-video').fadeToggle(200);
    });
    $('.close-video').click(function(){
        $('.open-video').fadeToggle(200);
    });
});
</script>


<!-- OPEN NAV -->
<!-- <script>
$(document).ready(function(){
    $('.nav-icon-lg').click(function(){
        $(this).toggleClass('opennav');
        $('.nav-main').fadeToggle(200);
    });
    $('.nav-icon-xs').click(function(){
        $(this).toggleClass('opennav');
        $('.nav-main').fadeToggle(200);
    });
     $('.video-about').click(function(){
        $('.open-video').fadeToggle(200);
    });
    $('.close-video').click(function(){
        $('.open-video').fadeToggle(200);
    });
});
</script> -->

<!-- CAROUSEL -->
<script>
$('.carousel').carousel({
    interval: 3000
})
</script>

<!-- WOW JS -->
<script>
    wow = new WOW(
      {
        animateClass: 'animated',
        offset:       100,
        callback:     function(box) {
        // console.log("WOW: animating <" + box.tagName.toLowerCase() + ">")
        }
      }
    );
    wow.init();
</script>
<?php wp_footer(); ?>
</body>
</html>
