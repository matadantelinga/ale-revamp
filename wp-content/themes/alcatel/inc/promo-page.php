<?php 

function promo_register(){
$lbl_promo = array(
  'name'               => _x( 'Promo', 'post type general name', 'your-plugin-textdomain' ),
  'singular_name'      => _x( 'Promo', 'post type singular name', 'your-plugin-textdomain' ),
  'menu_name'          => _x( 'Promo', 'admin menu', 'your-plugin-textdomain' ),
  'name_admin_bar'     => _x( 'Promo', 'add new on admin bar', 'your-plugin-textdomain' ),
  'add_new'            => _x( 'Add New', 'book', 'your-plugin-textdomain' ),
  'add_new_item'       => __( 'Add New Promo', 'your-plugin-textdomain' ),
  'new_item'           => __( 'New Promo', 'your-plugin-textdomain' ),
  'edit_item'          => __( 'Edit Promo', 'your-plugin-textdomain' ),
  'view_item'          => __( 'View Promo', 'your-plugin-textdomain' ),
  'all_items'          => __( 'All Promo', 'your-plugin-textdomain' ),
  'search_items'       => __( 'Search Promo', 'your-plugin-textdomain' ),
  'parent_item_colon'  => __( 'Parent Promo:', 'your-plugin-textdomain' ),
  'not_found'          => __( 'No Promo found.', 'your-plugin-textdomain' ),
  'not_found_in_trash' => __( 'No Promo found in Trash.', 'your-plugin-textdomain' )
);

$args_promo = array(
  'labels'             => $lbl_promo,
  'description'        => __( 'Description.', 'your-plugin-textdomain' ),
  'public'             => true,
  'publicly_queryable' => true,
  'show_ui'            => true,
  'show_in_menu'       => true,
  'query_var'          => true,
  'rewrite'            => array( 'slug' => 'promo' ),
  'capability_type'    => 'post',
  'has_archive'        => true,
  'hierarchical'       => true,
  'menu_position'      => null,
  'supports'           => array( 'title', 'thumbnail', 'page-attributes' )
);
  register_post_type( 'promo', $args_promo );
}
add_action('init', 'promo_register');
function create_promo_taxonomies() {
    $lbl_promo = array(
        'name'              => _x( 'Categories', 'taxonomy general name' ),
        'singular_name'     => _x( 'Category', 'taxonomy singular name' ),
        'search_items'      => __( 'Search Categories' ),
        'all_items'         => __( 'All Categories' ),
        'parent_item'       => __( 'Parent Category' ),
        'parent_item_colon' => __( 'Parent Category:' ),
        'edit_item'         => __( 'Edit Category' ),
        'update_item'       => __( 'Update Category' ),
        'add_new_item'      => __( 'Add New Category' ),
        'new_item_name'     => __( 'New Category Name' ),
        'menu_name'         => __( 'promo' ),
    );

    $args_promo = array(
        'hierarchical'      => true, // Set this to 'false' for non-hierarchical taxonomy (like tags)
        'labels'            => $lbl_promo,
        'show_ui'           => true,
        'show_admin_column' => true,
        'query_var'         => true,
        'rewrite'           => array( 'slug' => 'promo' ),
    );

    register_taxonomy( 'promo_categories', array( 'promo' ), $args_promo );
}
add_action( 'init', 'promo_register', 0 );

