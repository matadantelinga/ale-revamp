<?php 
//SIDE PRODUK KATEGORI
function add_custom_meta_box()
{
    add_meta_box("kategori-meta-box", "Produk Kategori", "produk_kategori_meta_box_markup", "promosi-alcatel", "side", "high", null);
}

add_action("add_meta_boxes", "add_custom_meta_box");
function produk_kategori_meta_box_markup($post)
{
    wp_nonce_field( basename(__FILE__), 'promo_kategori_meta_nonce' );
    $current_template = get_post_meta( $post->ID, 'promo_kategori_meta_box', true);

    // $box_label = '<label for="promo_kategori_meta_box">Pilih Kategori</label>';
    // <select> wrapper around our options. notice the 'name' == 'for' from above
    $box_select = '<select name="promo_kategori_meta_box">';
    $box_default_option = '<option value="">--Pilih Kategori--</option>';
    $box_options = '';
      $args = array(
        //Post & Page Parameters
        'numberposts' => 15,
        'post_type'   => 'solusi-alcatel',
        'post_status' => 'publish', 
        'order'       => 'ASC',
        'orderby'     => 'published',   
        );      
     $option_values = get_posts( $args );
    // and put it in our <select> box.
    foreach (  $option_values as $key => $value ) {
        $title = $value->post_title;
        $name = $value->post_name;
        if ( $current_template == $name ) {
            $box_options .= '<option value="' . $name . '" selected="selected">' . $title . '</option>';
        } else {
            $box_options .= '<option value="' . $name . '">' . $title . '</option>';
        }
    }
      // echo our markup (you should return it, but we won't do that here).
    echo $box_label;
    echo $box_select;
    echo $box_default_option;
    echo $box_options;
    echo '</select>';

}
function promoKategoriMetaBoxSave( $post_id ) {
    $current_nonce = $_POST['promo_kategori_meta_nonce'];
    $is_autosaving = wp_is_post_autosave( $post_id );
    $is_revision   = wp_is_post_revision( $post_id );
    $valid_nonce   = ( isset( $current_nonce ) && wp_verify_nonce( $current_nonce, basename( __FILE__ ) ) ) ? 'true' : 'false';

    // if the post is autosaving, a revision, or the nonce is not valid
    // do not save any changed settings.
    if ( $is_autosaving || $is_revision || !$valid_nonce ) {
        return;
    }

    // Find our 'promo_kategori_meta_box' field in the POST request, and save it
    // when the post is updated. Note that the POST field matches the
    // name of the select box in the markup.
    $promo_kategori_meta_box = $_POST['promo_kategori_meta_box'];
    update_post_meta( $post_id, 'promo_kategori_meta_box', $promo_kategori_meta_box );
}
add_action( 'save_post', 'promoKategoriMetaBoxSave' );
//end - SIDE PRODUK KATEGORI

function promosi_register(){
$lbl_promosi = array(
  'name'               => _x( 'Promosi', 'post type general name', 'your-plugin-textdomain' ),
  'singular_name'      => _x( 'Promosi', 'post type singular name', 'your-plugin-textdomain' ),
  'menu_name'          => _x( 'Promosi', 'admin menu', 'your-plugin-textdomain' ),
  'name_admin_bar'     => _x( 'Promosi', 'add new on admin bar', 'your-plugin-textdomain' ),
  'add_new'            => _x( 'Add New', 'book', 'your-plugin-textdomain' ),
  'add_new_item'       => __( 'Add New Promosi', 'your-plugin-textdomain' ),
  'new_item'           => __( 'New Promosi', 'your-plugin-textdomain' ),
  'edit_item'          => __( 'Edit Promosi', 'your-plugin-textdomain' ),
  'view_item'          => __( 'View Promosi', 'your-plugin-textdomain' ),
  'all_items'          => __( 'All Promosi', 'your-plugin-textdomain' ),
  'search_items'       => __( 'Search Promosi', 'your-plugin-textdomain' ),
  'parent_item_colon'  => __( 'Parent Promosi:', 'your-plugin-textdomain' ),
  'not_found'          => __( 'No promo found.', 'your-plugin-textdomain' ),
  'not_found_in_trash' => __( 'No promo found in Trash.', 'your-plugin-textdomain' )
);

$args_promosi = array(
  'labels'             => $lbl_promosi,
  'description'        => __( 'Description.', 'your-plugin-textdomain' ),
  'public'             => true,
  'publicly_queryable' => true,
  'show_ui'            => true,
  'show_in_menu'       => true,
  'query_var'          => true,
  'rewrite'            => array( 'slug' => 'promosi-alcatel' ),
  'capability_type'    => 'post',
  'has_archive'        => true,
  'hierarchical'       => true,
  'menu_position'      => null,
  'supports'           => array( 'title', 'thumbnail', 'page-attributes' )
);
  register_post_type( 'promosi-alcatel', $args_promosi );
}
add_action('init', 'promosi_register');
function create_promosi_taxonomies() {
    $lbl_promosi = array(
        'name'              => _x( 'Categories', 'taxonomy general name' ),
        'singular_name'     => _x( 'Category', 'taxonomy singular name' ),
        'search_items'      => __( 'Search Categories' ),
        'all_items'         => __( 'All Categories' ),
        'parent_item'       => __( 'Parent Category' ),
        'parent_item_colon' => __( 'Parent Category:' ),
        'edit_item'         => __( 'Edit Category' ),
        'update_item'       => __( 'Update Category' ),
        'add_new_item'      => __( 'Add New Category' ),
        'new_item_name'     => __( 'New Category Name' ),
        'menu_name'         => __( 'promosi-alcatel' ),
    );

    $args_promosi = array(
        'hierarchical'      => true, // Set this to 'false' for non-hierarchical taxonomy (like tags)
        'labels'            => $lbl_promosi,
        'show_ui'           => true,
        'show_admin_column' => true,
        'query_var'         => true,
        'rewrite'           => array( 'slug' => 'promosi-alcatel' ),
    );

    register_taxonomy( 'promosi_categories', array( 'promosi-alcatel' ), $args_promosi );
}
add_action( 'init', 'promosi_register', 0 );

// TEMPLATE PAGE PROMOSI
add_action('save_post','dg_insert_page_template_promo');
function dg_insert_page_template_promo($post_id){
  global $post;
  if(defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) return;

  if(!isset($_POST['dg_template_promo_meta_nonce']) || !wp_verify_nonce($_POST['dg_template_promo_meta_nonce'], 'dg_template_promo_meta_box_nonce' )) return;
  if(!current_user_can('edit_post')) return;
  $slug = "promosi-alcatel";
  if($slug != $post->post_type) return;
  
   // if(isset($_POST['dg_template_page_promo_meta_box'])){
   //      update_post_meta($post_id,'dg_template_page_promo_meta_box',esc_html($_POST['dg_template_page_promo_meta_box']));
   //  }

} 
add_action('add_meta_boxes','dg_post_template_promo_meta_add');
function dg_post_template_promo_meta_add(){
    //Template page custom meta box dihidden
     // add_meta_box( 'template-promo-meta-box-id', 'Template Page', 'template_promo_new_meta_box_cb', 'promosi-alcatel', 'normal', 'high' );
}
function template_promo_new_meta_box_cb(){
     wp_nonce_field(basename(__FILE__), "meta-box-nonce");
     global $post;
     //$getTemplate = get_post_meta($post->ID,'dg_template_page_promo_meta_box',true);

     $result = !empty($get_template) ? esc_html($get_template) : '';
     wp_nonce_field('dg_template_promo_meta_box_nonce','dg_template_promo_meta_nonce');
   
     $get_kategori = get_post_meta( $post->ID, 'promo_kategori_meta_box', true);
  
      $postId = get_post_id_value('publish','solusi-alcatel',$get_kategori);
      // print_r($postId);die();
      
        $name = 'dg_template_page_promo_meta_box';
        $options = get_post_meta($postId,'solusi_template_meta_box');
        // print_r($options);die();
        $selected = $dg_template_page_promo_meta_box;
        $current_template = get_post_meta( $post->ID, 'dg_template_page_promo_meta_box', true);
        // print_r($options2);
         //ambil fungsi dropdown
          if($options > 0){
            echo dg_dropdown( $name, $options, $selected, $current_template); 
         }else{
            echo "Silahkan pilih Produk Kategori"; 
         }
}

// end- TEMPLATE PAGE PROMOSI

add_action('save_post','wp_insert_post_promosi');

function wp_insert_post_promosi($post_id){
    global $post;
     // Bail if we're doing an auto save
    if(defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) return;

    // if our nonce isn't there, or we can't verify it, bail
    if( !isset( $_POST['promosi_meta_box_nonce'] ) || !wp_verify_nonce( $_POST['promosi_meta_box_nonce'], 'post_promosi_new_meta_box_nonce' ) ) return;

     // if our current user can't edit this post, bail
    if( !current_user_can( 'edit_post' ) ) return;

    $slug = "promosi-alcatel";
    if($slug != $post->post_type) return;

    if(isset($_POST['subtitle1_promosi_meta_box'])){
        update_post_meta($post_id,'subtitle1_promosi_meta_box',esc_html($_POST['subtitle1_promosi_meta_box']));
    }
     if(isset($_POST['subtitle2_promosi_meta_box'])){
        update_post_meta($post_id,'subtitle2_promosi_meta_box',esc_html($_POST['subtitle2_promosi_meta_box']));
    }
     if(isset($_POST['subtitle3_promosi_meta_box'])){
        update_post_meta($post_id,'subtitle3_promosi_meta_box',esc_html($_POST['subtitle3_promosi_meta_box']));
    }

   if(isset($_POST['subtitle4_promosi_meta_box'])){
        update_post_meta($post_id,'subtitle4_promosi_meta_box',esc_html($_POST['subtitle4_promosi_meta_box']));
    }
    if(isset($_POST['subtitle5_promosi_meta_box'])){
        update_post_meta($post_id,'subtitle5_promosi_meta_box',esc_html($_POST['subtitle5_promosi_meta_box']));
    }
      if(isset($_POST['subtitle6_promosi_meta_box'])){
        update_post_meta($post_id,'subtitle6_promosi_meta_box',esc_html($_POST['subtitle6_promosi_meta_box']));
    }
      if(isset($_POST['subtitle7_promosi_meta_box'])){
        update_post_meta($post_id,'subtitle7_promosi_meta_box',esc_html($_POST['subtitle7_promosi_meta_box']));
    }
      if(isset($_POST['subtitle8_promosi_meta_box'])){
        update_post_meta($post_id,'subtitle8_promosi_meta_box',esc_html($_POST['subtitle8_promosi_meta_box']));
    }

    if ( isset ( $_POST['post_promosi_meta_description'] ) ) {
    update_post_meta( $post_id, 'post_promosi_meta_description', $_POST['post_promosi_meta_description'] );
    }
    //description 2
  
    // if(isset($_POST['post_promosi_use_new_tab'])){
    //    $use_new_tab = 1;
    // }else{
    //     $use_new_tab = 0;
    // }
    //  update_post_meta($post_id, 'post_promosi_use_new_tab', $use_new_tab);
    // if( isset( $_POST['post_promosi_new_meta_url'] ) )
    //     update_post_meta( $post_id, 'post_promosi_new_meta_url', esc_url_raw( $_POST['post_promosi_new_meta_url'] ) );

   if( isset( $_POST['url1_promosi_new_meta_url'] ) )
      update_post_meta( $post_id, 'url1_promosi_new_meta_url', esc_url_raw( $_POST['url1_promosi_new_meta_url'] ) );
   if( isset( $_POST['url2_promosi_new_meta_url'] ) )
    update_post_meta( $post_id, 'url2_promosi_new_meta_url', esc_url_raw( $_POST['url2_promosi_new_meta_url'] ) );
   if( isset( $_POST['url3_promosi_new_meta_url'] ) )
    update_post_meta( $post_id, 'url3_promosi_new_meta_url', esc_url_raw( $_POST['url3_promosi_new_meta_url'] ) );

    if(isset($_POST['img1_upload_meta_box']))  
      update_post_meta( $post_id, 'img1_upload_meta_box', $_POST['img1_upload_meta_box'] );
   if(isset($_POST['img2_upload_meta_box']))  
    update_post_meta( $post_id, 'img2_upload_meta_box', $_POST['img2_upload_meta_box'] );
   if(isset($_POST['img3_upload_meta_box']))  
    update_post_meta( $post_id, 'img3_upload_meta_box', $_POST['img3_upload_meta_box'] );

}
add_action( 'add_meta_boxes', 'post_promosi_new_meta_box_add' );
function post_promosi_new_meta_box_add()
{
    add_meta_box( 'promosi-meta-box-id', 'Promosi Details', 'post_promosi_new_meta_box_cb', 'promosi-alcatel', 'normal', 'high' );
}

function post_promosi_new_meta_box_cb(){
  wp_nonce_field(basename(__FILE__), "meta-box-nonce");
   global $post;
   $promo_desc = get_post_meta($post->ID,'post_promosi_meta_description',true);
   $subTitle1 = get_post_meta($post->ID, 'subtitle1_promosi_meta_box', true);
   $subTitle2 = get_post_meta($post->ID, 'subtitle2_promosi_meta_box', true);
   $subTitle3 = get_post_meta($post->ID, 'subtitle3_promosi_meta_box', true);
   $subTitle4 = get_post_meta($post->ID, 'subtitle4_promosi_meta_box', true);
   $subTitle5 = get_post_meta($post->ID, 'subtitle5_promosi_meta_box', true);
   $subTitle6 = get_post_meta($post->ID, 'subtitle6_promosi_meta_box', true);
   $subTitle7 = get_post_meta($post->ID, 'subtitle7_promosi_meta_box', true);
   $subTitle8 = get_post_meta($post->ID, 'subtitle8_promosi_meta_box', true);
   $use_new_tab = get_post_meta($post->ID, 'post_promosi_use_new_tab', true);
   //$value_txt = get_post_meta($post->ID, 'post_promosi_new_meta_url', true);
   $link1_txt = get_post_meta($post->ID, 'url1_promosi_new_meta_url', true);
   $link2_txt = get_post_meta($post->ID, 'url2_promosi_new_meta_url', true);
   $link3_txt = get_post_meta($post->ID, 'url3_promosi_new_meta_url', true);

   $description = !empty($promo_desc) ? esc_html($promo_desc) : '';
   $subTtl1 = !empty($subTitle1) ? esc_html($subTitle1) : '';
   // print_r($subTtl1);die();
   $subTtl2 = !empty($subTitle2) ? esc_html($subTitle2) : '';
   $subTtl3 = !empty($subTitle3) ? esc_html($subTitle3) : '';
   $subTtl4 = !empty($subTitle4) ? esc_html($subTitle4) : '';
   $subTtl5 = !empty($subTitle5) ? esc_html($subTitle5) : '';
   $subTtl6 = !empty($subTitle6) ? esc_html($subTitle6) : '';
   $subTtl7 = !empty($subTitle7) ? esc_html($subTitle7) : '';
   $subTtl8 = !empty($subTitle8) ? esc_html($subTitle8) : '';
   $subDesc = !empty($promo_desc2) ? esc_html($promo_desc2) : '';
   $txt = !empty($value_txt) ? esc_url($value_txt ) : '';
   $txtUrl1 = !empty($link1_txt) ? esc_url($link1_txt) : '';
   $txtUrl2 = !empty($link2_txt) ? esc_url($link2_txt) : '';
   $txtUrl3 = !empty($link3_txt) ? esc_url($link3_txt) : '';

   $imgValue1 = get_post_meta($post->ID, 'img1_upload_meta_box',true);
   // print_r($imgValue1);die();
   $imgValue2 = get_post_meta($post->ID, 'img2_upload_meta_box',true);
   // print_r($imgValue2);die();
   $imgValue3 = get_post_meta($post->ID, 'img3_upload_meta_box',true);
   // print_r($imgValue3);die();
   wp_nonce_field( 'post_promosi_new_meta_box_nonce', 'promosi_meta_box_nonce' );

       //so, dont ned to use esc_attr in front of get_post_meta 
  ?>
  <p>
  <label>Sub Title 1</label>
  <input type="text" name="subtitle1_promosi_meta_box" id="subtitle1_promosi_meta_box" value="<?= $subTtl1; ?>" style="width: 100%;"></p>
  <label>Sub Title 2 (sebagai baris baru di slider promo)</label>
  <input type="text" name="subtitle2_promosi_meta_box" id="subtitle2_promosi_meta_box" value="<?= $subTtl2; ?>" style="width: 100%;"></p>
  <p><label>Jumlah User</label>
  <input type="text" name="subtitle3_promosi_meta_box" id="subtitle3_promosi_meta_box" value="<?= $subTtl3; ?>" style="width: 100%;"></p>  
  <p>
   <p>
  <label>Harga Item</label>
  <input type="text" name="subtitle4_promosi_meta_box" id="subtitle4_promosi_meta_box" value="<?= $subTtl4; ?>" style="width: 100%;"></p>
  <p>
   <p>
  <label>Slogan / Headline</label>
  <input type="text" name="subtitle5_promosi_meta_box" id="subtitle5_promosi_meta_box" value="<?= $subTtl5; ?>" style="width: 100%;"></p>
  <p>
  <?php 
    // $promo_desc=  get_post_meta($_GET['post'], 'post_promosi_meta_description' , true ) ;
    // wp_editor( htmlspecialchars_decode($promo_desc), 'post_promosi_meta_description', $settings = array('textarea_name'=>'post_promosi_meta_description') );
     $promo_desc = get_post_meta( $post->ID, 'post_promosi_meta_description', false );
  wp_editor( $promo_desc[0], 'post_promosi_meta_description' );
    ?>
    </p>
   <!--  <p>
        <input type="checkbox" id="post_promosi_use_new_tab" name="post_promosi_use_new_tab" value="1" <?= ($use_new_tab == 0)? "" : "checked" ; ?> > <label for="post_promosi_use_new_tab">Use New Tab? </label>
    </p>  -->
  <!--   <p>
        <label for="post_promosi_new_meta_url">Link Cari Promosi</label>
        <input type="text" name="post_promosi_new_meta_url" id="post_promosi_new_meta_url" value="<?php //echo $txt; ?>" style="width: 100%;" />
    </p> -->
    <hr>
  <?php  
  $image = ' button">Upload image';
  $image_size = 'full'; // it would be better to use thumbnail size here (150x150 or so)
  $display = 'none'; // display state ot the "Remove image" button
 
  if( $image_attributes = wp_get_attachment_image_src( $imgValue1, $image_size ) ) { 
    $image = '"><img src="' . $image_attributes[0] . '" style="max-width:20%;display:block;" />';
    $display = 'inline-block';
 
  } 
  $image1 = ' button">Upload image';
  $image_size1 = 'full'; // it would be better to use thumbnail size here (150x150 or so)
  $display1 = 'none';
  if( $image_attributes1 = wp_get_attachment_image_src( $imgValue2, $image_size1 ) ) { 
    $image1 = '"><img src="' . $image_attributes1[0] . '" style="max-width:20%;display:block;" />';
    $display1 = 'inline-block';
 
  } 
  $image2 = ' button">Upload image';
  $image_size2 = 'full'; // it would be better to use thumbnail size here (150x150 or so)
  $display2 = 'none';
  if( $image_attributes2 = wp_get_attachment_image_src( $imgValue3, $image_size2 ) ) { 
    $image2 = '"><img src="' . $image_attributes2[0] . '" style="max-width:20%;display:block;" />';
    $display2 = 'inline-block';
 
  } 
 
  echo '
  <div>
    <a href="#" class="misha_upload_image_button' . $image . '</a>
    <input type="hidden" name="img1_upload_meta_box" id="img1_upload_meta_box" value="' . $imgValue1 . '" />
    <a href="#" class="misha_remove_image_button" style="display:inline-block;display:' . $display . '">Remove image</a>
     <p>
        <label for="subtitle6_promosi_meta_box">Nama Produk 1</label>
        <input type="text" name="subtitle6_promosi_meta_box" id="subtitle6_promosi_meta_box" value="'.$subTitle6.'" style="width: 100%;" />
    </p>
    <p>
        <label for="url1_promosi_new_meta_url">Link Produk</label>
        <input type="text" name="url1_promosi_new_meta_url" id="url1_promosi_new_meta_url" value="'.$txtUrl1.'" style="width: 100%;" />
    </p>
  </div>
  <hr>
  <div>
    <a href="#" class="misha_upload_image_button' . $image1 . '</a>
    <input type="hidden" name="img2_upload_meta_box" id="img2_upload_meta_box" value="' . $imgValue2 . '" />
    <a href="#" class="misha_remove_image_button" style="display:inline-block;display:' . $display1 . '">Remove image</a>
    <p>
        <label for="subtitle7_promosi_meta_box">Nama Produk 1</label>
        <input type="text" name="subtitle7_promosi_meta_box" id="subtitle7_promosi_meta_box" value="'.$subTitle7.'" style="width: 100%;" />
    </p>
    <p>
        <label for="url2_promosi_new_meta_url">Link Produk</label>
        <input type="text" name="url2_promosi_new_meta_url" id="url2_promosi_new_meta_url" value="'.$txtUrl2.'" style="width: 100%;" />
    </p>
  </div>
  <hr>
  <div>
    <a href="#" class="misha_upload_image_button' . $image2 . '</a>
    <input type="hidden" name="img3_upload_meta_box" id="img3_upload_meta_box" value="' . $imgValue3 . '" />
    <a href="#" class="misha_remove_image_button" style="display:inline-block;display:' . $display2 . '">Remove image</a>
    <p>
        <label for="subtitle8_promosi_meta_box">Nama Produk 1</label>
        <input type="text" name="subtitle8_promosi_meta_box" id="subtitle8_promosi_meta_box" value="'.$subTitle8.'" style="width: 100%;" />
    </p>
    <p>
        <label for="url3_promosi_new_meta_url">Link Produk</label>
        <input type="text" name="url3_promosi_new_meta_url" id="url3_promosi_new_meta_url" value="'.$txtUrl3.'" style="width: 100%;" />
    </p>
  </div>
  '
  ;


}

//UPLOAD IMAGE

function misha_include_myuploadscript() {
  /*
   * I recommend to add additional conditions just to not to load the scipts on each page
   * like:
   * if ( !in_array('post-new.php','post.php') ) return;
   */
  if ( ! did_action( 'wp_enqueue_media' ) ) {
    wp_enqueue_media();
  }
 
  wp_enqueue_script( 'myuploadscript', get_stylesheet_directory_uri() . '/js/upload-image.js', array('jquery'), null, false );
}
 
add_action( 'admin_enqueue_scripts', 'misha_include_myuploadscript' );

// function misha_image_uploader_field( $name, $value = '') {
//   $image = ' button">Upload image';
//   $image_size = 'full'; // it would be better to use thumbnail size here (150x150 or so)
//   $display = 'none'; // display state ot the "Remove image" button
 
//   if( $image_attributes = wp_get_attachment_image_src( $value, $image_size ) ) {
 
//     // $image_attributes[0] - image URL
//     // $image_attributes[1] - image width
//     // $image_attributes[2] - image height
 
//     $image = '"><img src="' . $image_attributes[0] . '" style="max-width:20%;display:block;" />';
//     $display = 'inline-block';
 
//   } 
 
//   return '
//   <div>
//     <a href="#" class="misha_upload_image_button' . $image . '</a>
//     <input type="hidden" name="' . $name . '" id="' . $name . '" value="' . $value . '" />
//     <a href="#" class="misha_remove_image_button" style="display:inline-block;display:' . $display . '">Remove image</a>
//   </div>';
// }

// add_action( 'admin_menu', 'misha_meta_box_add' );
 
// function misha_meta_box_add() {
//   add_meta_box('mishadiv', // meta box ID
//     'More settings', // meta box title
//     'misha_print_box', // callback function that prints the meta box HTML 
//     'promosi-alcatel', // post type where to add it
//     'normal', // priority
//     'high' ); // position
// }
 
/*
 * Meta Box HTML
 */
// function misha_print_box( $post ) {
//   $meta_key = 'second_featured_img';
//   echo misha_image_uploader_field( $meta_key, get_post_meta($post->ID, $meta_key, true) );
// }
 
/*
 * Save Meta Box data
 */
// add_action('save_post', 'misha_save');
 
// function misha_save( $post_id ) {
//   if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE ) 
//     return $post_id;
 
//   $meta_key = 'second_featured_img';
 
//   update_post_meta( $post_id, $meta_key, $_POST[$meta_key] );
 
//   // if you would like to attach the uploaded image to this post, uncomment the line:
//   // wp_update_post( array( 'ID' => $_POST[$meta_key], 'post_parent' => $post_id ) );
 
//   return $post_id;
// }
