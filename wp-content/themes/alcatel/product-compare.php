<?php /* Template Name: Product-Compare */ ?>

<?php get_header('nav');  ?> ?>
 

<a class="btn-expand" data-toggle="collapse" data-target=".switch-detail" aria-expanded="false"><em class="fa fa-chevron-right"></em>   Expand</a>

<div id="primary" class="content-area">
    <main id="main" class="site-main" role="main">
        <?php
        // Start the loop.
        while ( have_posts() ) : the_post();
 
            // Include the page content template.
            get_template_part( 'template-parts/content', 'page' );
 
            // If comments are open or we have at least one comment, load up the comment template.
            if ( comments_open() || get_comments_number() ) {
                comments_template();
            }
 
            // End of the loop.
        endwhile;
        ?>
 
    </main><!-- .site-main -->
 
</div><!-- .content-area -->
 
<?php get_template_part('contact_us'); ?>

<?php get_footer(); ?>

<script type="text/javascript">
    jQuery(document).ready(function() {
        jQuery('#slider_693').owlCarousel({
            responsive:{
                0:{ items:2 },
                480:{ items:2 },
                768:{ items:2 },
                980:{ items:4 },
                1200:{ items:4 },
                1500:{ items:4 }
            },
            autoplay : true,
            autoplayTimeout : 5000,
            smartSpeed : 200,
            fluidSpeed : 200,
            autoplaySpeed : 200,
            navSpeed : 200,
            dotsSpeed : 200,
            loop : true,
            autoplayHoverPause : true,
            nav : true,
            navText : ['',''],
            dots : true,
            responsiveRefreshRate : 200,
            slideBy : 1,
            mergeFit : true,
            mouseDrag : true,
            touchDrag : true
        });
        sa_resize_slider_693();
        window.addEventListener('resize', sa_resize_slider_693);
        function sa_resize_slider_693() {
            var min_height = '50';
            var win_width = jQuery(window).width();
            var slider_width = jQuery('#slider_693').width();
            if (win_width < 480) {
                var slide_width = slider_width / 2;
            } else if (win_width < 768) {
                var slide_width = slider_width / 2;
            } else if (win_width < 980) {
                var slide_width = slider_width / 2;
            } else if (win_width < 1200) {
                var slide_width = slider_width / 4;
            } else if (win_width < 1500) {
                var slide_width = slider_width / 4;
            } else {
                var slide_width = slider_width / 4;
            }
            slide_width = Math.round(slide_width);
            var slide_height = '0';
            if (min_height == 'aspect43') {
                slide_height = (slide_width / 4) * 3;               slide_height = Math.round(slide_height);
            } else if (min_height == 'aspect169') {
                slide_height = (slide_width / 16) * 9;              slide_height = Math.round(slide_height);
            } else {
                slide_height = (slide_width / 100) * min_height;                slide_height = Math.round(slide_height);
            }
            jQuery('#slider_693 .owl-item .sa_hover_container').css('min-height', slide_height+'px');
        }
    });
</script>
<script type="text/javascript">
	$(document).ready(function()
		{

			$('.btn-expand').each(function()
				{
					$(this).click(function()
						{
							var _href = $(this).attr('href');
							var els = $('a[href="' + _href + '"] em');
							console.log(els);
							if (els.length > 0)
							{
								for (var i = 0;i < els.length;i++) {
									if(els[i].classList.contains('fa-chevron-right'))
									{
										els[i].classList.remove("fa-chevron-right");
										els[i].classList.add("fa-chevron-down");
									}

									else 
									{
										els[i].classList.remove("fa-chevron-down");
										els[i].classList.add("fa-chevron-right");
									}
								}
							}
						});
				});
		});
</script>
<style>
.entry-title {
    margin-top: -30px;
    margin-bottom: 20px;
}

.btn-expand
{
	display: block;
	width:100%;
	/*position:absolute;
	z-index:9999;
	top:375px;*/
	text-align:left;
	background-color: #7e5eaa;
	color:#fff;
	border:none;
	padding:10px 25px;
	box-sizing:border-box;
	text-decoration:none;
	text-transform:uppercase;
}
.btn-expand:hover
{
	text-decoration:none;
	color:#fff;
}

.btn-expand em
{
	display:inline-block;
	margin-right:10px;
}

.owl-item p
{
	/*padding-top:75px;*/
}

.owl-carousel .owl-item img
{
	/*max-height:100px;*/
}

.owl-item h3
{
	min-height:60px;
}

.OS2220-24
{
	height: 100px !important;
}

@media (max-width: 576px)
{
	.owl-carousel .owl-item img
	{
		max-height:40px;
	}

	.owl-item h3
	{
		min-height:110px;
	}

	.OS2220-24
	{
		height: auto !important;
	}
}

@media (min-width:576px) {
    
}

@media (min-width:768px) {
    
}

@media (min-width:992px) {
    
}

@media (min-width:1200px) {
    
}

@media (min-width:1400px) {
    
}
</style>