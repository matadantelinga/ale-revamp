<?php 
/**
 * Template Name: Single Product Page Template
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

get_header('nav');
the_post();
?>

<!--Single Product -->


<section id="single-product-page">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                        <?php 

                    $images = get_field('product_gallery');

                    if( $images ): ?>
            
                    <div id="myCarousel" class="carousel slide" data-ride="carousel">

                            <!-- Wrapper for slides -->
                        <div class="carousel-inner">
                        <?php foreach( $images as $image ): ?>
                            <div class="item">
                                <img src="<?php echo $image['sizes']['large']; ?>" alt="<?php echo $image['alt']; ?>" style="width:100%;" />
                            </div>
                        <?php endforeach; ?>
                        </div>

                            <!-- Left and right controls -->
                            <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                            <span class="glyphicon glyphicon-chevron-left"></span>
                            <span class="sr-only">Previous</span>
                            </a>
                            <a class="right carousel-control" href="#myCarousel" data-slide="next">
                            <span class="glyphicon glyphicon-chevron-right"></span>
                            <span class="sr-only">Next</span>
                            </a>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="fitur-detail-title"><h3><?php the_title(); ?></h3></div>
                <?php  the_field('produk_fitur_detail');?>
            </div>
        </div>
    </div>
</section>

<?php get_template_part('contact_us'); ?>
<?php get_footer(); ?>