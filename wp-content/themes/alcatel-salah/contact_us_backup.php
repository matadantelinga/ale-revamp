<?php 
global $wpdb;
$tbl_name = $wpdb->prefix. "contact_us";
$produk = '';
$errors = new WP_Error();   
if('POST'== $_SERVER['REQUEST_METHOD'] && !empty($_POST['action']) && $_POST['action']=='submit-form'){
     $recaptcha_secret = "6LfyyRcUAAAAAJWX_BjL4SIy8kVHY5UR8Bu8SjdC";
        $response = file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=".$recaptcha_secret."&response=".$_POST['g-recaptcha-response']);
        $response = json_decode($response, true);
       
    $username = $wpdb->escape(trim($_POST['sws_inputName']));
    $company = $wpdb->escape(trim($_POST['sws_inputCompany']));
    $email = $wpdb->escape(trim($_POST['sws_inputEmail']));
    $tlp = $wpdb->escape(trim($_POST['sws_inputTlp']));
    $phone = $wpdb->escape(trim($_POST['sws_inputPhone']));
    $produk = $wpdb->escape(trim($_POST['options']['sws_inputProduk']));
    $message = $wpdb->escape(trim($_POST['sws_inputMsg']));
    date_default_timezone_set("Asia/Bangkok");
    $date = date('Y-m-d h:i:sa');  

   $kv_data = array( 
                 'name'         => $username
                ,'perusahaan'   => $company
                ,'email'        => $email
                ,'telepon'      => $tlp
                ,'phone'        => $phone
                ,'produk'       => $produk
                ,'pesan'        => $message
                ,'date_created' => $date
            ) ; 
   // print_r($kv_data);die();
     // if(isset($_POST['g-recaptcha-response']) && !empty($_POST['g-recaptcha-response'])){
       
          if($response["success"] === false)
          {
             $err = 'Please click on the reCAPTCHA box.';
          }else if(empty($username)|| empty($company) || empty($email) || empty($tlp) || empty($phone) || empty($produk) || empty($message)){
              $err = 'Please don\'t leave the required fields.';
              // echo $err;die();
          }else if(!filter_var($email, FILTER_VALIDATE_EMAIL)){
              $err = 'Invalid email address.';
              // echo $err;die();
          }else if(email_exists($email)){
              $err = 'Email already exist.';
              // echo $err;die();
          }else{
              // echo $tbl_name;die();
             $tambah = $wpdb->insert($tbl_name, $kv_data);
             if($tambah){
                redirect('terima-kasih'); 
                wp_mail('ezi@dgtraffic.com', 'Notification', 'Test notification email');
                $success = 'Data Berhasil di Tambahkan';
             }
             
              // $tambah = $wpdb->insert($table_name, $kv_data,
              //   array('%s','%s','%s','%s','%s','%s','%s'));
                // if($tambah) $message = "Data berhasil ditambahkan";
                // echo $message;
          }

  }

?>
<!-- HUBUNGI KAMI -->
<section>
    <article>
        <div class="wrap-contact | contact-view">
            <!-- <div class="icon-online | icon-online-view">
                <a href="#"><img src="<?= esc_url(get_option('siteurl')) ?>/wp-content/assets/img/icon/icon-online.png" alt=""></a>
            </div> -->
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="title-contact">
                            <h3>hubungi kami</h3>
                            <p>pertanyaan tentang produk, 
                            perusahaan dan lainnya.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4 | col-md-offset-1 | col-sm-6">
                        <div class="info-contact">
                            <?php dynamic_sidebar('contact-bar'); ?>
                        </div>
                    </div>
                   
                    <div class="padding_article">
                    <div class="col-md-6 | col-sm-6">
                        <div class="form-contact">
                          <?php if(isset($success) && $success):?>
                            <div class="alert alert-success">
                              <a href="#" class="close" data-dismiss="alert">&times;</a>
                              <center><strong><?= $success; ?></strong></center>
                            </div>
                          <?php endif; 
                            if(isset($err) && $err):?>
                            <div class="alert alert-danger">
                              <a href="#" class="close" data-dismiss="alert">&times;</a>
                              <center><strong><?= $err; ?></strong></center>
                            </div>
                          <?php endif; ?>
                            <form action="" name="contactForm" method="post" role="form">
                                  <div class="form-group">
                                      <input type="text" class="form-control" name="sws_inputName" id="sws_inputName" value="" placeholder="Nama" required >
                                  </div>
                                  <div class="form-group">
                                      <input type="text" class="form-control" name="sws_inputCompany" id="sws_inputCompany" value="" placeholder="Perusahaan" required>
                                  </div>
                                  <div class="form-group">
                                      <input type="email" class="form-control" name="sws_inputEmail" value="" id="sws_inputEmail" placeholder="Email" data-error="Sorry, that email address is invalid" required>
                                      <div class="help-block with-errors"></div>
                                  </div>
                                  <div class="form-group">
                                      <input type="text" class="form-control" value="" name="sws_inputTlp" id="sws_inputTlp" placeholder="No. Telepon" required>
                                  </div>
                                  <div class="form-group">
                                      <input type="text" class="form-control" name="sws_inputPhone" value="" id="sws_inputPhone" placeholder="No. Handphone" required>
                                  </div>
                                  

                                  <select name="options[sws_inputProduk]" id="select" class="form-control selectpicker" >
                                  <!-- <select name="select" id="select" class="form-control selectpicker" > -->
                                      <option value="" <?php selected($produk,'');?>>Produk</option>
                                      <option <?php selected($produk,'OXO CONNECT R2');?> value="OXO CONNECT R2">OXO CONNECT R2</option>
                                      <option <?php selected($produk,'OMNISWITCH 6350');?> value="OMNISWITCH 6350">OMNISWITCH 6350</option>
                                      <option <?php selected($produk,'WIFI OMNIACCESS AP 1101');?> value="WIFI OMNIACCESS AP 1101">WIFI OMNIACCESS AP 1101</option>
                                  </select>

                                  <textarea class="form-control" name="sws_inputMsg" rows="5" id="comment" value="" placeholder="Permintaan Khusus"></textarea>
                                 

                                  <div class="wrap-captcha">
                                       <div class="g-recaptcha" data-sitekey="6LfyyRcUAAAAAPxG6ATLIqg7ylJ1N_itRddobh_I"></div>
                                  </div>

                                  <div class="btn-contact | form-group">
                                      <input type="hidden" name="action" value="submit-form">
                                      <button type="submit" class="btn btn-warning">kirim</button>
                                  </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </article>
</section>

