 <article>
        <div class="wrap-content-wifi">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="top-title | wifi-title | wow fadeInDown">
                            <h3><?= $SubTitle1; ?><br><span><?= $SubTitle2; ?></span></h3>
                            <p class="sub-left-content | sub-wifi"><?= $SubTitle3;?></p>
                        </div>
                    </div>
                    <div class="col-md-7 | col-sm-7">
                        <div class="left-content | wow fadeInUp">
                            <p class="txt-left-content | txt-wifi"><?= $Desc1; ?>
                            </p>
                            <a class="btn-content | wifi-btn" href="<?= get_permalink($get_solusi[2]->ID); ?>">selengkapnya<i class="fa fa-long-arrow-right"></i></a>
                        </div>
                    </div>
                    <div class="col-md-5 | col-sm-5">
                        <div class="right-content | img-wifi | wow fadeInRight">
                            <img src="<?= $img1; ?>" alt="<?= $getTitle;?>" title="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </article>