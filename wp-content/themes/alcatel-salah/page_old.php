<?php
/**
 * The template for displaying pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
?>


<?php 
	get_header();

	// $page_featured_img = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ) , "full" );
?>
	<div class="padding-top"></div>

	<section class="middle-section padding-bottom20">
		

		<div class="content padding-top40 padding-bottom20 clearfix">
			<div class="container">
				<div class="row">
					<?php
					while ( have_posts() ) : the_post();
					?>
					<center><h1><?php echo get_the_title() ?></h1></center><br>
					<div class="content-box col-lg-7 col-md-7 col-sm-6 col-xs-12 map">
						<div class="content">
							<div id="googleMap" style="background: #eeeeee; width: 100%; height:400px;"></div>
						</div>
					</div>
					<div class="content-box col-lg-5 col-md-5 col-sm-6 col-xs-12">
						<div class="content">
							<div class="content-form">
								<?php the_content(); ?>
							</div>
						</div>
					</div>
					<?php	
					endwhile;
					?>
				</div>
			</div>
		</div>
		<div class="content padding-top40 padding-bottom20">
			<div class="container">
				<div class="row">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<div class="content clearfix">
							<ul class="contact-list clearfix">
								<?php dynamic_sidebar('contact-bar'); ?>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

<?php get_footer(); ?>