<?php
/**
 * Twenty Sixteen functions and definitions
 *
 * Set up the theme and provides some helper functions, which are used in the
 * theme as custom template tags. Others are attached to action and filter
 * hooks in WordPress to change core functionality.
 *
 * When using a child theme you can override certain functions (those wrapped
 * in a function_exists() call) by defining them first in your child theme's
 * functions.php file. The child theme's functions.php file is included before
 * the parent theme's file, so the child theme functions would be used.
 *
 * @link https://codex.wordpress.org/Theme_Development
 * @link https://codex.wordpress.org/Child_Themes
 *
 * Functions that are not pluggable (not wrapped in function_exists()) are
 * instead attached to a filter or action hook.
 *
 * For more information on hooks, actions, and filters,
 * {@link https://codex.wordpress.org/Plugin_API}
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

/**
 * Twenty Sixteen only works in WordPress 4.4 or later.
 */
//SELECT JOIN POST AND POSTMETA
if (!function_exists('get_post_id_based_on_template')) {
function get_post_id_based_on_template($type,$key) {
  global $wpdb;
  $querystr= "SELECT * 
      FROM $wpdb->posts
      LEFT JOIN $wpdb->postmeta ON($wpdb->posts.ID = $wpdb->postmeta.post_id)
      WHERE $wpdb->posts.post_type = '".$type."' 
      AND $wpdb->posts.post_status = 'publish'
      AND $wpdb->postmeta.meta_value='".$key."'
      ORDER BY $wpdb->postmeta.meta_value DESC";
      $pageposts = $wpdb->get_results($querystr, OBJECT);
  return $pageposts;
 }
}
//SELECT FILTER FUNCTION
if (!function_exists('get_id_from_posts')) {
function get_id_from_posts($status,$type) {
  global $wpdb;
  $querystr = "
    SELECT *
    FROM $wpdb->posts
    WHERE $wpdb->posts.post_status = '".$status."'
      AND $wpdb->posts.post_type = '".$type."' 
      ORDER BY $wpdb->posts.ID ASC
 ";
  $pageposts = $wpdb->get_results($querystr, OBJECT);
  return $pageposts;
 }
}
if (!function_exists('get_name_from_posts')) {
function get_name_from_posts($status,$type,$name) {
  global $wpdb;
  // $status='publish';
  // $type='promosi-alcatel';
  // $name='oxo-connect-r2';
  $querystr = "
    SELECT *
    FROM $wpdb->posts
    WHERE $wpdb->posts.post_status = '".$status."'
      AND $wpdb->posts.post_type = '".$type."'
      AND $wpdb->posts.post_name = '".$name."'
      ORDER BY $wpdb->posts.ID ASC
 ";
  $pageposts = $wpdb->get_results($querystr, OBJECT);
  return $pageposts;
 }
}
if (!function_exists('get_distintct_post_id_by_meta_key_and_value')) {
function get_distintct_post_id_by_meta_key_and_value($key) {
  global $wpdb;
  $querystr = "
    SELECT DISTINCT $wpdb->postmeta.meta_value, $wpdb->postmeta.post_id
    FROM $wpdb->posts, $wpdb->postmeta
    WHERE $wpdb->posts.ID = $wpdb->postmeta.post_id 
      AND $wpdb->posts.post_status = 'publish' 
      AND $wpdb->posts.post_type = 'promosi-alcatel'
      AND $wpdb->postmeta.meta_key = '".$key."' 
     
    GROUP BY $wpdb->postmeta.meta_value
 ";
  $pageposts = $wpdb->get_results($querystr, OBJECT);
  return $pageposts;
 }
}
if (!function_exists('get_template_solusi_by_meta_key_and_postid')) {
function get_template_solusi_by_meta_key_and_postid($id) {
  global $wpdb;
  $querystr = "
    SELECT *
    FROM $wpdb->postmeta
    WHERE  $wpdb->postmeta.meta_key = 'solusi_template_meta_box'
      AND $wpdb->postmeta.post_id = '".$id."' 
 ";
  $pageposts = $wpdb->get_results($querystr, OBJECT);
  return $pageposts;
 }
}
if (!function_exists('get_post_id_by_meta_key_and_value')) {
 function get_post_id_by_meta_key_and_value($key, $id) {
  global $wpdb;
  $meta = $wpdb->get_results("SELECT * FROM `".$wpdb->postmeta."` WHERE meta_key='".$wpdb->escape($key)."' AND post_id='".$wpdb->escape($id)."'");
  if (is_array($meta) && !empty($meta) && isset($meta[0])) {
    $meta = $meta[0];
    } 
  if (is_object($meta)) {
    return $meta->meta_value;
    }
  else {
    return false;
    }
  }
}
//SELECT FILTER FUNCTION
if (!function_exists('get_post_id_value')) {

 function get_post_id_value($key, $value,$value2) {
  global $wpdb;
  $meta = $wpdb->get_results("SELECT * FROM `".$wpdb->posts."` WHERE post_status='".$wpdb->escape($key)."' AND post_type='".$wpdb->escape($value)."'AND post_title='".$wpdb->escape($value2)."'");
  if (is_array($meta) && !empty($meta) && isset($meta[0])) {
    $meta = $meta[0];
    } 
  if (is_object($meta)) {
    return $meta->ID;
    }
  else {
    return false;
    }
  }
}
//DROP DOWN FUNCTION
function dg_dropdown( $name, array $options, $selected,$value)
  {
    /*** begin the select ***/
    $dropdown = '<select name="'.$name.'" id="'.$name.'">'."\n";
   
    $selected = $selected;
    /*** loop over the options ***/
    foreach( $options as $key=>$option )
    {
        /*** assign a selected value ***/
        // echo $key;die();
        $select = $selected==$key ? 'selected' : null;
        if($value == $option ){
           /*** add each option to the dropdown ***/
           $dropdown .= '<option value="' . $option . '" selected="selected">' . $option . '</option>';
        }else{
          // echo 'jangkrik bos';die();
           $dropdown .= '<option value="'.$option.'"'.$select.'>'.$option.'</option>'."\n";
        }
       
    }
    /*** close the select ***/
    $dropdown .= '</select>'."\n";

    /*** and return the completed dropdown ***/
    return $dropdown;
  }
//SORT PROMO PAGE
function getPromo($getTemplate){
    if($getTemplate=='oxo-connect-r2'){
        include('promo-oxo-connect.php');
    }else if($getTemplate=='omniswitch-6350'){
        include('promo-omni-switch.php');
    }else if($getTemplate=='wifi-omniAccess-ap-1101'){
        include('promo-wifi-omniaccess.php');
    }
    else{
      include('promo-oxo-connect.php');
    }
}
// function getProdukPromo($getTemplate){
//     if($getTemplate=='oxo-connect-r2'){
//         include_once('promo-oxo-connect.php');
//     }
//     if($getTemplate=='omniswitch-6350'){
//         include_once('promo-omni-switch.php');
//     }
//     if($getTemplate=='wifi-omniAccess-ap-1101'){
//         include_once('promo-wifi-omniaccess.php');
//     }
// }
//SORT IN BLOG
function sortIt($sortType)
{
    global $wp_query;
   // $cat_ID = get_query_var('Blog');
   // print_r($cat_ID);die();

    if ($sortType == 'tanggal')
    {
        $postslist = new WP_Query( array('posts_per_page' => 6
                           ,'category_name' => 'Blog'
                           ,'orderby'       => 'date'
                           ,'order'         => 'ASC'
                           ,'post_type'     => 'post'
                           ,'paged'         => $paged
                           ,'post_status'   => 'publish') );
    }
    if ($sortType == 'judul'){
        $postslist = new WP_Query( array('posts_per_page' => 6
                           ,'category_name' => 'Blog'
                           ,'orderby'       => 'title'
                           ,'order'         => 'ASC'
                           ,'post_type'     => 'post'
                           ,'paged'         => $paged
                           ,'post_status'   => 'publish') );
    }
    if($sortType==''){
        $postslist = new WP_Query( array('posts_per_page' => 6
                           ,'category_name' => 'Blog'
                           ,'orderby'       => 'title'
                           ,'order'         => 'DESC'
                           ,'post_type'     => 'post'
                           ,'paged'         => $paged
                           ,'post_status'   => 'publish') );
    }

    // if (strcmp($sortType, 'alphabetical') == 0 )
    // {
    //     $newQuery = new WP_Query( array( 'orderby' => 'title' , 'cat' => $cat_ID, 'posts_per_page' => '10') );
    // }

    return $postslist;
}

//END SORT
// FILE UPLOAD
include_once('template-parts/side-upload-file.php');
function add_custom_attachment_script() {
 
    wp_register_script('custom-attachment-script', get_stylesheet_directory_uri() . '/js/custom_attachment.js');
    wp_enqueue_script('custom-attachment-script');
 
} // end add_custom_attachment_script
add_action('admin_enqueue_scripts', 'add_custom_attachment_script');

//ADD MENU SOLUSI
include_once('template-parts/solusi-type.php');

//ADD MENU PROMO
include_once('template-parts/promo-type.php');

// include_once('template-parts/promo-page.php');
//add new menu slider

$labels = array(
  'name'               => _x( 'Sliders', 'post type general name', 'your-plugin-textdomain' ),
  'singular_name'      => _x( 'Slider', 'post type singular name', 'your-plugin-textdomain' ),
  'menu_name'          => _x( 'Sliders', 'admin menu', 'your-plugin-textdomain' ),
  'name_admin_bar'     => _x( 'Slider', 'add new on admin bar', 'your-plugin-textdomain' ),
  'add_new'            => _x( 'Add New', 'book', 'your-plugin-textdomain' ),
  'add_new_item'       => __( 'Add New Slider', 'your-plugin-textdomain' ),
  'new_item'           => __( 'New Slider', 'your-plugin-textdomain' ),
  'edit_item'          => __( 'Edit Slider', 'your-plugin-textdomain' ),
  'view_item'          => __( 'View Slider', 'your-plugin-textdomain' ),
  'all_items'          => __( 'All Sliders', 'your-plugin-textdomain' ),
  'search_items'       => __( 'Search Sliders', 'your-plugin-textdomain' ),
  'parent_item_colon'  => __( 'Parent Sliders:', 'your-plugin-textdomain' ),
  'not_found'          => __( 'No sliders found.', 'your-plugin-textdomain' ),
  'not_found_in_trash' => __( 'No sliders found in Trash.', 'your-plugin-textdomain' )
);

$args = array(
  'labels'             => $labels,
  'description'        => __( 'Description.', 'your-plugin-textdomain' ),
  'public'             => false,
  'publicly_queryable' => false,
  'show_ui'            => true,
  'show_in_menu'       => true,
  'query_var'          => true,
  'rewrite'            => array( 'slug' => 'slider_home' ),
  'capability_type'    => 'post',
  'has_archive'        => true,
  'hierarchical'       => false,
  'menu_position'      => null,
  'supports'           => array( 'title', 'thumbnail', 'page-attributes' )
);

register_post_type( 'slider_home', $args );

add_action( 'save_post', 'home_slider_new_meta_box_save' );

add_action( 'add_meta_boxes', 'home_slider_new_meta_box_add' );
function home_slider_new_meta_box_add()
{
    add_meta_box( 'my-meta-box-id', 'Slider Details', 'home_slider_new_meta_box_cb', 'slider_home', 'normal', 'high' );
}

function home_slider_new_meta_box_save( $post_id )
{
    global $post;
    // print_r($_POST); die();
    // if( $_POST['page_template'] != 'page-product.php' ) return; //WILL ONLY WORK IN PRODUCT PAGE TEMPLATE

    // Bail if we're doing an auto save
    if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) return;
     
    // if our nonce isn't there, or we can't verify it, bail
    if( !isset( $_POST['meta_box_nonce'] ) || !wp_verify_nonce( $_POST['meta_box_nonce'], 'home_slider_new_meta_box_nonce' ) ) return;
     
    // if our current user can't edit this post, bail
    if( !current_user_can( 'edit_post' ) ) return;

    $slug = "slider_home";
    if($slug != $post->post_type) return;

    if( isset( $_POST['home_slider_meta_use_new_tab'] ) ) 
        $use_new_tab = 1;
    else
        $use_new_tab = 0;

    update_post_meta( $post_id, 'home_slider_meta_use_new_tab', $use_new_tab );
     
     // print_r($_POST['desc_metaname_value']);die();
    //Make sure your data is set before trying to save it
    if( isset( $_POST['home_slider_new_meta_url'] ) )
        update_post_meta( $post_id, 'home_slider_new_meta_url', esc_url_raw( $_POST['home_slider_new_meta_url'] ) );
    if( isset( $_POST['home_slider_meta_description'] ) )
        update_post_meta( $post_id, 'home_slider_meta_description', esc_html( $_POST['home_slider_meta_description'] ) );
    if(isset($_POST['home_slider_meta_subtitle']))
        update_post_meta( $post_id, 'home_slider_meta_subtitle', esc_html( $_POST['home_slider_meta_subtitle']) );

    if (!empty($_POST['desc_metaname_value']))
        {
          $datta=htmlspecialchars($_POST['desc_metaname_value']);
          update_post_meta($post_id, 'desc_metaname_value', $datta );
        }
   
   // RADIO BUTTON
    $radio_meta_value = ( isset( $_POST['home_slider_radio_buttons'] ) ? sanitize_html_class( $_POST['home_slider_radio_buttons'] ) : '' );
    // Update the meta field in the database.
        update_post_meta( $post_id, 'home_slider_radio_buttons', $radio_meta_value );
}


//add_filter( 'walker_nav_menu_start_el', 'bm_nav_description', 10, 4 );
function home_slider_new_meta_box_cb()
{

  wp_nonce_field(basename(__FILE__), "meta-box-nonce");

    // $post is already set, and contains an object: the WordPress post
    global $post;

    $post_id = get_option('my_option');
    $use_new_tab = get_post_meta($post->ID, "home_slider_meta_use_new_tab", true);
    $values_text = get_post_meta($post->ID, "home_slider_new_meta_url", true);
    $values_desc = get_post_meta($post->ID, "home_slider_meta_description", true);
    $values_subTitle = get_post_meta($post->ID, "home_slider_meta_subtitle", true);
    $text = !empty( $values_text ) ? esc_url($values_text) : '';
    $desc = !empty( $values_desc ) ? esc_html($values_desc) : '';
    $subTitle = !empty($values_subTitle) ? esc_html($values_subTitle) : '';
    // We'll use this nonce field later on when saving.
    
    wp_nonce_field( 'home_slider_new_meta_box_nonce', 'meta_box_nonce' );
    ?>
  <!--   <p>
        <label for="home_slider_meta_subtitle">Sub Title</label>
        <input type="text" name="home_slider_meta_subtitle" id="home_slider_meta_subtitle" value="<?= $subTitle; ?>" style="width: 100%;">
    </p> -->
   
    <!-- <p>
        Notes :<br/>
        - Size Slider : 1600px(W) X 626px(H)
    </p> -->
    <?php 
       //so, dont ned to use esc_attr in front of get_post_meta
    $values_desc=  get_post_meta($_GET['post'], 'desc_metaname_value' , true ) ;
    wp_editor( htmlspecialchars_decode($values_desc), 'desc_metaname_value', $settings = array('textarea_name'=>'desc_metaname_value') );

     $radio_value = get_post_meta( $post->ID, 'home_slider_radio_buttons', true );
    ?>
    <p>
        <input type="checkbox" id="home_slider_meta_use_new_tab" name="home_slider_meta_use_new_tab" value="1" <?= ($use_new_tab == 0)? "" : "checked" ; ?> > <label for="home_slider_meta_use_new_tab">Use New Tab? </label>
    </p> 
    <p>
        <label for="home_slider_new_meta_url">Slider Url</label>
        <input type="text" name="home_slider_new_meta_url" id="home_slider_new_meta_url" value="<?php echo $text; ?>" style="width: 100%;" />
    </p>
   
    <!-- <p>
        <label for="home_slider_meta_description">Description </label><br>
        <textarea id="home_slider_meta_description" name="home_slider_meta_description" style="width: 100%; height: 200px"><?php echo $desc; ?></textarea>
    </p> -->

    <style type="text/css">
      label.slider-button{
            margin-right: 10px;
      }
      label.slider-button span{
        display: inline-block;
        height: 15px;
        width: 15px;
        background: transparent;
        margin-right: 5px;
        vertical-align: sub;
      }
    </style>

     <label for="wdm_new_field"><?php _e( "Pilih Warna Slider:", 'choose_value' ); ?></label>
        <br /><br/>  
        <label class="slider-button"><input type="radio" name="home_slider_radio_buttons" value="Biru" <?php checked( $radio_value, 'Biru' ); ?> ><span style="background: #0085CA;"></span>Biru &nbsp;</label>
        <label class="slider-button">
        <input type="radio" name="home_slider_radio_buttons" value="Hijau" <?php checked( $radio_value, 'Hijau' ); ?> ><span style="background: #34B233;"></span>Hijau &nbsp;</label>
        <label class="slider-button">
        <input type="radio" name="home_slider_radio_buttons" value="Toska" <?php checked( $radio_value, 'Toska' ); ?> ><span style="background: #00B9E1;"></span>Biru Langit (Toska) &nbsp;</label>
        <label class="slider-button">
         <input type="radio" name="home_slider_radio_buttons" value="Orange" <?php checked( $radio_value, 'Orange' ); ?> ><span style="background: #FF4500;"></span>Orange &nbsp;</label>
         <label>
        <input type="radio" name="home_slider_radio_buttons" value="Merah" <?php checked( $radio_value, 'Merah' ); ?> ><span style="background: #A50034;""></span>Merah &nbsp; </label>
        <label class="slider-button">
         <input type="radio" name="home_slider_radio_buttons" value="Ungu" <?php checked( $radio_value, 'Ungu' ); ?> ><span style="background: #921B9B;"></span>Ungu &nbsp; </label>
        
    <?php    
}
/*
 * Enable support for Post Thumbnails on posts and pages.
 *
 * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
 */
// Add Custom image sizes
// Note: 'true' enables hard cropping so each image is exactly those dimensions and automatically cropped
add_theme_support( 'post-thumbnails' );
set_post_thumbnail_size( 1200, 9999 );
/** Add secondary thumbnail */

/* Add secondary thumbnail slider_home (featured image) in posts */
$thumb = new MultiPostThumbnails(
  array(
   'label' => 'Mobile Image',
   'id' => 'secondary-image',
   'post_type' => 'slider_home'
 )
);
add_image_size('post-secondary-image-thumbnail', 250, 150);
 if (class_exists('MultiPostThumbnails')) :
    MultiPostThumbnails::the_post_thumbnail(
        get_post_type(),
        'secondary-image'
    );
endif;
array('class'=>'','title'=>'','alt'=>'');

/* Add secondary thumbnail post-solusi (featured image) in posts */
$thumb = new MultiPostThumbnails(
  array(
   'label' => 'Image Large',
   'id' => 'secondary-image',
   'post_type' => 'solusi-alcatel'
 )
);
add_image_size('post-secondary-image-thumbnail', 1300, 1000);
 if (class_exists('MultiPostThumbnails')) :
    MultiPostThumbnails::the_post_thumbnail(
        get_post_type(),
        'secondary-image'
    );
endif;
array('class'=>'','title'=>'','alt'=>'');

/* Add secondary thumbnail page (featured image) in posts */
// $thumb = new MultiPostThumbnails(
//   array(
//    'label' => 'Zoom Image',
//    'id' => 'zoom-image-page',
//    'post_type' => 'page'
//  )
// );
// add_image_size('page-secondary-image-thumbnail', 1300, 1000);
//  if (class_exists('MultiPostThumbnails')) :
//     MultiPostThumbnails::the_post_thumbnail(
//         get_post_type(),
//         'secondary-image'
//     );
// endif;
// array('class'=>'','title'=>'','alt'=>'');
/**
 * Registers a widget area.
 *
 * @link https://codex.wordpress.org/Widgetizing_Themes
 *
 * @since Twenty Sixteen 1.0
 */

/**
 * Register our sidebars and widgetized areas.
 *
 */
function alcatel_widgets_init() {

    register_sidebar( array(
        'name' => __( 'Footer Bar', 'footer-bar' ),
        'id' => 'footer-bar',
        'description' => __( 'Widgets for footer', 'footer-bar' ),
        'before_widget' => '',
        'after_widget'  => '',
        'before_title'  => '<div class="footer-left-title"><h5 class="widgettitle">',
        'after_title'   => '</h5></div>',
    ) );
    //  register_sidebar( array(
    //     'name' => __( 'About Us Video', 'about-bar' ),
    //     'id' => 'about-bar',
    //     'description' => __( 'Widgets for contact ', 'about-bar' ),
    //     'before_widget' => '',
    //     'after_widget'  => '',
    //     'before_title'  => '',
    //     'after_title'   => '',
    // ) );
   
    register_sidebar( array(
        'name' => __( 'Alamat', 'contact-bar' ),
        'id' => 'contact-bar',
        'description' => __( 'Widgets for contact ', 'contact-bar' ),
        'before_widget' => '',
        'after_widget'  => '',
        'before_title'  => '<div class="info-txt widgettitle"><h3 class="info-color">',
        'after_title'   => '</h3><img src="'.esc_url( home_url('/')).'wp-content/assets/img/logo/aca-right-black.png" alt="Alcatel Lucent Enterprise"></div>',
    ) );
                             
    // register_sidebar( array(
    //     'name' => __( 'Email Enquiry', 'email-enquiry' ),
    //     'id' => 'email-enquiry',
    //     'description' => __( 'Widgets for.', 'email-enquiry' ),
    //     'before_widget' => '',
    //     'after_widget'  => '',
    //     'before_title'  => '<h2 class="widgettitle">',
    //     'after_title'   => '</h2>',
    // ) );

}
add_action( 'widgets_init', 'alcatel_widgets_init' );

/** end Registers a widget area. */



// New widget Alcatel THEME OPTIONS
function theme_option_load_wp_media_files() {
  wp_enqueue_media();
}
add_action( 'admin_enqueue_scripts', 'theme_option_load_wp_media_files' );

add_action('admin_menu', 'alcatel_theme_menu');

function alcatel_theme_menu()
{
    //add_theme_page( $page_title, $menu_title, $capability, $menu_slug, $function );
    add_theme_page( 'Theme Option', 'Theme Options', 'manage_options', 'alcatel_theme_options.php', 'alcatel_theme_page');  
}

function alcatel_theme_page()
{
?>
    <style>
  .custom_theme_option h2{
    margin-top: 30px;
      border-top: 1px solid #ccc;
      padding-top: 30px;
  }
  </style>
  <script type="text/javascript" src="<?php echo get_option('siteurl') ?>/wp-content/assets/js/custom.js"></script>
    <div class="section panel">
      <h1>Alcatel Theme Options</h1>
      <form method="post" enctype="multipart/form-data" action="options.php">
        <?php 
            //settings_fields( $option_group )
            settings_fields('alcatel_theme_options'); 
        
            //do_settings_sections( $page )
            do_settings_sections(__FILE__);
        ?>
            <p class="submit">  
                <input type="submit" class="button-primary" value="<?php _e('Save Changes') ?>" />  
            </p>  
            
      </form>
    </div>
    <?php
}


add_action( 'admin_init', 'alcatel_register_settings' );

function alcatel_register_settings()
{
    register_setting( 'alcatel_theme_options', 'alcatel_theme_options', 'alcatel_validate_settings' );
    //Home - Beranda disabled
   // add_settings_section( 'alcatel_home_service_section', 'HOME - BERANDA', 'alcatel_home_service_display_section', __FILE__ ); 
   add_settings_section( 'alcatel_send_msg_section', 'EMAIL', 'alcatel_send_msg_display_section', __FILE__ ); 
   add_settings_section('alcatel_about_us_section', 'TENTANG KAMI', 'alcatel_about_us_display_section', __FILE__);
    add_settings_section( 'alcatel_social_section', 'SOCIAL MEDIA', 'alcatel_social_display_section', __FILE__ );
}
function alcatel_send_msg_display_section($section){
   $args = array(
      'type'      => 'text',
      'id'        => 'alcatel_to_message1',
      'name'      => 'alcatel_to_message1',
      'desc'      => '',
      'std'       => '',
      'label_for' => 'alcatel_to_message1',
      'class'     => 'css_class'
    );
    add_settings_field( 'alcatel_to_message1', 'Email To', 'alcatel_send_msg_display_setting', __FILE__ , 'alcatel_send_msg_section', $args );
     $args1 = array(
      'type'      => 'text',
      'id'        => 'alcatel_to_message2',
      'name'      => 'alcatel_to_message2',
      'desc'      => '',
      'std'       => '',
      'label_for' => 'alcatel_to_message2',
      'class'     => 'css_class'
    );
    add_settings_field( 'alcatel_to_message2', 'Email Cc', 'alcatel_send_msg_display_setting', __FILE__ , 'alcatel_send_msg_section', $args1 );
     $args2 = array(
      'type'      => 'text',
      'id'        => 'alcatel_to_message3',
      'name'      => 'alcatel_to_message3',
      'desc'      => '',
      'std'       => '',
      'label_for' => 'alcatel_to_message3',
      'class'     => 'css_class'
    );
    add_settings_field( 'alcatel_to_message3', 'Email BCc', 'alcatel_send_msg_display_setting', __FILE__ , 'alcatel_send_msg_section', $args2 );
    // $args3 = array(
    //   'type'      => 'text',
    //   'id'        => 'alcatel_to_message4',
    //   'name'      => 'alcatel_to_message4',
    //   'desc'      => '',
    //   'std'       => '',
    //   'label_for' => 'alcatel_to_message4',
    //   'class'     => 'css_class'
    // );
    // add_settings_field( 'alcatel_to_message4', 'Message', 'alcatel_send_msg_display_setting', __FILE__ , 'alcatel_send_msg_section', $args3 );
}
function alcatel_send_msg_display_setting($args){
    extract( $args );

    $option_name = 'alcatel_theme_options';

    $options = get_option( $option_name );
    switch ( $type ) {
        case 'text':  
          $options[$id] = stripslashes($options[$id]);  
            // $options[$id] = esc_attr( $options[$id]);     
       if($id == "alcatel_to_message1")
        echo "
          <input class='regular-text$class' type='text' id='alcatel_to_message1' name='" . $option_name . "[alcatel_to_message1]' value='$options[alcatel_to_message1]' style='width:50%' /><br>";
        if($id == "alcatel_to_message2")
          echo "  <input class='regular-text$class' type='text' id='alcatel_to_message2' name='" . $option_name . "[alcatel_to_message2]' value='$options[alcatel_to_message2]' style='width:50%' /><br>";
        if($id == "alcatel_to_message3")
          echo " <input class='regular-text$class' type='text' id='alcatel_to_message3' name='" . $option_name . "[alcatel_to_message3]' value='$options[alcatel_to_message3]' style='width:50%' /><br>
            <textarea class='regular-text$class' type='text' id='alcatel_to_message_3' name='" . $option_name . "[alcatel_to_message_3]' style='width:50%;height:200px;' /> $options[alcatel_to_message_3]</textarea>
            ";
        // if($id == "alcatel_to_message4")
        //   echo "<textarea class='regular-text$class' type='text' id='alcatel_to_message4' name='" . $option_name . "[alcatel_to_message4]' style='width:50%;height:100px;' /> $options[alcatel_to_message4]</textarea>";
        break;
    }
}
function alcatel_about_us_display_section($section){
    $args = array(
      'type'      => 'button',
      'id'        => 'alcatel_aboutus_image4',
      'name'      => 'alcatel_aboutus_image4',
      'desc'      => '',
      'std'       => '',
      'data_index'=> '4',
      'label_for' => 'alcatel_aboutus_image4',
      'class'     => 'css_class'
    );
    add_settings_field( 'alcatel_aboutus_image4', 'Image Video Tentang Kami', 'alcatel_display_setting2', __FILE__ , 'alcatel_about_us_section', $args );

     $args1 = array(
      'type'      => 'text',
      'id'        => 'alcatel_aboutus_url1',
      'name'      => 'alcatel_aboutus_url1',
      'desc'      => '',
      'std'       => '',
      'label_for' => 'alcatel_aboutus_url1',
      'class'     => 'css_class'
    );
    add_settings_field( 'alcatel_aboutus_url1', 'Link Video Tentang Kami', 'alcatel_display_setting2', __FILE__ , 'alcatel_about_us_section', $args1 );

      $args2 = array(
      'type'      => 'text',
      'id'        => 'alcatel_aboutus_title1',
      'name'      => 'alcatel_aboutus_title1',
      'desc'      => '',
      'std'       => '',
      'label_for' => 'alcatel_aboutus_title1',
      'class'     => 'css_class'
    );
    add_settings_field( 'alcatel_aboutus_title1', 'Title 1 Tentang Kami', 'alcatel_display_setting2', __FILE__ , 'alcatel_about_us_section', $args2 );

     $args3 = array(
      'type'      => 'text',
      'id'        => 'alcatel_aboutus_title2',
      'name'      => 'alcatel_aboutus_title2',
      'desc'      => '',
      'std'       => '',
      'label_for' => 'alcatel_aboutus_title2',
      'class'     => 'css_class'
    );
    add_settings_field( 'alcatel_aboutus_title2', 'Title 2 Tentang Kami', 'alcatel_display_setting2', __FILE__ , 'alcatel_about_us_section', $args3 );
}

function alcatel_display_setting2($args)
{
    extract( $args );

    $option_name = 'alcatel_theme_options';

    $options = get_option( $option_name );
    switch ( $type ) {
        case 'text':  
          $options[$id] = stripslashes($options[$id]);  
            // $options[$id] = esc_attr( $options[$id]);
      if($id=="alcatel_aboutus_url1")         
      echo "<input class='regular-text$class' type='text' id='$id' name='" . $option_name . "[alcatel_aboutus_url1]' value='$options[$id]' style='width:50%'/>";  
      echo ($desc != '') ? "<br /><span class='description'>$desc</span>" : "";  
     
       if($id == "alcatel_aboutus_title1")
        echo "
          <input class='regular-text$class' type='text' id='alcatel_aboutus_title1' name='" . $option_name . "[alcatel_aboutus_title1]' value='$options[alcatel_aboutus_title1]' style='width:50%' /><br>
          <textarea class='regular-text$class' type='text' id='alcatel_aboutus_title_1' name='" . $option_name . "[alcatel_aboutus_title_1]' style='width:50%;height:100px;' /> $options[alcatel_aboutus_title_1]</textarea>";
      if($id == "alcatel_aboutus_title2")
        echo "
          <input class='regular-text$class' type='text' id='alcatel_aboutus_title2' name='" . $option_name . "[alcatel_aboutus_title2]' value='$options[alcatel_aboutus_title2]' style='width:50%' /><br>
          <textarea class='regular-text$class' type='text' id='alcatel_aboutus_title_2' name='" . $option_name . "[alcatel_aboutus_title_2]' style='width:50%;height:100px;' />$options[alcatel_aboutus_title_2]</textarea>";

        break;
        case 'file':  
          $options[$id] = stripslashes($options[$id]);  
          $options[$id] = esc_attr( $options[$id]);  
          if($options[$id])
            echo "<img src='$options[$id]' style='width : 250px' /><br/>"; 
            echo "<input class='regular-text$class' type='file' id='$id' name='$id' />"; 
        break;

        case 'button':  
          $options[$id] = stripslashes($options[$id]);  
          $options[$id] = esc_attr( $options[$id]);  
          // echo $id;
          if($options[$id])
            echo "<img src='$options[$id]' style='width : 250px' id='image_$data_index' /><br/>"; 
          echo "<input class='regular-text$class' type='hidden' id='alcatel_aboutus_image$data_index' name='" . $option_name . "[alcatel_aboutus_image$data_index]' onclick='media_uploder_aboutus(" .$data_index. ")' value='$options[$id]'  /alcatel_aboutus_image>"; 
          echo "<input class='regular-text$class' type='button' id='$id' name='$id' onclick='media_uploder_aboutus(" .$data_index. ")' value='Choose File' />"; 
          
        break;
    }
}

function alcatel_validate_settings2($plugin_options) {
    
    foreach($plugin_options as $k => $v)
    {
        if("alcatel_aboutus_image4" == $k || "alcatel_aboutus_url1" == $k || "alcatel_aboutus_title1"==$k || "alcatel_aboutus_title2"== $k)
        {
            $plugin_options3[$k] = esc_url_raw($v);
        }
    }

    return $plugin_options3;
}


function alcatel_home_service_display_section($section){
  $field_args = array(
      'type'      => 'button',
      'id'        => 'alcatel_homeservice_image1',
      'name'      => 'alcatel_homeservice_image1',
      'desc'      => '',
      'std'       => '',
      'data_index'=> '1',
      'label_for' => 'alcatel_homeservice_image1',
      'class'     => 'css_class'
    );
    add_settings_field( 'homeservice_image1', 'Home Image 1', 'alcatel_display_setting1', __FILE__ , 'alcatel_home_service_section', $field_args );

    $field_args2 = array(
      'type'      => 'text',
      'id'        => 'alcatel_homeservice_title1',
      'name'      => 'alcatel_homeservice_title1',
      'desc'      => '',
      'std'       => '',
      'label_for' => 'alcatel_homeservice_title1',
      'class'     => 'css_class'
    );
     add_settings_field( 'homeservice_title1', 'Home Title 1', 'alcatel_display_setting1', __FILE__ , 'alcatel_home_service_section', $field_args2 );
      $field_args3 = array(
      'type'      => 'text',
      'id'        => 'alcatel_homeservice_url1',
      'name'      => 'alcatel_homeservice_url1',
      'desc'      => '',
      'std'       => '',
      'label_for' => 'alcatel_homeservice_url1',
      'class'     => 'css_class'
    );
    add_settings_field( 'homeservice_url1', 'Home Url 1', 'alcatel_display_setting1', __FILE__ , 'alcatel_home_service_section', $field_args3 );
    //2
    $field_args4 = array(
      'type'      => 'button',
      'id'        => 'alcatel_homeservice_image2',
      'name'      => 'alcatel_homeservice_image2',
      'desc'      => '',
      'std'       => '',
      'data_index'=> '2',
      'label_for' => 'alcatel_homeservice_image2',
      'class'     => 'css_class'
    );
    add_settings_field( 'homeservice_image2', 'Home Image 2', 'alcatel_display_setting1', __FILE__ , 'alcatel_home_service_section', $field_args4 );

    $field_args5 = array(
      'type'      => 'text',
      'id'        => 'alcatel_homeservice_title2',
      'name'      => 'alcatel_homeservice_title2',
      'desc'      => '',
      'std'       => '',
      'label_for' => 'alcatel_homeservice_title2',
      'class'     => 'css_class'
    );
     add_settings_field( 'homeservice_title2', 'Home Title 2', 'alcatel_display_setting1', __FILE__ , 'alcatel_home_service_section', $field_args5 );
      $field_args6 = array(
      'type'      => 'text',
      'id'        => 'alcatel_homeservice_url2',
      'name'      => 'alcatel_homeservice_url2',
      'desc'      => '',
      'std'       => '',
      'label_for' => 'alcatel_homeservice_url2',
      'class'     => 'css_class'
    );
    add_settings_field( 'homeservice_url2', 'Home Url 2', 'alcatel_display_setting1', __FILE__ , 'alcatel_home_service_section', $field_args6 );

    //3
    $field_args7 = array(
      'type'      => 'button',
      'id'        => 'alcatel_homeservice_image3',
      'name'      => 'alcatel_homeservice_image3',
      'desc'      => '',
      'std'       => '',
      'data_index'=> '3',
      'label_for' => 'alcatel_homeservice_image3',
      'class'     => 'css_class'
    );
    add_settings_field( 'homeservice_image3', 'Home Image 3', 'alcatel_display_setting1', __FILE__ , 'alcatel_home_service_section', $field_args7 );


    $field_args8 = array(
      'type'      => 'text',
      'id'        => 'alcatel_homeservice_title3',
      'name'      => 'alcatel_homeservice_title3',
      'desc'      => '',
      'std'       => '',
      'label_for' => 'alcatel_homeservice_title3',
      'class'     => 'css_class'
    );
     add_settings_field( 'homeservice_title3', 'Home Title 3', 'alcatel_display_setting1', __FILE__ , 'alcatel_home_service_section', $field_args8 );
      $field_args9 = array(
      'type'      => 'text',
      'id'        => 'alcatel_homeservice_url3',
      'name'      => 'alcatel_homeservice_url3',
      'desc'      => '',
      'std'       => '',
      'label_for' => 'alcatel_homeservice_url3',
      'class'     => 'css_class'
    );

    add_settings_field( 'homeservice_url3', 'Home Url 3', 'alcatel_display_setting1', __FILE__ , 'alcatel_home_service_section', $field_args9 );

    //   $field_args10 = array(
    //   'type'      => 'text',
    //   'id'        => 'alcatel_homeservice_textarea1',
    //   'name'      => 'alcatel_homeservice_textarea1',
    //   'desc'      => '',
    //   'std'       => '',
    //   'label_for' => 'alcatel_homeservice_textarea1',
    //   'class'     => 'css_class'
    // );

    // add_settings_field( 'homeservice_textarea1', 'Home Text Area', 'alcatel_display_setting1', __FILE__ , 'alcatel_home_service_section', $field_args10 );    

}

function alcatel_display_setting1($args)
{
    extract( $args );

    $option_name = 'alcatel_theme_options';

    $options = get_option( $option_name );

    switch ( $type ) {
    case 'text':  
      $options[$id] = stripslashes($options[$id]);  
      $options[$id] = esc_attr( $options[$id]);  

      // echo "<input class='regular-text$class' type='text' id='$id' name='" . $option_name . "[$id]' value='$options[$id]' style='width:50%'/>";  
      echo ($desc != '') ? "<br /><span class='description'>$desc</span>" : "";  
      if($id == "alcatel_homeservice_title1")
        echo "
          <input class='regular-text$class' type='text' id='alcatel_homeservice_title1' name='" . $option_name . "[alcatel_homeservice_title1]' value='$options[alcatel_homeservice_title1]' style='width:50%' /><br>
          <input class='regular-text$class' type='text' id='alcatel_homeservice_title_1' name='" . $option_name . "[alcatel_homeservice_title_1]' value='$options[alcatel_homeservice_title_1]' style='width:50%' /><br>
          <input class='regular-text$class' type='text' id='alcatel_homeservice_title_2' name='" . $option_name . "[alcatel_homeservice_title_2]' value='$options[alcatel_homeservice_title_2]' style='width:50%' /><br>
          <textarea class='regular-text$class' type='text' id='alcatel_homeservice_title_3' name='" . $option_name . "[alcatel_homeservice_title_3]' style='width:50%;height:100px;' /> $options[alcatel_homeservice_title_3]</textarea>";
      if($id == "alcatel_homeservice_title2")
        // echo $options[alcatel_homeservice_title_6];die();
        echo "
          <input class='regular-text$class' type='text' id='alcatel_homeservice_title2' name='" . $option_name . "[alcatel_homeservice_title2]' value='$options[alcatel_homeservice_title2]' style='width:50%' /><br>
          <input class='regular-text$class' type='text' id='alcatel_homeservice_title_1' name='" . $option_name . "[alcatel_homeservice_title_4]' value='$options[alcatel_homeservice_title_4]' style='width:50%' /><br>
          <input class='regular-text$class' type='text' id='alcatel_homeservice_title_5' name='" . $option_name . "[alcatel_homeservice_title_5]' value='$options[alcatel_homeservice_title_5]' style='width:50%' /><br>
          <textarea class='regular-text$class' type='text' id='alcatel_homeservice_title_6' name='" . $option_name . "[alcatel_homeservice_title_6]' style='width:50%;height:100px;' />$options[alcatel_homeservice_title_6]</textarea>";
       if($id == "alcatel_homeservice_title3")
        echo "
          <input class='regular-text$class' type='text' id='alcatel_homeservice_title3' name='" . $option_name . "[alcatel_homeservice_title3]' value='$options[alcatel_homeservice_title3]' style='width:50%' /><br>
          <input class='regular-text$class' type='text' id='alcatel_homeservice_title_7' name='" . $option_name . "[alcatel_homeservice_title_7]' value='$options[alcatel_homeservice_title_7]' style='width:50%' /><br>
          <input class='regular-text$class' type='text' id='alcatel_homeservice_title_8' name='" . $option_name . "[alcatel_homeservice_title_8]' value='$options[alcatel_homeservice_title_8]' style='width:50%' /><br>
          <textarea class='regular-text$class' type='text' id='alcatel_homeservice_title_9' name='" . $option_name . "[alcatel_homeservice_title_9]' style='width:50%;height:100px;' />$options[alcatel_homeservice_title_9]</textarea>"; 
         if($id == 'alcatel_homeservice_url1'){
          echo "<input class='regular-text$class' type='text' id='alcatel_homeservice_url1' name='" . $option_name . "[alcatel_homeservice_url1]' value='$options[alcatel_homeservice_url1]' style='width:50%' />";
        }
        if($id == 'alcatel_homeservice_url2'){
          echo "<input class='regular-text$class' type='text' id='alcatel_homeservice_url2' name='" . $option_name . "[alcatel_homeservice_url2]' value='$options[alcatel_homeservice_url2]' style='width:50%' />";
        }
         if($id == 'alcatel_homeservice_url3'){
          // echo $options[alcatel_homeservice_url3];
          echo "<input class='regular-text$class' type='text' id='alcatel_homeservice_url3' name='" . $option_name . "[alcatel_homeservice_url3]' value='$options[alcatel_homeservice_url3]' style='width:50%' />";
        }

          // echo $options[$id];die();
    break;

    case 'file':  
      $options[$id] = stripslashes($options[$id]);  
      $options[$id] = esc_attr( $options[$id]);  
      if($options[$id])
        echo "<img src='$options[$id]' style='width : 250px' /><br/>"; 
        echo "<input class='regular-text$class' type='file' id='$id' name='$id' />"; 
    break;

    case 'button':  
      $options[$id] = stripslashes($options[$id]);  
      $options[$id] = esc_attr( $options[$id]);  
      if($options[$id])
        echo "<img src='$options[$id]' style='width : 250px' id='image_$data_index' /><br/>"; 
      echo "<input class='regular-text$class' type='hidden' id='alcatel_homeservice_image$data_index' name='" . $option_name . "[alcatel_homeservice_image$data_index]' onclick='media_uploder(" .$data_index. ")' value='$options[$id]'  /alcatel_home_image>"; 
      echo "<input class='regular-text$class' type='button' id='$id' name='$id' onclick='media_uploder(" .$data_index. ")' value='Choose File' />"; 
      
    break;
    }
}

function alcatel_validate_settings1($plugin_options) {

  
  foreach($plugin_options as $k => $v)
  {
    if("alcatel_homeservice_url1" == $k || "alcatel_homeservice_url2" == $k || "alcatel_homeservice_url3" == $k || "alcatel_homeservice_image1" == $k || "alcatel_homeservice_image2" == $k ||  "alcatel_homeservice_image3" == $k )
    {
      $plugin_options2[$k] = esc_url_raw($v);
    }
    else if("alcatel_homeservice_title1" == $k || "alcatel_homeservice_title2" == $k || "alcatel_homeservice_title3" == $k)
    {
      $plugin_options2[$k] = esc_html($v);
    }
  }

  return $plugin_options2;
}



function alcatel_social_display_section($section){

    // Create textbox field
    $field_args = array(
      'type'      => 'text',
      'id'        => 'alcatel_facebook',
      'name'      => 'alcatel_facebook',
      'desc'      => '',
      'std'       => '',
      'label_for' => 'alcatel_facebook',
      'class'     => 'css_class'
    );

    //add_settings_field( $id, $title, $callback, $page, $section, $args )
    add_settings_field( 'facebook_url', 'Url Facebook', 'alcatel_display_setting', __FILE__ , 'alcatel_social_section', $field_args );

    $field_args2 = array(
      'type'      => 'text',
      'id'        => 'alcatel_twitter',
      'name'      => 'alcatel_twitter',
      'desc'      => '',
      'std'       => '',
      'label_for' => 'alcatel_twitter',
      'class'     => 'css_class'
    );
    add_settings_field( 'twitter_url', 'Url Twitter', 'alcatel_display_setting', __FILE__ , 'alcatel_social_section', $field_args2 );

    // $field_args4 = array(
    //   'type'      => 'text',
    //   'id'        => 'alcatel_instagram',
    //   'name'      => 'alcatel_instagram',
    //   'desc'      => '',
    //   'std'       => '',
    //   'label_for' => 'alcatel_instagram',
    //   'class'     => 'css_class'
    // );
    // add_settings_field( 'instagram_url', 'Url Instagram', 'alcatel_display_setting', __FILE__ , 'alcatel_social_section', $field_args4 );

    // $field_args5 = array(
    //   'type'      => 'text',
    //   'id'        => 'alcatel_youtube_sosmed',
    //   'name'      => 'alcatel_youtube_sosmed',
    //   'desc'      => '',
    //   'std'       => '',
    //   'label_for' => 'alcatel_youtube_sosmed',
    //   'class'     => 'css_class'
    // );
    // add_settings_field( 'youtube_sosmed_url', 'Url Youtube', 'alcatel_display_setting', __FILE__ , 'alcatel_social_section', $field_args5 );

    $field_args3 = array(
      'type'      => 'text',
      'id'        => 'alcatel_youtube',
      'name'      => 'alcatel_youtube',
      'desc'      => '',
      'std'       => '',
      'label_for' => 'alcatel_youtube',
      'class'     => 'css_class'
    );
    add_settings_field( 'youtube_url', 'Url Youtube', 'alcatel_display_setting', __FILE__ , 'alcatel_social_section', $field_args3 );
}


function alcatel_display_setting($args)
{
    extract( $args );

    $option_name = 'alcatel_theme_options';

    $options = get_option( $option_name );
    switch ( $type ) {
        case 'text':  
            // $options[$id] = stripslashes($options[$id]);  
            // $options[$id] = esc_attr( $options[$id]);
            $options[$id] = esc_url( $options[$id]);  
            echo "<input class='regular-text$class' type='text' id='$id' name='" . $option_name . "[$id]' value='$options[$id]' placeholder='http://www.linkurl.com' style='width:50%' />";
        break;
    }
}

function alcatel_validate_settings($plugin_options) {
    
    foreach($plugin_options as $k => $v)
    {
        if("alcatel_facebook" == $k || "alcatel_twitter" == $k || "alcatel_youtube" == $k || "alcatel_instagram" == $k || "alcatel_youtube_sosmed" == $k )
        {
            $plugin_options[$k] = esc_url_raw($v);
        }
    }

    return $plugin_options;
}


class Event_Widget extends WP_Widget {

    /**
     * Register widget with WordPress.
     */
    function __construct() {
        parent::__construct(
            'event_widget', // Base ID
            __( 'Event Widget', 'text_domain' ), // Name
            array( 'description' => __( 'An Event Widget', 'text_domain' ), ) // Args
        );
    }

    /**
     * Front-end display of widget.
     *
     * @see WP_Widget::widget()
     *
     * @param array $args     Widget arguments.
     * @param array $instance Saved values from database.
     */
    public function widget( $args, $instance ) {
    ?>
        <h3>OTHER EVENTS</h3>
        <ul class="sidebar-list">

    <?php
    $number_display = intval( $instance['number_display'] );
    $args1 = array(
        'posts_per_page'   => $number_display,
        'category_name'    => 'Event',
        'orderby'          => 'date',
        'order'            => 'DESC',
        'post_type'        => 'post',
        'post_status'      => 'publish'
    );

    $postslist = get_posts($args1);
    foreach ($postslist as $event_) :
        setup_postdata($event_);
        $title = get_the_title( $event_->ID );
        $permalink = get_permalink($event_->ID);
        $excerpt = substr( strip_tags( get_the_content() ), 0, 100);
        $excerpt = $excerpt.'.';                                    
        $post_thumb = wp_get_attachment_image_src( get_post_thumbnail_id( $event_->ID ) , 'medium_large' );
    ?>
        <li class="sidebar-mini">
            <div class="news-side-img">
                <a href="<?= $permalink ?>"><img src="<?= $post_thumb[0] ?>"></a>
            </div>
            <div class="news-side-tittle">
                <a href="<?= $permalink ?>"><h3><?= $title ?></h3></a>
            </div>
            <div class="news-side-paraf">
                <p><?= $excerpt ?> <a href="<?= $permalink ?>">read more...</a></p>
            </div>
            <div class="news-side-tittle">
                <p><?= get_the_date( "d F Y" ) ?></p>
            </div>
            <div class="clearfix"></div>
        </li>
    <?php
    endforeach; 
    ?>
        
        </ul>

    <?php
    }

    /**
     * Back-end widget form.
     *
     * @see WP_Widget::form()
     *
     * @param array $instance Previously saved values from database.
     */
    public function form( $instance ) {
        $number_display = ! empty( $instance['number_display'] ) ? $instance['number_display'] : 5 ;
        $number_display = intval( $number_display );
        ?>
        <p>
        <label for="<?php echo esc_attr( $this->get_field_id( 'number_display' ) ); ?>"><?php _e( esc_attr( 'Number event display:' ) ); ?></label> 
        <input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'number_display' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'number_display' ) ); ?>" type="text" value="<?php echo esc_attr( $number_display ); ?>">
        </p>
        <?php 
    }

    /**
     * Sanitize widget form values as they are saved.
     *
     * @see WP_Widget::update()
     *
     * @param array $new_instance Values just sent to be saved.
     * @param array $old_instance Previously saved values from database.
     *
     * @return array Updated safe values to be saved.
     */
    public function update( $new_instance, $old_instance ) {
        $instance = array();
        $instance['number_display'] = ( ! empty( $new_instance['number_display'] ) ) ? strip_tags( $new_instance['number_display'] ) : '';
        $instance['number_display'] = intval( $instance['number_display'] );

        return $instance;
    }

} // class Foo_Widget

function register_event_widget() {
    register_widget( 'Event_Widget' );
}
add_action( 'widgets_init', 'register_event_widget' );



function register_tips_widget() {
    register_widget( 'Tips_Widget' );
}
add_action( 'widgets_init', 'register_tips_widget' );

class Tips_Widget extends WP_Widget {

    /**
     * Register widget with WordPress.
     */
    function __construct() {
        parent::__construct(
            'tips_widget', // Base ID
            __( 'Tips Widget', 'text_domain' ), // Name
            array( 'description' => __( 'An Tips Widget', 'text_domain' ), ) // Args
        );
    }

    /**
     * Front-end display of widget.
     *
     * @see WP_Widget::widget()
     *
     * @param array $args     Widget arguments.
     * @param array $instance Saved values from database.
     */
    public function widget( $args, $instance ) {
    ?>
        <h3>OTHER TIPS</h3>
        <ul class="sidebar-list">

    <?php
    $number_display = intval( $instance['number_display'] );
    $args1 = array(
        'posts_per_page'   => $number_display,
        'category_name'    => 'Tips',
        'orderby'          => 'date',
        'order'            => 'DESC',
        'post_type'        => 'post',
        'post_status'      => 'publish'
    );

    $postslist = get_posts($args1);
    foreach ($postslist as $tips_) :
        setup_postdata($tips_);
        $title = get_the_title( $tips_->ID );
        $permalink = get_permalink($tips_->ID);
        $excerpt = substr( strip_tags( get_the_content() ), 0, 100);
        $excerpt = $excerpt.'.';                                    
        $post_thumb = wp_get_attachment_image_src( get_post_thumbnail_id( $tips_->ID ) , 'medium_large' );
    ?>
        <li class="sidebar-mini">
            <?php
            if($post_thumb)
            {
            ?>
            <div class="news-side-img">
                <a href="<?= $permalink ?>"><img src="<?= $post_thumb[0] ?>"></a>
            </div>
            <?php
            }
            ?>
            <div class="news-side-tittle">
                <a href="<?= $permalink ?>"><h3><?= $title ?></h3></a>
            </div>
            <div class="news-side-paraf">
                <p><?= $excerpt ?> <a href="<?= $permalink ?>">read more...</a></p>
            </div>
            <div class="news-side-tittle">
                <p><?= get_the_date( "d F Y" ) ?></p>
            </div>
            <div class="clearfix"></div>
        </li>
    <?php
    endforeach; 
    ?>
        
        </ul>

    <?php
    }

    /**
     * Back-end widget form.
     *
     * @see WP_Widget::form()
     *
     * @param array $instance Previously saved values from database.
     */
    public function form( $instance ) {
        $number_display = ! empty( $instance['number_display'] ) ? $instance['number_display'] : 5 ;
        $number_display = intval( $number_display );
        ?>
        <p>
        <label for="<?php echo esc_attr( $this->get_field_id( 'number_display' ) ); ?>"><?php _e( esc_attr( 'Number tips display:' ) ); ?></label> 
        <input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'number_display' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'number_display' ) ); ?>" type="text" value="<?php echo esc_attr( $number_display ); ?>">
        </p>
        <?php 
    }

    /**
     * Sanitize widget form values as they are saved.
     *
     * @see WP_Widget::update()
     *
     * @param array $new_instance Values just sent to be saved.
     * @param array $old_instance Previously saved values from database.
     *
     * @return array Updated safe values to be saved.
     */
    public function update( $new_instance, $old_instance ) {
        $instance = array();
        $instance['number_display'] = ( ! empty( $new_instance['number_display'] ) ) ? strip_tags( $new_instance['number_display'] ) : '';
        $instance['number_display'] = intval( $instance['number_display'] );

        return $instance;
    }

} // class Foo_Widget


// New widget Alcatel Theme Options

/** NAVIGATION MENU  */
// CUSTOM NAVIGATION
class Custom_Nav_Walker extends Walker_Nav_Menu {
 
  /**
     * Starts the list before the elements are added.
     *
     * Adds classes to the unordered list sub-menus.
     *
     * @param string $output Passed by reference. Used to append additional content.
     * @param int    $depth  Depth of menu item. Used for padding.
     * @param array  $args   An array of arguments. @see wp_nav_menu()
     */
    function start_lvl( &$output, $depth = 0, $args = array() ) {
        // Depth-dependent classes.
        $indent = ( $depth > 0  ? str_repeat( "\t", $depth ) : '' ); // code indent
        $display_depth = ( $depth + 1); // because it counts the first submenu as 0
        $classes = array(
            'sub-menu',
            ( $display_depth % 2  ? 'menu-odd' : 'menu-even' ),
            ( $display_depth >=2 ? 'sub-sub-menu' : '' ),
            'menu-depth-' . $display_depth
        );
        $class_names = implode( ' ', $classes );
 
        // Build HTML for output.
        $output .= "\n" . $indent . '<ul class="' . $class_names . '">' . "\n";
    }
 
    /**
     * Start the element output.
     *
     * Adds main/sub-classes to the list items and links.
     *
     * @param string $output Passed by reference. Used to append additional content.
     * @param object $item   Menu item data object.
     * @param int    $depth  Depth of menu item. Used for padding.
     * @param array  $args   An array of arguments. @see wp_nav_menu()
     * @param int    $id     Current item ID.
     */
    function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {
        global $wp_query;
        $indent = ( $depth > 0 ? str_repeat( "\t", $depth ) : '' ); // code indent
 
        // Depth-dependent classes.
        $depth_classes = array(
            ( $depth == 0 ? 'main-menu-item' : 'sub-menu-item' ),
            ( $depth >=2 ? 'sub-sub-menu-item' : '' ),
            ( $depth % 2 ? 'menu-item-odd' : 'menu-item-even' ),
            'menu-item-depth-' . $depth
        );
        $depth_class_names = esc_attr( implode( ' ', $depth_classes ) );
 
        // Passed classes.
        $classes = empty( $item->classes ) ? array() : (array) $item->classes;
        $class_names = esc_attr( implode( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item ) ) );
 
        // Build HTML.
        $output .= $indent . '<li id="nav-menu-item-'. $item->ID . '" class="' . $depth_class_names . ' ' . $class_names . '">';
 
        // Link attributes.
        $attributes  = ! empty( $item->attr_title ) ? ' title="'  . esc_attr( $item->attr_title ) .'"' : '';
        $attributes .= ! empty( $item->target )     ? ' target="' . esc_attr( $item->target     ) .'"' : '';
        $attributes .= ! empty( $item->xfn )        ? ' rel="'    . esc_attr( $item->xfn        ) .'"' : '';
        $attributes .= ! empty( $item->url )        ? ' href="'   . esc_attr( $item->url        ) .'"' : '';
        $attributes .= ' class="menu-link ' . ( $depth > 0 ? 'sub-menu-link' : 'main-menu-link' ) . '"';
 
        // Build HTML output and pass through the proper filter.
        $item_output = sprintf( '%1$s<a%2$s>%3$s%4$s%5$s</a>%6$s',
            $args->before,
            $attributes,
            $args->link_before,
            apply_filters( 'the_title', $item->title, $item->ID ),
            $args->link_after,
            $args->after
        );
        $output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
    }
}

/**
 * Add descriptions to menu items
 */


/** END MENU */

/**
 * CUSTOMIZER ADD BY R56
 */
/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/** 
 * Widgets
 */
// require get_template_directory().'/inc/widgets.php';

/**
 * Twenty Sixteen only works in WordPress 4.4 or later.
 */
if ( version_compare( $GLOBALS['wp_version'], '4.4-alpha', '<' ) ) {
  require get_template_directory() . '/inc/back-compat.php';
}

/** FORM HUBUNGI KAMI /--Add By R56 */
include_once('template-parts/kontak-form.php');
/** END HUBUNGI KAMI*/

include_once('template-parts/promo-form.php');


// METABOX UNTUK SUB TITLE
/** ADD META BOX */
/**add_action( 'add_meta_boxes', 'subtitle_meta_box_add' );
function subtitle_meta_box_add()
{
    add_meta_box( 'sub-title-meta-box-id', 'Sub Title Meta Box', 'subtitle_meta_box_cb', 'post', 'normal', 'high' );
}
/** DISPLAY META BOX */
/**function subtitle_meta_box_cb()
{
  // $post is already set, and contains an object: the WordPress post
    global $post;
    $values_name = get_post_meta($post->ID, "subtitle_meta_box_name", true);
       
    if(!empty( $values_name ))
    {
      $text_1 = esc_html($values_name);
    }
    
    // We'll use this nonce field later on when saving.
    wp_nonce_field( 'outlet_meta_box_nonce', 'meta_box_nonce' );
    ?>

    <script type="text/javascript" src="<?php echo get_option('siteurl') ?>/wp-content/assets/js/custom.js"></script>

    <p>
        <label for="subtitle_meta_box_name">Sub Title : </label>
        <input type="text" name="subtitle_meta_box_name" id="subtitle_meta_box_name" value="<?php echo $text_1; ?>" style="width : 90%" />
    </p>
    

    <?php    
}*/



/** SAVE META BOX */
/*add_action( 'save_post', 'outlet_meta_box_save' );
function outlet_meta_box_save( $post_id )
{
  global $post;

    // Bail if we're doing an auto save
    if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) return;
     
    // if our nonce isn't there, or we can't verify it, bail
    if( !isset( $_POST['meta_box_nonce'] ) || !wp_verify_nonce( $_POST['meta_box_nonce'], 'outlet_meta_box_nonce' ) ) return;
     
    // if our current user can't edit this post, bail
    if( !current_user_can( 'edit_post' ) ) return;

    $slug = "post";
    if($slug != $post->post_type) return;
     
    // Make sure your data is set before trying to save it
    if( isset( $_POST['subtitle_meta_box_name'] ) )
        update_post_meta( $post_id, 'subtitle_meta_box_name', esc_html( $_POST['subtitle_meta_box_name'] ) );

}*/

/** META BOX UNTUK PAGE */
add_action( 'add_meta_boxes', 'description_meta_box_add' );
function description_meta_box_add()
{
    //add_meta_box( 'description-meta-box-id', 'Description Meta Box', 'description_meta_box_cb', 'page', 'normal', 'high' );
}

add_action( 'save_post', 'description_meta_box_save' );
function description_meta_box_save( $post_id )
{
  global $post;

    // Bail if we're doing an auto save
    if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) return;
     
    // if our nonce isn't there, or we can't verify it, bail
    if( !isset( $_POST['meta_box_nonce'] ) || !wp_verify_nonce( $_POST['meta_box_nonce'], 'description_meta_box_nonce' ) ) return;
     
    // if our current user can't edit this post, bail
    if( !current_user_can( 'edit_post' ) ) return;

    $slug = "page";
    if($slug != $post->post_type) return;

    // echo $_POST['page_meta_subtitle'];die();
    if(isset($_POST['page_meta_subtitle']))
        update_post_meta( $post_id, 'page_meta_subtitle', esc_html( $_POST['page_meta_subtitle']) );
     
    // if (!empty($_POST['description_metaname_value']))
    //     {
    //       $datta=htmlspecialchars($_POST['description_metaname_value']);
    //       update_post_meta($post_id, 'description_metaname_value', $datta );
    //     }
    if ( isset ( $_POST['description_metaname_value'] ) ) {
    update_post_meta( $post_id, 'description_metaname_value', $_POST['description_metaname_value'] );
  }

}
/** DISPLAY META BOX */
function description_meta_box_cb()
{
   wp_nonce_field(basename(__FILE__), "meta-box-nonce");
  // $post is already set, and contains an object: the WordPress post
    global $post;
    $values_subTitle = get_post_meta($post->ID, "page_meta_subtitle", true);
    $values_desc = get_post_meta($post->ID, "description_metaname_value", true);
       
    $desc = !empty( $values_desc ) ? esc_html($values_desc) : '';
    $subTitle = !empty($values_subTitle) ? esc_html($values_subTitle) : '';

    // We'll use this nonce field later on when saving.
    wp_nonce_field( 'description_meta_box_nonce', 'meta_box_nonce' );
    ?>

    <script type="text/javascript" src="<?php echo get_option('siteurl') ?>/wp-content/assets/js/custom.js"></script>

    <p>
        <label for="page_meta_subtitle">Sub Title : </label>
        <input type="text" name="page_meta_subtitle" id="page_meta_subtitle" value="<?php echo $subTitle; ?>" style="width : 90%" />
    </p>
    <?php
    //  $values_desc=  get_post_meta($_GET['post'], 'description_metaname_value' , true ) ;
    // wp_editor( htmlspecialchars_decode($values_desc), 'description_metaname_value', $settings = array('textarea_name'=>'description_metaname_value') );

     // Use nonce for verification
  // wp_nonce_field( plugin_basename( __FILE__ ), 'myplugin_noncename' );

 $values_desc = get_post_meta( $post->ID, 'description_metaname_value', false );
  wp_editor( $values_desc[0], 'description_metaname_value' );
      
}

//PAGINATION 
function alcatel_pagination() {
    global $postslist;
    $big = 999999999;
    echo paginate_links( array(
       'base'     => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
       'format'   => '?paged=%#%',
       'prev_next'      => true,
       'prev_text'      => '<< Sebelumnya',
       'next_text'      => 'Selanjutnya >>',
       'current'  => max( 1, get_query_var('paged') ),
       'total'  => $postslist->max_num_pages )
    );
}

//CHECK LIST NEW TAB PAGE
/**
 * Adds a box to the main column on the Post add/edit screens.
 */
function new_tab_add_meta_box() {

        add_meta_box(
                'new_tab_sectionid', 'Add Link Meta Box', 'new_tab_meta_box_callback', 'page'
        ); //you can change the 4th paramter i.e. post to custom post type name, if you want it for something else

}

add_action( 'add_meta_boxes', 'new_tab_add_meta_box' );

/**
 * Prints the box content.
 * 
 * @param WP_Post $post The object for the current post/page.
 */
function new_tab_meta_box_callback( $post ) {

        // Add an nonce field so we can check for it later.
        wp_nonce_field( 'new_tab_meta_box', 'new_tab_meta_box_nonce' );

        /*
         * Use get_post_meta() to retrieve an existing value
         * from the database and use the value for the form.
         */
       $values_text = get_post_meta($post->ID, "blog_new_meta_url", true);
        $text = !empty( $values_text ) ? esc_url($values_text) : '';
      $use_new_tab = get_post_meta($post->ID, "blog_meta_use_new_tab",true);
      // print_r($user_new_tab);die();

        ?>
       <p>
        <input type="checkbox" id="blog_meta_use_new_tab" name="blog_meta_use_new_tab" value="1" <?= ($use_new_tab == 0)? "" : "checked" ; ?> > <label for="blog_meta_use_new_tab">Use New Tab? </label>
    </p> 
    <p>
        <label for="blog_new_meta_url">Slider Url</label>
        <input type="text" name="blog_new_meta_url" id="blog_new_meta_url" value="<?php echo $text; ?>" style="width: 100%;" />
    </p>

        <?php

}

/**
 * When the post is saved, saves our custom data.
 *
 * @param int $post_id The ID of the post being saved.
 */
function wdm_save_meta_box_data( $post_id ) {

        /*
         * We need to verify this came from our screen and with proper authorization,
         * because the save_post action can be triggered at other times.
         */

        // If this is an autosave, our form has not been submitted, so we don't want to do anything.
        if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
                return;
        }

        // Check the user's permissions.
        if ( !current_user_can( 'edit_post', $post_id ) ) {
                return;
        }


      if( isset( $_POST['blog_meta_use_new_tab'] ) ) 
        $use_new_tab = 1;
      else
        $use_new_tab = 0;

    update_post_meta( $post_id, 'blog_meta_use_new_tab', $use_new_tab );
      //Make sure your data is set before trying to save it
    if( isset( $_POST['blog_new_meta_url'] ) )
        update_post_meta( $post_id, 'blog_new_meta_url', esc_url_raw( $_POST['blog_new_meta_url'] ) );

}

add_action( 'save_post', 'wdm_save_meta_box_data' );


// GLOBAL HELPER
function dg_get_date_diff($date1, $date2){
        $interval = date_diff(date_create($date1), date_create($date2));

        $str = "";
        if($interval->y > 0){
            $str = $interval->y ." tahun";
        }else if($interval->m > 0){
            $str = $interval->m ." bulan";
        }else if($interval->d > 0){
            $str = $interval->d ." hari";
        }else{
            $str = $interval->h ." jam";
        }

        return $str;
    }

// REDIRECT
function redirect($url){
    $string = '<script type="text/javascript">';
    $string .= 'window.location = "' . $url . '"';
    $string .= '</script>';
    echo $string;
}


//WP-OPTIONS
// require_once( 'wptuts-options/more-options.php' );

include_once('functions_ezi.php');
