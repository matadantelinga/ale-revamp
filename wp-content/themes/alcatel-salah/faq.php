<?php 
/**
 * Template Name: Faq Template
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

get_header('nav');  ?>
    <!-- PRODUCT -->

<?php
//wp_mail('ezi@dgtraffic.com', 'Notification', 'Test notification email');
?>
<section>
    <article>
        <div class="wrap-faq">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h3 class="title-faq">frequently asked question</h3>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                <?php 
                        $args = array(
                                'category_name' => 'Faq'
                                ,'orderby' => 'date'
                                ,'order'   => 'ASC'
                                ,'post_type' => 'post'
                                ,'post_status' => 'publish'
                                ,'posts_per_page' => -1
                            );
                        $post_list = new WP_Query($args);
                        if($post_list->have_posts()): 
                            $i=1;
                            // echo "<pre>";
                            // print_r($post_list);die();
                            while ($post_list->have_posts()) : $post_list->the_post(); 
                                $permalink = get_permalink($post->ID);


                        ?>
                        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                            <div class="panel panel-default">
                                <h4 class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse<?= $i; ?>" aria-expanded="true" aria-controls="collapse<?= $i; ?>">
                                        <i class="fa fa-plus | pull-right"></i><?= $i .".&nbsp;"?>
                                        <?php the_title(); ?>
                                    </a>
                                </h4>
                            </div>
                            <div id="collapse<?= $i; ?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading1">
                                <div class="panel-body">
                                    <p><?= get_the_content(); ?>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <?php $i++; endwhile; endif; ?>
                        
                        <!-- End panel group -->

                    </div>
                </div>
            </div>
        </div>
    </article>
</section>


    <?php get_footer(); ?>
    <!-- /.container -->