<?php
/**
 * The template for displaying pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

get_header('nav');  
?>
	<!-- DETAIL PRODUCT -->
<section>
    <article>
        <div class="wrap-blogview">
            <div class="container">
                <div class="row | title-blogview">
                    <div class="col-lg-3 | col-md-3 | col-sm-4 | col-xs-12 | pull-right">
                        <div class="btn-blog-list | btn-blogview">
                            <a href="bloglist.html">blog list</a>
                        </div>
                    </div>
                </div>

                <!-- BLOG CONTENT-->
                <div class="row | padd15">
                    <div class="wrap-content-blogview | clearfix">
                        <div class="col-md-10 | col-sm-10 | nopadding">
                            <div class="img-blogview">
                                <img src="asset/img/img-blog.jpg" alt="">
                            </div>
                            <div class="tag-blog | tag-blogview">
                                <ul>
                                    <li><a href="#">MB</a></li>
                                    <li><a href="#">User Experience</a></li>
                                    <li><a href="#">Cloud</a></li>
                                    <li><a href="#">Vision</a></li>
                                    <li><a href="#">Insight And Analytics</a></li>
                                </ul>
                            </div>
                            <div class="txt-blogview">
                                <h3 class="title-blogview">
                                    5 Key Issues SMBs are Grappling With Issues SMBs are Grappling With
                                </h3>
                                <p><span>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quaerat quas molestias culpa sunt, voluptatem provident commodi quasi natus? Quam, quaerat sint nisi asperiores. Autem, ex voluptates fuga, mollitia vero laborum!</span>
                                <span>Enim aperiam voluptate accusantium doloribus, quos architecto facere minus distinctio alias. Porro aliquid accusantium repellat dicta rerum. A quos officiis quibusdam et rem, aliquam, eveniet atque repellendus voluptate voluptatibus hic.</span><br><br>
                                <span>Mollitia natus ducimus pariatur, omnis maxime impedit vero dicta molestiae saepe nemo officia et, sed aliquam cum deserunt rem labore fuga. Nulla, nihil. Voluptatum dolorum quibusdam dolorem, excepturi! Dolores, consequuntur!</span>
                                <span>Illum culpa dignissimos qui ipsam deleniti fuga deserunt libero, explicabo distinctio blanditiis consequuntur omnis eius, vitae quas cumque nam amet laborum ad mollitia quae accusantium reprehenderit adipisci voluptate! Magnam, dolorum?</span><br><br>
                                <span>Et ipsam nam a, autem ut repellendus eius dolorem saepe magnam eveniet doloremque perferendis aliquam aut veritatis, molestias fugit accusantium recusandae porro totam consectetur quasi! Inventore voluptas doloribus sapiente maxime!</span>
                                <span>Vel corporis esse vitae deleniti rem, tempore ea saepe mollitia porro sapiente laboriosam beatae pariatur qui dignissimos natus enim maxime nisi nam illo, ratione velit, aliquid. Repellat explicabo earum autem?</span>
                                <span>Mollitia voluptatibus consectetur eius sequi temporibus eos suscipit assumenda, impedit quibusdam maxime sapiente exercitationem in ut omnis aperiam a quasi dolores iste dolorum facilis facere doloribus repellendus dicta? Reprehenderit, quia.</span><br><br>
                                <span>Quos non sunt recusandae laboriosam nostrum, nulla aliquid unde dignissimos iste repudiandae sit, porro, placeat temporibus laudantium. Necessitatibus cumque numquam quos ipsam qui reprehenderit praesentium cum porro explicabo. Deleniti, ad.</span>
                                <span>Natus minus, dolore, veniam ex hic quis, maiores non similique cumque quasi sequi odit, temporibus quae accusantium. Sapiente est quos quaerat commodi qui nulla temporibus autem ducimus accusantium iusto? Beatae!</span>
                                <span>Ipsa doloribus et, iusto libero cum, consectetur nostrum possimus molestias dolor, quas voluptatum voluptatem blanditiis reprehenderit tempore unde. Consequatur unde quaerat sequi architecto id aliquid, commodi dolores ipsum atque perferendis!</span></p>
                            </div>
                        </div>
                        <div class="col-md-2 | col-sm-2">
                            <div class="date-blog | date-blogview | pull-left">
                                <p class="bdate">10</p>
                                <p class="bmonth">nov</p>
                                <p class="byear">2016</p>
                            </div>
                            <div class="share-blogview">
                                <p>bagikan</p>
                                <ul>
                                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                    <li><a href="#"><i class="fa fa-youtube-play"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

            </div><!-- end container -->
        </div>
    </article>
</section>

<!-- ARTIKEL TERKAIT -->
<section>
    <article>
        <div class="wrap-artikel-terkait">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h3 class="main-artikel-terkait">artikel terkait</h3>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 | col-sm-6">
                        <div class="artikel-terkait">
                            <div class="col-lg-4 | col-md-4 | img-artikel-terkait | nopadding">
                                <img src="asset/img/img-blog.jpg" alt="">
                            </div>
                            <div class="col-lg-5 | col-md-5 | title-artikel-terkait">
                                <h3><a href="#">5 Key Issues SMBs are Grappling
                                With Issues SMBs are Grappling With
                                Issues SMBs are Issues</a></h3>
                            </div> 
                            <div class="col-lg-2 | col-md-2 | date-blog | date-artikel-terkait | pull-left">
                                <p class="bdate">10</p>
                                <p class="bmonth">nov</p>
                                <p class="byear">2016</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 | col-sm-6">
                        <div class="artikel-terkait">
                            <div class="col-lg-4 | col-md-4 | img-artikel-terkait | nopadding">
                                <img src="asset/img/img-blog.jpg" alt="">
                            </div>
                            <div class="col-lg-5 | col-md-5 | title-artikel-terkait">
                                <h3><a href="#">5 Key Issues SMBs are Grappling
                                With Issues SMBs are Grappling With
                                Issues SMBs are Issues</a></h3>
                            </div> 
                            <div class="col-lg-2 | col-md-2 | date-blog | date-artikel-terkait | pull-left">
                                <p class="bdate">10</p>
                                <p class="bmonth">nov</p>
                                <p class="byear">2016</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </article>
</section>

<?php get_footer(); ?>