<?php 
/**
 * Template Name: OXO Detail
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
$alcatel_theme_options = 'alcatel_theme_options'; 
$alcatel_theme_options = get_option($alcatel_theme_options);
$title1  = $alcatel_theme_options['alcatel_homeservice_title1'];
$title2  = $alcatel_theme_options['alcatel_homeservice_title_1'];
$title3  = $alcatel_theme_options['alcatel_homeservice_title_2'];
$url1    = esc_url( $alcatel_theme_options['alcatel_homeservice_url1'] );
$image1 = esc_url( $alcatel_theme_options['alcatel_homeservice_image1'] );
$desc1   = $alcatel_theme_options['alcatel_homeservice_title_3'];
 // $zoom_img_page =  MultiPostThumbnails::get_post_thumbnail_url(get_post_type(), 'zoom-image-page', NULL, 'large');
 // echo $zoom_img_page;die();
 $args = array( 'post_type' => 'page' , 'orderby' => 'menu_order', 'order' => 'ASC', 'post_status' => 'publish' );
                $loop_slide = new WP_Query( $args_slide );
                $counter = 0;
// print_r($args);die();

get_header('nav');  ?>
<!-- DETAIL PRODUCT -->
<section>
    <article>
        <div class="wrap-solusi-oxo">
            <div class="container">
             <?php while(have_posts()) : the_post();
                $meta_box_desc2 = get_post_meta( $post->ID, 'description_metaname_value', true );
                ?>
                <div class="row">
                    <div class="col-md-12">
                        <div class="top-title | oxo-title | wow fadeInDown">
                            <h3><?= $title1; ?><br><span><?= $title2; ?></span></h3>
                        </div>
                    </div>
                </div>
               
                <div class="row">
                    <div class="col-md-6 | col-sm-5">
                        <div class="txt-solusi | wow fadeInDown">
                            <h3><?php the_title(); ?></h3>
                            <p><?= the_content(); ?></p>
                        </div>
                    </div>
                    <div class="col-md-6 | col-sm-7">
                        <div class="wrap-img-solusi">
                            <div class="main-img-solusi | wow fadeInUp | gallery">
                                <div class="wrap-zoom">
                                    <a href="<?= esc_url( $alcatel_theme_options['alcatel_homeservice_image1'] );?>" rel="prettyPhoto">
                                        <img src="<?= esc_url( home_url('/')); ?>wp-content/assets/img/icon/icon-zoom.png" alt="">
                                    </a>
                                </div>
                                <a href="<?= esc_url( $alcatel_theme_options['alcatel_homeservice_image1'] ); ?>" rel="prettyPhoto">
                                    <img src="<?= $image1;?>" alt="">
                                </a>
                            </div>
                            <a class="btn-cari-solusi" href="#">cari promosi</a>
                            <a class="btn-download-solusi" href="#">download promosi</a>
                        </div>
                    </div>
                </div>
                <?php endwhile;?>
            </div>
        </div>
    </article>
</section>  

<!-- DETAIL SOLUSI -->
<section>
    <article>
        <div class="wrap-fitur-solusi">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="fitur-solusi">
                           <!--  <h3>keuntungan</h3> -->
                            <?= $meta_box_desc2; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </article>
</section>


<!-- HUBUNGI KAMI -->
<?php get_template_part('contact_us'); ?>
<!-- <section>
    <article>
        <div class="wrap-contact">
            <div class="icon-online">
                <a href="#"><img src="asset/img/icon/icon-online.png" alt=""></a>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="title-contact">
                            <h3>hubungi kami</h3>
                            <p>pertanyaan tentang produk, 
                            perusahaan dan lainnya.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4 | col-md-offset-1 | col-sm-6">
                        <div class="info-contact">
                            <div class="info-txt">
                                <h3 class="info-color">ALCATEL-LUCENT ENTERPRISE</h3>
                            </div>
                            <div class="info-txt">
                                <h3>alamat</h3>
                                <p>Menara Palma 8th Floor
                                <br>Jl. H.R. Rasuna Said Blok X2 kav. 6
                                <br>Kuningan, Jakarta 12950 - Indonesia
                                </p>
                            </div>
                            <div class="info-txt">
                                <h3>EMAIL ENQUIRY</h3>
                                <p>Untuk permintaan produk hubungi :
                                <br><span>enquiry@acapacific.co.id
                                <br>smb.enquiry@al-enterprise.co.id</span>
                                </p>
                            </div>
                            <div class="info-txt">
                                <h3 class="info-color">DISTRIBUTOR</h3>
                                <img src="asset/img/logo/acablack.png" alt="">
                                <p>PT. ACA Pacific
                                <br>Plaza Kuningan Menara Utara 5th Floor
                                <br>Jl. H.R. Rasuna Said Kav. C11-14 Kuningan
                                <br>Jakarta 12940, Indonesia t : +6221 29023483
                                <br><span>f : +6221 29023484 
                                <br>w : www.acapacific.co.id</span></p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 | col-sm-6">
                        <div class="form-contact">
                            <form data-toggle="validator" role="form">
                                <div class="form-group">
                                    <input type="text" class="form-control" id="inputName" placeholder="Nama" required>
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control" id="inputName" placeholder="Perusahaan" required>
                                </div>
                                <div class="form-group">
                                    <input type="email" class="form-control" id="inputEmail" placeholder="Email" data-error="Bruh, that email address is invalid" required>
                                    <div class="help-block with-errors"></div>
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control" id="inputName" placeholder="No. Telepon" required>
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control" id="inputPhone" placeholder="Phone Number" required>
                                </div>
                                <select name="select" class="form-control selectpicker" >
                                <option value="" class="option-top">Produk</option>
                                <option>Web Development</option>
                                <option>Google AdWord</option>
                                <option>Google Analytics</option>
                                <option>SEO</option>
                                </select>
                                <textarea class="form-control" rows="5" id="comment" placeholder="Pesan"></textarea>

                                <div class="wrap-captcha">
                                    <img src="asset/img/captcha.jpg" alt="">
                                </div>

                                <div class="btn-contact | form-group">
                                    <button type="submit" class="btn btn-warning">kirim</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </article>
</section> -->


<?php get_footer(); ?>