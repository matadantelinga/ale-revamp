<!DOCTYPE html>
<html>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="keywords" content="">
    <meta name="author" content="">
      <title>ALCATEL</title>
      <link rel="shortcut icon" type="image/x-icon" href="<?php echo esc_url( get_option('siteurl') ); ?>/wp-content/assets/img/logo/favicon.png" />

    <link rel="stylesheet" href="<?= esc_url(get_option('siteurl')) ?>/wp-content/assets/css/app.css">
    <link rel="stylesheet" href="<?= esc_url(get_option('siteurl')) ?>/wp-content/assets/css/app-rsg.css?v=20170406-1816">
    <link rel="stylesheet" href="<?= esc_url(get_option('siteurl')) ?>/wp-content/assets/css/animate.css">
     <link rel="stylesheet" href="<?= esc_url(get_option('siteurl')) ?>/wp-content/assets/css/prettyPhoto.css">
    <!-- FONT -->
    <link href="<?= esc_url(get_option('siteurl')) ?>/wp-content/assets/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    
    <!-- MONSERRAT FONT GOOGLE -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
     <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-P6FMMX7');</script>
    <!-- End Google Tag Manager -->  


    <script src='https://www.google.com/recaptcha/api.js'></script>
    <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-P6FMMX7"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<!-- NAVBAR -->
<div class="nav-main">
    <?php
          wp_nav_menu( array(
            'menu'   => 'Something custom walker',
            'walker' => new Custom_Nav_Walker()
        ) );
    ?>
</div>

<nav class="nav-fixed | nav-white">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 | col-md-4 | col-sm-4 | col-xs-5">
                <div class="logo-topleft">
                    <a href="<?= site_url(); ?>">
                        <img src="<?= esc_url( home_url('/')); ?>wp-content/assets/img/logo/acablack.png" alt="">
                    </a>
                </div>
            </div>
            <div class="wrap-nav-icon-lg | col-lg-4 | col-md-4 | col-sm-4">
                <div class="nav-icon-lg | nav-icon-black">
                    <span></span>
                    <span></span>
                    <span></span>
                    <span></span>
                </div>
            </div>
            <div class="col-lg-4 | col-md-4 | col-sm-4 | col-xs-5">
                <div class="logo-topright | pull-right">
                    <a href="<?= site_url(); ?>">
                        <img src="<?= esc_url( home_url('/')); ?>wp-content/assets/img/logo/aca-right-black.png" alt="">
                    </a>
                </div>
            </div>
            <div class="col-md-12 | col-xs-2">
                <div class="nav-icon-xs | nav-icon-black">
                    <span></span>
                    <span></span>
                    <span></span>
                    <span></span>
                </div>
            </div>
        </div>
    </div>
</nav>


