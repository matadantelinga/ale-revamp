<?php 
/**
 * Template Name: Wifi Omni Detail
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
$alcatel_theme_options = 'alcatel_theme_options'; 
$alcatel_theme_options = get_option($alcatel_theme_options);
$title1  = $alcatel_theme_options['alcatel_homeservice_title3'];
$title2  = $alcatel_theme_options['alcatel_homeservice_title_7'];
$title3  = $alcatel_theme_options['alcatel_homeservice_title_8'];
$image1 = esc_url( $alcatel_theme_options['alcatel_homeservice_image3'] );
// echo $image1; die();
$url1    = esc_url( $alcatel_theme_options['alcatel_homeservice_url3'] );
$desc1   = $alcatel_theme_options['alcatel_homeservice_title_9'];
$args = array( 'post_type' => 'page' , 'orderby' => 'menu_order', 'order' => 'ASC', 'post_status' => 'publish' );
$loop_slide = new WP_Query( $args_slide );
$counter = 0;
get_header('nav');  ?>
<section>
    <article>
        <div class="wrap-solusi-wifi">
            <div class="container">
                 <?php while(have_posts()) : the_post();
                    $meta_box_desc2 = get_post_meta( $post->ID, 'description_metaname_value', true );
                ?>
                <div class="row">
                    <div class="col-md-12">
                        <div class="top-title | wifi-title | wifi-detail | wow fadeInDown">
                            <h3><?= $title1; ?><br><span><?= $title2; ?></span></h3>
                            <p class="sub-left-content | sub-wifi"><?php the_title();?></p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 | col-sm-5">
                        <div class="txt-solusi | color-grey | wow fadeInDown">
                            <p><?= the_content(); ; ?></p>
                        </div>
                    </div>
                    <div class="col-md-6 | col-sm-7">
                        <div class="wrap-img-solusi">
                            <div class="main-img-solusi | img-wifi-solusi | wow fadeInUp | gallery">
                                <div class="wrap-zoom">
                                    <a href="<?= esc_url( home_url('/')); ?>wp-content/assets/img/object/omniwifi.png" rel="prettyPhoto">
                                        <img src="<?= esc_url( home_url('/')); ?>wp-content/assets/img/icon/icon-zoom.png" alt="">
                                    </a>
                                </div>
                                <a href="<?= esc_url( home_url('/')); ?>wp-content/assets/img/object/omniwifi.png" rel="prettyPhoto">
                                    <img src="<?= esc_url( home_url('/')); ?>wp-content/assets/img/object/omniwifi.png" alt="">
                                </a>
                            </div>
                            <a class="btn-cari-solusi" href="#">cari promosi</a>
                            <a class="btn-download-solusi" href="#">download promosi</a>
                        </div>
                    </div>
                </div>
                <?php endwhile; ?>
            </div>
        </div>
    </article>
</section>  

<!-- DETAIL SOLUSI -->
<section>
    <article>
        <div class="wrap-fitur-solusi">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="fitur-solusi">
                            <?= $meta_box_desc2; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </article>
</section>
<?php get_template_part('hubungi_kami'); ?>

<?php get_footer(); ?>