<section>
    <article>
        <div class="wrap-content-wifi">
            <div class="container">
                <div class="row">
                  <?php 
                    $args_slide = array( 'post_type' => 'solusi-alcatel' , 'orderby' => 'menu_order', 'order' => 'ASC', 'post_status' => 'publish' );          
                    $get_solusi = get_posts($args_slide);
                    //  echo "<pre>";
                    // print_r($get_solusi[2]);
                    if($get_solusi):
                        // print_r($get_solusi[0]->ID);die();
                            $Title1 = get_post_meta($get_solusi[2]->ID,'produk1_solusi_meta_box',true);
                            $Title2 = get_post_meta($get_solusi[2]->ID,'produk2_solusi_meta_box',true);
                            $image = get_the_post_thumbnail_url($get_solusi[2]);
                            $getTitle = $Title1.' '.$Title2;
                            // print_r($getTitle);die();  
                        ?>
                    <div class="col-md-12">
                        <div class="top-title | wifi-title | wow fadeInDown">
                            <h3><?= $Title1; ?><br><span><?= $Title2; ?></span></h3>
                            <!-- <p class="sub-left-content | sub-wifi">access point nirkabel dalam ruangan</p> -->
                        </div>
                    </div>
                    <div class="col-md-5 | col-sm-6">
                        <div class="left-content | wow fadeInUp">
                            <div class="wrap-carousel-promo">
                                 <?php 

                                $get_promo = get_post_id_based_on_template('promosi-alcatel',$vTitle);
                                // echo "<pre>";
                                // print_r($get_promo);die();
                                // if(count($get_promo) >= 1):
                            ?>
                                <div class="arrow-promo">
                                    <a class="control-promo" href="#carousel-promo" data-slide="prev">
                                        <i class="fa fa-4x fa-angle-left"></i>
                                    </a>
                                </div>
                            
                                <div id="carousel-promo" class="carousel-promo slide carousel-sync" data-ride="carousel" data-pause="false">
                                    <div class="carousel-inner | carousel-oxo-top">
                                    <?php  $count = 0;
                                    foreach ($get_promo as $key => $promo):
                                        $title3 = get_post_meta($promo->ID,'subtitle3_promosi_meta_box',true);
                                        if(!empty($title3)):?>
                                        <div class="item | item-promo | <?= ($count==0) ? 'active' : ''; ?>">
                                            <p class="txt-item-promo">
                                                <?= $title3;?>
                                            </p>
                                        </div>
                                         <?php endif; $count++; endforeach;?>
                                    </div>
                                </div>
                                
                                <!-- NEXT BTN -->
                                <div class="arrow-promo">
                                    <a class="control-promo" href="#carousel-promo" data-slide="next">
                                        <i class="fa fa-4x fa-angle-right"></i>
                                    </a>
                                </div>
                               
                                <div id="carousel-promo" class="slide carousel-sync" data-ride="carousel" data-pause="false">
                                    <div class="carousel-inner | carousel-oxo-bottom">
                                     <?php 
                                        $count=0;
                                        foreach ($get_promo as $key => $promo):
                                            $title5 = get_post_meta($promo->ID,'subtitle5_promosi_meta_box',true);
                                            $title4 = get_post_meta($promo->ID,'subtitle4_promosi_meta_box',true);
                                            $link1 = get_permalink($promo->ID);
                                            // print_r($title4);
                                         ?>
                                        <div class="item <?= ($count==0) ? 'active' : ''; ?>">
                                            <div class="txt-oxo-promo">
                                                <!-- update -->
                                               <p><?= $title5; ?></p>
                                                <?php if($title4>0){ ?><p>harga idr <?= number_format($title4,2);?> </p><?php } ?>
                                            </div>
                                            <a class="btn-content | oxo-btn" href="<?= $link1;?>">lihat promo<i class="fa fa-long-arrow-right"></i>
                                            </a>
                                        </div>
                                        <?php $count++; endforeach; ?>
                                    </div>
                                </div>
                            </div>
                            <!-- <?php //endif; ?> -->
                        </div>
                    </div>
                    <div class="col-md-6 | col-sm-5">
                        <div class="right-content | img-wifi | wow fadeInRight">
                            <img src="<?= $image; ?>" alt="<?= $getTitle;?>" title="">
                        </div>
                    </div>
                     <?php endif;?>
                </div>
            </div>
        </div>
    </article>
</section>

