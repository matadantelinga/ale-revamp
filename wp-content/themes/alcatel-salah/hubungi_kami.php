<?php 
/**
 * Template Name: Hubungi Kami Template
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
$alcatel_theme_msg = get_option('alcatel_theme_options');
$toMsg = $alcatel_theme_msg['alcatel_to_message1'];
$toMsgCc = $alcatel_theme_msg['alcatel_to_message2'];
$toMsgBCc = $alcatel_theme_msg['alcatel_to_message3'];
$Message = $alcatel_theme_msg['alcatel_to_message_3'];
// print_r($toMsgCc);die();

global $wpdb;
$tbl_name = $wpdb->prefix. "contact_us";
$produk = '';
$errors = new WP_Error();   
if('POST'== $_SERVER['REQUEST_METHOD'] && !empty($_POST['action']) && $_POST['action']=='submit-form'){
     $recaptcha_secret = "6LceHBsUAAAAAA6S0GkVp-vBEGGzKBnR8jgQh5AG";
        $response = file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=".$recaptcha_secret."&response=".$_POST['g-recaptcha-response']);
        $response = json_decode($response, true);
       
    $username = $wpdb->escape(trim($_POST['sws_inputName']));
    $company = $wpdb->escape(trim($_POST['sws_inputCompany']));
    $email = $wpdb->escape(trim($_POST['sws_inputEmail']));
    $tlp = $wpdb->escape(trim($_POST['sws_inputTlp']));
    $phone = $wpdb->escape(trim($_POST['sws_inputPhone']));
    $produk = $wpdb->escape(trim($_POST['options']['sws_inputProduk']));
    $message = $wpdb->escape(trim($_POST['sws_inputMsg']));
    date_default_timezone_set("Asia/Bangkok");
    $date = date('Y-m-d h:i:sa');  

   $kv_data = array( 
                'date_created' => $date
                ,'name'         => $username
                ,'perusahaan'   => $company
                ,'email'        => $email
                ,'telepon'      => $tlp
                ,'phone'        => $phone
                ,'produk'       => $produk
                ,'pesan'        => $message
            ); 
   // print_r($kv_data);die();
     // if(isset($_POST['g-recaptcha-response']) && !empty($_POST['g-recaptcha-response'])){
       
          if($response["success"] === false)
          {
             $err = 'Please click on the reCAPTCHA box.';
          }else if(empty($username)|| empty($company) || empty($email) || empty($tlp) || empty($phone) || empty($produk) || empty($message)){
              $err = 'Please don\'t leave the required fields.';
              // echo $err;die();
          }else if(!filter_var($email, FILTER_VALIDATE_EMAIL)){
              $err = 'Invalid email address.';
              // echo $err;die();
          }else if(email_exists($email)){
              $err = 'Email already exist.';
              // echo $err;die();
          }else{
              
             $tambah = $wpdb->insert($tbl_name, $kv_data);
             if($tambah){

                $success = 'Data Berhasil di Tambahkan';

                $email_data = array();
                foreach ($kv_data as $key => $value) {
                  if($key === 'pesan'){
                    $email_data['Permintaan Khusus'] = $value;  
                  }
                  else if($key === 'date_created'){
                    $email_data['Tanggal'] = date('l, d M Y H:i:s', strtotime($value));
                  }
                  else{
                    $email_data[ucwords($key)] = $value;
                  }
                }

                 // $mdata = array(
                 //          'data' => $email_data
                 //          ,'cc' => 'Ichita-Meilandany Puspa <Ichita-Meilandany.Puspa@al-enterprise.com>, Rio <rio.hertanto@acapacific.co.id>'
                 //          ,'bcc' => 'Fauzi Ezi <ezi@dgtraffic.com>,Risgianto <risgi@webdeveloper.dgtraffic.com>'
                          
                 //  );

                // dg_send_email('acaale@acapacific.co.id', 'Contact Form - Product', 'email-notif-admin', $mdata);
                 $mdata = array(
                          'data' => $email_data
                          ,'cc' => $toMsgCc
                          ,'bcc' => $toMsgBCc
                          
                  );
                 dg_send_email($toMsg, 'Contact Form - Product', 'email-notif-admin', $mdata);
                 redirect(site_url('terima-kasih')); 
                
             }
             
              // $tambah = $wpdb->insert($table_name, $kv_data,
              //   array('%s','%s','%s','%s','%s','%s','%s'));
                // if($tambah) $message = "Data berhasil ditambahkan";
                // echo $message;
          }

}

get_header('nav');  ?>

<!-- HUBUNGI KAMI -->
<section>
    <article>
        <div class="wrap-contact | contact-view">
            <!-- <div class="icon-online | icon-online-view">
                <a href="#"><img src="<?= esc_url(get_option('siteurl')) ?>/wp-content/assets/img/icon/icon-online.png" alt=""></a>
            </div> -->
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="title-contact">
                            <h3 class="big-title">hubungi kami</h3>
                            <p class="little-title">pertanyaan tentang produk, 
                            perusahaan dan lainnya.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4 | col-md-offset-1 | col-sm-6">
                        <div class="info-contact">
                            <?php dynamic_sidebar('contact-bar'); ?>
                        </div>
                    </div>
                   
                    <div class="padding_article">
                    <div class="col-md-6 | col-sm-6">
                        <div class="form-contact">
                          <?php if(isset($success) && $success):?>
                            <div class="alert alert-success">
                              <a href="#" class="close" data-dismiss="alert">&times;</a>
                              <center><strong><?= $success; ?></strong></center>
                            </div>
                          <?php endif; 
                            if(isset($err) && $err):?>
                            <div class="alert alert-danger">
                              <a href="#" class="close" data-dismiss="alert">&times;</a>
                              <center><strong><?= $err; ?></strong></center>
                            </div>
                          <?php endif; ?>
                            <form action="" name="contactForm" method="post" role="form">
                                  <div class="form-group">
                                      <input type="text" class="form-control" name="sws_inputName" id="sws_inputName" value="" placeholder="Nama" required >
                                  </div>
                                  <div class="form-group">
                                      <input type="text" class="form-control" name="sws_inputCompany" id="sws_inputCompany" value="" placeholder="Perusahaan" required>
                                  </div>
                                  <div class="form-group">
                                      <input type="email" class="form-control" name="sws_inputEmail" value="" id="sws_inputEmail" placeholder="Email" data-error="Sorry, that email address is invalid" required>
                                      <div class="help-block with-errors"></div>
                                  </div>
                                  <div class="form-group">
                                      <input type="text" class="form-control" value="" name="sws_inputTlp" id="sws_inputTlp" placeholder="No. Telepon" required>
                                  </div>
                                  <div class="form-group">
                                      <input type="text" class="form-control" name="sws_inputPhone" value="" id="sws_inputPhone" placeholder="No. Handphone" required>
                                  </div>
                                  

                                  <select name="options[sws_inputProduk]" id="select" class="form-control selectpicker" >
                                  <!-- <select name="select" id="select" class="form-control selectpicker" > -->
                                      <option value="" <?php selected($produk,'');?>>Produk</option>
                                      <option <?php selected($produk,'OXO CONNECT R2');?> value="OXO CONNECT R2">OXO CONNECT R2</option>
                                      <option <?php selected($produk,'OMNISWITCH 6350');?> value="OMNISWITCH 6350">OMNISWITCH 6350</option>
                                      <option <?php selected($produk,'WIFI OMNIACCESS AP 1101');?> value="WIFI OMNIACCESS AP 1101">WIFI OMNIACCESS AP 1101</option>
                                  </select>

                                  <textarea class="form-control" name="sws_inputMsg" rows="5" id="comment" value="" placeholder="Permintaan Khusus"></textarea>
                                 

                                  <div class="wrap-captcha">
                                       <div class="g-recaptcha" data-sitekey="6LceHBsUAAAAAJafmmjp3ybxbVL9EZfrcgh3vYp9"></div>
                                  </div>

                                  <div class="btn-contact | form-group">
                                      <input type="hidden" name="action" value="submit-form">
                                      <button type="submit" class="btn btn-warning">kirim</button>
                                  </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </article>
</section>



<?php get_footer(); ?>