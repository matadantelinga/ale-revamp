<?php 
/**
 * Template Name: Blog Template
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

get_header('nav');  ?>
<style type="text/css">
    // CSS Code - add this to the style.css file
.navigation { list-style:none; font-size:12px; }
.navigation li{ display:inline; }
.navigation li a{ display:block; float:left; padding:4px 9px; margin-right:7px; border:1px solid #efefef; }
.navigation li span.current { display:block; float:left; padding:4px 9px; margin-right:7px; border:1px solid #efefef; background-color:#f5f5f5;  }  
.navigation li span.dots { display:block; float:left; padding:4px 9px; margin-right:7px;  }
</style>

<!-- BLOG LIST -->
<section>
    <article>
        <div class="wrap-bloglist">
            <div class="container">
                <div class="row | title-bloglist">
                    <div class="col-lg-7 | col-md-6 | col-md-offset-1 | col-sm-7">
                        <div class="top-title-blog">
                            <h4><span>blog</span>list</h4>
                        </div>
                    </div>
                 <?php while(have_posts()): the_post(); 
                global $wp_query;
                    $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
                    ?>
                    <div class="col-lg-3 | col-md-4 | col-sm-5">
                        <div class="dropdown">
                            <a href="#" class="btn-blog-listview" dropdown-toggle" type="button" data-toggle="dropdown">urut berdasarkan</a>
                            <ul class="dropdown-menu">
                                <li><a href="?sortby=tanggal">Tanggal</a></li>
                                <li><a href="?sortby=judul">Judul</a></li>
                                <!-- <li><a href="#">Kategori</a></li> -->
                            </ul>
                        </div>
                        
                    </div>
                </div>
                <!-- BLOG -->
               <?php
if (array_key_exists("sortby", $_GET) === true)
{
    $postslist = sortIt($_GET['sortby']);
    // print_r($post_list);die();

if($postslist->have_posts()):
    while ($postslist->have_posts()) : $postslist->the_post();
         $total = $postslist->max_num_pages;
            $background = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' );
            // print_r($background);die();
            $excerpt = substr( strip_tags( get_the_content() ), 0, 250);
            $excerpt = $excerpt.'.';
            $permalink = get_permalink($post->ID);
            $date=get_the_date('d', $post->ID );
            $month=get_the_date('M', $post->ID );
            $year = get_the_date('Y', $post->ID);
                ?>
                <div class="row">
                    <div class="col-md-10 | col-md-offset-1">
                        <div class="content-bloglist | content-blog">
                            <div class="col-lg-5 | col-md-4 | col-sm-5 | nopadding">
                                <div class="img-blog" style="background: url('<?= $background[0]; ?>') no-repeat;background-position: center;background-size: cover;">
                                    <a href="<?= the_permalink() ?>">&nbsp;</a>
                                </div>
                            </div>
                            <div class="col-lg-7 | col-md-8 | col-sm-7 | nopadding">
                                <div class="wrap-txt-blog | pull-left">
                                    <h3 class="title-blog">
                                        <a href="<?= the_permalink() ?>"><?php the_title(); ?></a>
                                    </h3>
                                    <p class="txt-blog">
                                       <?= $excerpt; ?>
                                    </p>
                                    <div class="tag-blog">
                                        <?php the_tags( '<ul><li>', '</li><li>', '</li></ul>' ); ?>
                                    </div>
                                </div>
                                <div class="date-blog | pull-left">
                                    <p class="bdate"><?= $date; ?></p>
                                    <p class="bmonth"><?= $month;?></p>
                                    <p class="byear"><?= $year; ?></p>
                                </div>
                            </div>
                            <div class="view-blog">
                                <div class="comment-blog">
                                    <a href="#">
                                    <img src="<?= esc_url(get_option('siteurl')) ?>/wp-content/assets/img/icon/icon-time.png" alt="">
                                    <?= dg_get_date_diff(date('Y-m-d H:i:s'), get_the_time( 'Y-m-d H:i:s', $post->ID)); ?> yang lalu
                                    </a>
                                </div>
                                <ul class="socmed-blog">
                                    <li>share :</li>
                                    <li>
                                        <a href="<?= the_permalink() ?>" data-share-facebook data-share-url="<?= the_permalink() ?>" data-share-title="<?php the_title();?>" data-share-text="<?= $excerpt; ?>" ><i class="fa fa-facebook"></i></a>
                                    </li>
                                    <li>
                                        <a data-share-twitter data-share-url="<?= the_permalink() ?>" data-share-text="<?php the_title(); ?>" href="<?= the_permalink() ?>"><i class="fa fa-twitter"></i></a>
                                    </li>
                                    <li>
                                        <a href="mailto:?body=<?php the_permalink();?>"><i class="fa fa-envelope"></i></a>
                                    </li>
                                </ul>
                                <a class="btn-readmore" href="<?= $permalink ?>">
                                    selengkapnya<i class="fa fa-long-arrow-right"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
               <?php endwhile; endif; 
}else{ 
                   $args = array(
                            'posts_per_page' => 6
                           ,'category_name' => 'Blog'
                           ,'orderby'       => 'title'
                           ,'order'         => 'asc'
                           ,'post_type'     => 'post'
                           ,'paged'         => $paged
                           ,'post_status'   => 'publish'
                        );                      
                     $postslist = new WP_Query( $args );
                     if($postslist->have_posts()):
                        while ( $postslist->have_posts()) : $postslist->the_post(); 
                            $total = $postslist->max_num_pages;
                            // print_r($total);die();
                            $background = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' );
                            // print_r($background);die();
                            $excerpt = substr( strip_tags( get_the_content() ), 0, 250);
                            $excerpt = $excerpt.'.';
                            $permalink = get_permalink($post->ID);
                            $date=get_the_date('d', $post->ID );
                            $month=get_the_date('M', $post->ID );
                            $year = get_the_date('Y', $post->ID);

                            // $current_date = date( 'Y-m-d H:i:s');
                            // $get_date = get_the_time( 'Y-m-d H:i:s', $post_id);
                            // $current_date1 = strtotime($current_date);
                            // $get_date1 = strtotime($get_date);
                            // $diff = $current_date1- $get_date1;
                            // $getCount =  round($diff / 86400);
                            // $date1 = get_the_date($post_id);
                            // $date2 = date(get_option('date_format'));
                            // $interval = date_diff($current_date1,$get_date1);
                            // echo $getCount;die();
                ?>
                <div class="row">
                    <div class="col-md-10 | col-md-offset-1">
                        <div class="content-bloglist | content-blog">
                            <div class="col-lg-5 | col-md-4 | col-sm-5 | nopadding">
                                <div class="img-blog" style="background: url('<?= $background[0]; ?>') no-repeat;background-position: center;background-size: cover;">
                                    <a href="<?= the_permalink() ?>">&nbsp;</a>
                                </div>
                            </div>
                            <div class="col-lg-7 | col-md-8 | col-sm-7 | nopadding">
                                <div class="wrap-txt-blog | pull-left">
                                    <h3 class="title-blog">
                                        <a href="<?= the_permalink() ?>"><?php the_title(); ?></a>
                                    </h3>
                                    <p class="txt-blog">
                                       <?= $excerpt; ?>
                                    </p>
                                    <div class="tag-blog">
                                        <?php the_tags( '<ul><li>', '</li><li>', '</li></ul>' ); ?>
                                    </div>
                                </div>
                                <div class="date-blog | pull-left">
                                    <p class="bdate"><?= $date; ?></p>
                                    <p class="bmonth"><?= $month;?></p>
                                    <p class="byear"><?= $year; ?></p>
                                </div>
                            </div>
                            <div class="view-blog">
                                <div class="comment-blog">
                                    <a href="#">
                                    <img src="<?= esc_url(get_option('siteurl')) ?>/wp-content/assets/img/icon/icon-time.png" alt="">
                                    <?= dg_get_date_diff(date('Y-m-d H:i:s'), get_the_time( 'Y-m-d H:i:s', $post->ID)); ?> yang lalu
                                    </a>
                                </div>
                                <ul class="socmed-blog">
                                    <li>share :</li>
                                    <li>
                                        <a href="<?= the_permalink() ?>" data-share-facebook data-share-url="<?= the_permalink() ?>" data-share-title="<?php the_title();?>" data-share-text="<?= $excerpt; ?>" ><i class="fa fa-facebook"></i></a>
                                    </li>
                                    <li>
                                        <a data-share-twitter data-share-url="<?= the_permalink() ?>" data-share-text="<?php the_title(); ?>" href="<?= the_permalink() ?>"><i class="fa fa-twitter"></i></a>
                                    </li>
                                    <li>
                                        <a href="mailto:?body=<?php the_permalink();?>"><i class="fa fa-envelope"></i></a>
                                    </li>
                                </ul>
                                <a class="btn-readmore" href="<?= $permalink ?>">
                                    selengkapnya<i class="fa fa-long-arrow-right"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
    
<?php endwhile; endif; } ?>
                <!-- END BLOG LIST-->
                <!-- PAGINATION -->
                <div class="row">
                    <div class="col-md-12 | col-sm-12">
                        <div class="center-pagination">
                            <ul class="pagination ulpage">
                                <li><?php alcatel_pagination(); ?></li>
                            </ul>
                        </div>
                    </div>
                </div>
            <?php endwhile; ?>
            </div>
        </div>
    </article>
</section>
<?php get_footer(); ?>