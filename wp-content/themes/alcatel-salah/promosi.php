<?php 
/**
 * Template Name: Promosi Template
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

get_header('nav');  
?>
<!-- MAIN TITLE -->
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="title-promo">
                <img src="<?= site_url();?>/wp-content/assets/img/promo.png" alt="promo">
            </div>
        </div>
    </div>
</div>
<?php
if (array_key_exists('page', $_GET)){

  $getx = $_GET['page'];
 
  $getPostName = get_name_from_posts('publish','solusi-alcatel',$getx);
   // print_r( $getPostName);die();
  $getName = $getPostName[0]->post_name;
  $vTitle = $getPostName[0]->post_title;
        if($getx==='oxo-connect-r2'){
          // print_r($vTitle);die();
          include "get-promo-oxo-connect.php"; 
          // include "get-promo-product.php";
        }
        if($getx==='omniswitch-6350'){
          // print_r($vTitle);die();
           include "get-promo-omni-switch.php";
        }
        if($getx==='wifi-omniAccess-ap-1101'){
          // print_r($vTitle);die();
          include "get-promo-wifi-omniaccess.php";
        }
   
}else{
global $post;
 // $args_promosi = array( 'post_type' => 'promosi-alcatel' , 'orderby' => 'menu_order', 'order' => 'ASC', 'post_status' => 'publish' );
 // $get_promosi = get_posts($args_promosi);  
$getPost = get_id_from_posts('publish','promosi-alcatel');
global $post_temp;
$post_temp = array('data'=> array(), 'active' => -1);
// print_r($post_temp);die();
if($getPost){

    foreach ($getPost as $key => $value) {
        $id = get_post_id_by_meta_key_and_value('promo_kategori_meta_box',$value->ID);

        if(!isset($post_temp[$id])) {
            $post_temp['data'][$id] = array();
        }

        $post_temp['data'][$id] = $value;

    }

 


    //  $i=0;
    // for($i=0; $i<6; $i++):
    // $postId = $getPost[$i]->ID;
    // echo "<pre>";
    // print_r($getxx);die();
    // foreach ($getPost as $key => $value) {
        // $postId = $value->ID;
        // $getTempPromo = get_post_id_by_meta_key_and_value('promo_kategori_meta_box',$postId);
        // print_r($getTempPromo);echo "<br>";die();
        // }
      // foreach ($post_temp as $key => $value) {
      //     switch ($key) {
      //       case 'OXO CONNECT R2':
      //            include "promo-oxo-connect.php";
      //            // echo 'oxo';
      //           break;
      //       case 'OMNISWITCH 6350':
      //            include "promo-omni-switch.php";
      //            // echo 'omniswitch';
      //           break;
      //       case 'WIFI OMNIACCESS AP 1101':
      //            include "promo-wifi-omniaccess.php";
      //            // echo 'wifi omni';
      //           break;
      //       default:
      //           // echo 'oxo';
      //           include "promo-oxo-connect.php";
      //           break;
      //   }
      // }
        
        foreach ($post_temp as $key => $value) :
           
            $id = $value['OXO CONNECT R2']->ID;
            $id2 = $value['OMNISWITCH 6350']->ID;
            $id3 = $value['WIFI OMNIACCESS AP 1101']->ID;
            $getTempOxo = get_post_id_by_meta_key_and_value('promo_kategori_meta_box',$id);
             $getTempOmni = get_post_id_by_meta_key_and_value('promo_kategori_meta_box',$id2);
              $getTempWifi = get_post_id_by_meta_key_and_value('promo_kategori_meta_box',$id3);
            if($getTempOxo){
                $post_temp['active'] = $getTempOxo;
                // echo 'oxo'."<br>";
                include "promo-oxo-connect.php";
            }
            if($getTempOmni){
                $post_temp['active'] = $getTempOmni;
                // echo 'omniswitch'."<br>";
               include "promo-omni-switch.php";
            }if($getTempWifi){
                $post_temp['active'] = $getTempWifi;
                // echo 'wifi'."<br>";
                include "promo-wifi-omniaccess.php";
            }

        endforeach;
    // }
}

}
?>
<?php get_footer('more'); ?>
    <!-- /.container -->