<?php
/**
 * The template for displaying archive pages
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * If you'd like to further customize these archive views, you may create a
 * new template file for each one. For example, tag.php (Tag archives),
 * category.php (Category archives), author.php (Author archives), etc.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

get_header('nav'); ?>

<section>
    <article>
        <div class="wrap-blogview">
            <div class="container">
                <div class="row | title-blogview">
                    <div class="col-lg-3 | col-md-3 | col-sm-4 | col-xs-12 | pull-right">
                        <div class="btn-blog-list | btn-blogview">
                            <a href="<?= site_url(); ?>/blog">blog list</a>
                        </div>
                    </div>
                </div>

		<?php
		if ( have_posts() ) : ?>
			<?php
			/* Start the Loop */
			while ( have_posts() ) : the_post();
				 $post_id_main = $post->ID;
                 $excerpt = substr( strip_tags( get_the_content() ), 0, 250);
            ?>
			<div class="row | padd15">
                    <div class="wrap-content-blogview | clearfix">
                        <div class="col-md-10 | col-sm-10 | nopadding">
                            <div class="img-blogview">
                                <?= the_post_thumbnail('full'); ?>
                            </div>
                            <div class="tag-blog | tag-blogview">
                                <?php the_tags( '<ul><li>', '</li><li>', '</li></ul>' ); ?>
                            </div>
                            <div class="txt-blogview">
                                <h3 class="title-blogview">
                                    <?php the_title(); ?>
                                </h3>
                                <p><span><?= the_content();?></span></p>
                            </div>
                        </div>
                        <div class="col-md-2 | col-sm-2">
                            <div class="date-blog | date-blogview | pull-left">
                                <p class="bdate"><?= get_the_date('d',$post->ID); ?></p>
                                <p class="bmonth"><?= get_the_date('M',$post->ID); ?></p>
                                <p class="byear"><?= get_the_date('Y',$post->ID); ?></p>
                            </div>
                            <div class="share-blogview">
                                <p>bagikan</p>
                                <ul>
                                    <li>
                                        <a href="<?= the_permalink() ?>" data-share-facebook data-share-url="<?= the_permalink() ?>" data-share-title="<?php the_title();?>" data-share-text="<?= $excerpt; ?>" ><i class="fa fa-facebook"></i></a>
                                    </li>
                                    <li>
                                        <a data-share-twitter data-share-url="<?= the_permalink() ?>" data-share-text="<?php the_title(); ?>" href="<?= the_permalink() ?>"><i class="fa fa-twitter"></i></a>
                                    </li>
                                    <li>
                                        <a href="mailto:?body=<?php the_permalink();?>"><i class="fa fa-envelope"></i></a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            <?php
				/*
				 * Include the Post-Format-specific template for the content.
				 * If you want to override this in a child theme, then include a file
				 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
				 */
				//get_template_part( 'template-parts/content', get_post_format() );

			endwhile;

			the_posts_pagination( array(
				'prev_text' => '' . '<span class="screen-reader-text">' . 'Previous page' . '</span>',
				'next_text' => '<span class="screen-reader-text">' . 'Next page' . '</span>',
				'before_page_number' => '<span class="meta-nav screen-reader-text">' .'Page' . ' </span>',
			) );

		else :

			get_template_part( 'template-parts/post/content', 'none' );

		endif; ?>

	
	</div><!-- #primary -->
	<!-- <?php //get_sidebar(); ?> -->
</div><!-- .wrap -->
</article>
<section>
<?php get_footer();
