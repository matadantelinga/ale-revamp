<?php

// BEGIN iThemes Security - Do not modify or remove this line
// iThemes Security Config Details: 2
define( 'DISALLOW_FILE_EDIT', true ); // Disable File Editor - Security > Settings > WordPress Tweaks > File Editor
// END iThemes Security - Do not modify or remove this line

/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'alcatel');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '>/I  ./!8`6m$lI;/7[kz=.|31I5Lkm0[^*g`m)!c*9=Q^C9hS&{1h6hJiJesR8V');
define('SECURE_AUTH_KEY',  'LP&Xsr{:5ld^7k&,*+k<kDu;F:3^,&NoqS0;mINIagC vQ9-70~Xb(_;P2yT{2-e');
define('LOGGED_IN_KEY',    '_|PozL/9!F_6z,tTR3.T:-)c .!5,}ZHc5Rbb[G,^+]?bBABXg]mNjG~#M#%dfbo');
define('NONCE_KEY',        '* tS%M+*cf#3P>ks-y=?H:=b= {KB:]r|a8Te<]6|C9#kmk nkFslaNf+P#-Doo/');
define('AUTH_SALT',        'm3_.LQbKxP7&srQ>KuTe^Ms6W!}NK(@@&;bydetl[Em,USuKX[ +qK(m76nhvz42');
define('SECURE_AUTH_SALT', 'A-^DW`?2px7`f!+SZ}$<M1>;r|9Qp9OSb.l(z@OcZIdJKNURzePO>^DE%X@XkCpb');
define('LOGGED_IN_SALT',   'j|MDfCOqEo#Mw P{[`&ja~cnAr*j&&0#UWDo#)UXQ:z,XX[#ai7X^c<hDoQJ|+U,');
define('NONCE_SALT',       '3uE vnP</a7`(wQyauwmXv_&5j<n~vjFhl(oz$yJYfS)cd/@JR3$BHx-4*qjdR`t');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);
define('WP_ENV', 'production');
/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
